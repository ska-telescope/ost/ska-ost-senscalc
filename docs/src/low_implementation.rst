.. _low_implementation:

===================
LOW Implementation
===================

The implementation of the actual calculation for LOW and MID are different, however the two follow the same the basic structure of an API layer (see LOW API docs page), validation,
then calculation, using a look up table. The code is functional in nature, relying on pure functions which do not mutate state.
The diagram below aims to give a view of this functional structure for the calculate endpoint.

Function flow when calling calculate endpoint
----------------------------------------------

.. figure:: diagrams/export/low_calculate_function_view.svg
   :align: center



The flow is the same in general for the continuum, line and PSS calculations: each API path has its own function in the `api` module, and its own function in the
`service` module. This service function is responsible for converting the correct input parameters into the calculation inputs. Then the same calculation function
is used in all cases, with different inputs.


Function flow when calling PSS endpoint
-----------------------------------------------

.. figure:: diagrams/export/low_pss_function_view.svg


Module Structure
-----------------

The Python modules can also been seen below. Mostly the implementation uses module level pure functions, and makes use of
classes only for data objects and to encapsulate the look up tables.


.. figure:: diagrams/export/low_module_diagram.svg
   :align: center

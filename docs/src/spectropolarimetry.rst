.. _spectropolarimetry:

===================
Spectropolarimetry
===================

If the correlated data contain multiple channels and contain the cross-correlations necessary for determining the polarization state
of the detected radiation, it is possible to perform Rotation Measure Synthesis. This allows an image cube of the source to be made
that has Faraday depth on the third axis instead of frequency (`Brentjens & de Bruyn 2005 <https://ui.adsabs.harvard.edu/abs/2005A%26A...441.1217B/abstract>`_). One application of this is the imaging of
weak polarization across a wide band even in the presence of Faraday rotation.

Brentjens & de Bruyn present three equations (61, 62 and 63) that describe fundamental limits on the Rotation Measure Synthesis
technique based on the observational set-up (frequency coverage and spectral resolution). These are the resolution in Faraday depth space (FWHM of the RMSF)
and the largest scale and maximum value of Faraday depth that can be detected. All are now calculated based on the parameters
entered by the user for both continuum and zoom modes and the results reported to the user.

Note that Brentjens & de Bruyn use the term RMTF instead of RMSF. The reasons for the change are given in `Heald (2009) <https://ui.adsabs.harvard.edu/abs/2009IAUS..259..591H/abstract>`_.
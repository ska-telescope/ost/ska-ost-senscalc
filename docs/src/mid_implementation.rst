.. _mid_implementation:

===================
MID Implementation
===================

The implementation of the actual calculation for LOW and MID are different, however the two follow the same the basic structure of an API layer (see MID API docs page), validation,
then calculation, using a look up table. The code is functional in nature, relying on pure functions which do not mutate state.
The diagram below aims to give a view of this functional structure for the calculate endpoints.


Function flow when calling calculate endpoint for MID Continuum and MID Zoom 
-----------------------------------------------------------------------------

.. figure:: diagrams/export/mid_continuum_calculate_function.svg
   :align: center

.. figure:: diagrams/export/mid_zoom_calculate_function.svg
   :align: center


API path has its own function in the `api` module, and its own function in the `service` module. 
This service function is responsible for converting the correct input parameters into the calculation inputs. Then the same calculation function
is used in all cases, with different inputs.


See beam module implementation for the details of the lower level functions.


Module Structure
----------------

The Python modules can also been seen below.


.. figure:: diagrams/export/mid_module_diagram.svg
   :align: center

The module *Calculator* has 2 public functions dealing with final calculations:
*calculate_sensitivity* to get the array sensitivity in 
Jy for the given integration time, and *calculate_integration_time*
to get the integration time required for the given sensitivity. 

Also, this module has 2 functions *prepare_sensitivity_input* and *prepare_integration_input* with a number of required parameters
that define the observing configuration, target and weather. The
rest default to None, in which case their 
values will be calculated automatically. The automatic values can
be overriden by setting them here. 
 
All parameters, internal variables and results that describe 'physical' 
measures are implemented as astropy Quantities to prevent mixups over
units.

"""
The service layer is responsible for turning validated inputs into the relevant calculation inputs,
calling any calculation functions and collating the results.
"""

from math import sqrt
from typing import List, Tuple

import numpy as np
from astropy import units as u
from astropy.units.quantity import Quantity

from ska_ost_senscalc.common.beam import calculate_multi_weighting, calculate_weighting
from ska_ost_senscalc.common.model import (
    BeamSize,
    BeamSizeResponse,
    BeamSizeResult,
    ConfusionNoise,
    ConfusionNoiseResponse,
    ContinuumIntegrationTimeTransformedResults,
    ContinuumSensitivitiesTransformationsInput,
    ContinuumSensitivityTransformationsSubbandsInput,
    ContinuumSensitivityTransformedResults,
    ContinuumWeightingRequestParams,
    ContinuumWeightingResponse,
    EnumConversion,
    IntegrationTimeTransformationsWithoutWeightingInput,
    Limit,
    SensitivitiesTransformationsWithoutWeightingInput,
    SingleZoomWeightingResponse,
    SubbandsBeamSizeTransformedResults,
    SubbandsContinuumSensitivityTransformedResults,
    SubbandsItemsTransformedResults,
    SubbandWeightingResponse,
    Weighting,
    WeightingInput,
    WeightingMultiInput,
    WeightingSpectralMode,
    ZoomRequestPrepared,
    ZoomSensitivitiesTransformationsInput,
    ZoomSensitivityTransformedResults,
)
from ska_ost_senscalc.subarray import (
    LOWArrayConfiguration,
    MIDArrayConfiguration,
    SubarrayStorage,
)
from ska_ost_senscalc.utilities import Telescope

# the minimum Gaussian beam size in arcseconds for low-weighting results
MINIMUM_GAUSSIAN_BEAM_LOW: u.Quantity = u.Quantity(
    1.5, u.arcsec
)  # TODO: Check it it applies to MID
SENS_LIMIT_WARNING = "Warning: You are approaching the confusion limit given the synthesized beam-size and frequency."
LOW_SUBARRAY_STORAGE = SubarrayStorage(Telescope.LOW)
MID_SUBARRAY_STORAGE = SubarrayStorage(Telescope.MID)


def calculate_continuum_results(  # integration time -> sensitivity
    transformation_input: ContinuumSensitivitiesTransformationsInput,
    warnings: list[str],
) -> ContinuumSensitivityTransformedResults:
    """
    Uses an input containing values from Calculator and Weighting results to generate calculated results to be displayed in the UI.

    :param transformation_input: the transformation input including some of calculate and weighting results values.
    :type transformation_input: ContinuumSensitivitiesTransformationsInput
    :param warnings: the list of warnings to be returned.
    :type warnings: List of string
    :return: a data class object representing the transformed results.
    :rtype: ContinuumSensitivityTransformedResults
    """

    weighted_continuum_sensitivity: u.Quantity = get_weighted_sensitivity(
        transformation_input.continuum_sensitivity,
        transformation_input.continuum_weighting_factor,
    )
    continuum_confusion_noise: u.Quantity = get_confusion_noise(
        transformation_input.continuum_conf_noise,
        transformation_input.continuum_beam_min,
        transformation_input.continuum_beam_maj,
    )
    total_continuum_sensitivity: u.Quantity = get_total_sensitivity(
        continuum_confusion_noise, weighted_continuum_sensitivity
    )
    continuum_surface_brightness_sensitivity: u.Quantity = (
        get_surface_brightness_sensitivity(
            transformation_input.continuum_sbs_conv_factor,
            total_continuum_sensitivity,
        )
    )

    continuum_synthesized_beam_size: BeamSizeResult = get_synthesized_beam_size(
        transformation_input.continuum_beam_min,
        transformation_input.continuum_beam_maj,
    )

    weighted_spectral_sensitivity: u.Quantity = get_weighted_sensitivity(
        transformation_input.spectral_sensitivity,
        transformation_input.spectral_weighting_factor,
    )
    spectral_confusion_noise: u.Quantity = get_confusion_noise(
        transformation_input.spectral_conf_noise,
        transformation_input.spectral_beam_min,
        transformation_input.spectral_beam_maj,
    )
    total_spectral_sensitivity: u.Quantity = get_total_sensitivity(
        spectral_confusion_noise, weighted_spectral_sensitivity
    )
    spectral_surface_brightness_sensitivity: u.Quantity = (
        get_surface_brightness_sensitivity(
            transformation_input.spectral_sbs_conv_factor,
            total_spectral_sensitivity,
        )
    )
    spectral_synthesized_beam_size: BeamSizeResult = get_synthesized_beam_size(
        transformation_input.spectral_beam_min,
        transformation_input.spectral_beam_maj,
    )

    # handle warnings
    continuum_sensitivity_limit_reached = sensitivity_limit_reached(
        continuum_confusion_noise, total_continuum_sensitivity
    )
    spectral_sensitivity_limit_reached = sensitivity_limit_reached(
        spectral_confusion_noise, total_spectral_sensitivity
    )
    if continuum_sensitivity_limit_reached:
        warnings.append({"continuum_sensitivity": SENS_LIMIT_WARNING})

    if spectral_sensitivity_limit_reached:
        warnings.append({"spectral_sensitivity": SENS_LIMIT_WARNING})

    return ContinuumSensitivityTransformedResults(
        weighted_continuum_sensitivity=weighted_continuum_sensitivity,
        continuum_confusion_noise=continuum_confusion_noise,
        total_continuum_sensitivity=total_continuum_sensitivity,
        continuum_synthesized_beam_size=continuum_synthesized_beam_size,
        continuum_surface_brightness_sensitivity=continuum_surface_brightness_sensitivity,
        weighted_spectral_sensitivity=weighted_spectral_sensitivity,
        spectral_confusion_noise=spectral_confusion_noise,
        total_spectral_sensitivity=total_spectral_sensitivity,
        spectral_synthesized_beam_size=spectral_synthesized_beam_size,
        spectral_surface_brightness_sensitivity=spectral_surface_brightness_sensitivity,
        warnings=warnings,
    )


def calculate_continuum_results_without_weighting(
    transformation_input: SensitivitiesTransformationsWithoutWeightingInput,
    warnings: list[str],
) -> ContinuumSensitivityTransformedResults:
    """
    Uses an input containing values from Calculator and no Weighting to generate calculated results to be displayed in the UI.
    This is used to calculate results for a ccustom subarray config.

    :param transformation_input: the transformation input including some of calculate results values.
    :type transformation_input: SensitivitiesTransformationsWithoutWeightingInput
    :param warnings: the list of warnings to be returned.
    :type warnings: List of string
    :return: a data class object representing the transformed results.
    :rtype: ContinuumSensitivityTransformedResults
    """

    weighted_continuum_sensitivity: u.Quantity = get_weighted_sensitivity(
        transformation_input.continuum_sensitivity,
        transformation_input.weighting_factor,  # there is no weighting factor, so we use the default value set in the data class
    )

    weighted_spectral_sensitivity: u.Quantity = get_weighted_sensitivity(
        transformation_input.spectral_sensitivity,
        transformation_input.weighting_factor,  # there is no weighting factor, so we use the default value set in the data class
    )

    # warnings: There are no possible sensitivity warnings for this mode

    # the other results from ContinuumSensitivityTransformedResults will be returned as null
    return ContinuumSensitivityTransformedResults(
        weighted_continuum_sensitivity=weighted_continuum_sensitivity,
        weighted_spectral_sensitivity=weighted_spectral_sensitivity,
        warnings=warnings,
    )


def calculate_continuum_integration_time_results_without_weighting(
    transformation_input: IntegrationTimeTransformationsWithoutWeightingInput,
    warnings: list[str],
) -> ContinuumIntegrationTimeTransformedResults:
    """
    Uses an input containing values from Calculator and no Weighting to generate weighted results.
    This is used to calculate results for a custom subarray config.

    :param transformation_input: the transformation input including some of calculate results values.
    :type transformation_input: IntegrationTimeTransformationsWithoutWeightingInput
    :param warnings: the list of warnings to be returned.
    :type warnings: List of string
    :return: a data class object representing the transformed results.
    :rtype: ContinuumIntegrationTimeTransformedResults
    """

    # warnings: There are no possible sensitivity warnings for this mode

    # the other results from ContinuumIntegrationTimeTransformedResults will be returned as null
    return ContinuumIntegrationTimeTransformedResults(
        continuum_integration_time=transformation_input.continuum_integration_time,
        spectral_integration_time=transformation_input.spectral_integration_time,
        warnings=warnings,
    )


def calculate_continuum_sensitivities_subbands_results(
    subbands_input: ContinuumSensitivityTransformationsSubbandsInput,
    warnings: list[str],
) -> SubbandsContinuumSensitivityTransformedResults:
    """
    Uses an input containing values from Calculator and Weighting subbands to generate subbands calculated results to be displayed in the UI.

    :param subbands_input: the subband values from the calculator and weighting.
    :type subbands_input: data class ContinuumSensitivityTransformationsSubbandsInput
    :param warnings: the list of warnings to be returned.
    :type warnings: List of string
    :return: a data class object representing the subband transformed results.
    :rtype: SubbandsContinuumSensitivityTransformedResults
    """

    calculate_subbands = subbands_input.calculate_subbands
    weighting_subbands = subbands_input.weighting_subbands
    n_subbands = len(calculate_subbands)
    n_weighting_subbands = len(weighting_subbands)

    max_weighted_sensitivity = get_weighted_sensitivity(
        calculate_subbands[0]["sensitivity"],
        weighting_subbands[0].weighting_factor,
    )
    min_weighted_sensitivity = get_weighted_sensitivity(
        calculate_subbands[n_subbands - 1]["sensitivity"],
        weighting_subbands[0].weighting_factor,
    )
    min_max_weightedSensitivity = SubbandsItemsTransformedResults(
        min_value=min_weighted_sensitivity, max_value=max_weighted_sensitivity
    )

    max_confusion_noise: u.Quantity = get_confusion_noise(
        weighting_subbands[0].confusion_noise.value
    )
    min_confusion_noise: u.Quantity = get_confusion_noise(
        weighting_subbands[n_weighting_subbands - 1].confusion_noise.value
    )
    min_max_confusion_noise = SubbandsItemsTransformedResults(
        min_value=min_confusion_noise, max_value=max_confusion_noise
    )

    max_total_sensitivity = get_total_sensitivity(
        max_confusion_noise, max_weighted_sensitivity
    )
    min_total_sensitivity = get_total_sensitivity(
        min_confusion_noise, min_weighted_sensitivity
    )
    min_max_total_sensitivity = SubbandsItemsTransformedResults(
        min_value=min_total_sensitivity, max_value=max_total_sensitivity
    )

    max_synthesized_beam = get_synthesized_beam_size(
        weighting_subbands[0].beam_size.beam_min_scaled,
        weighting_subbands[0].beam_size.beam_maj_scaled,
    )
    min_synthesized_beam = get_synthesized_beam_size(
        weighting_subbands[n_weighting_subbands - 1].beam_size.beam_min_scaled,
        weighting_subbands[n_weighting_subbands - 1].beam_size.beam_maj_scaled,
    )
    min_max_synthesized_beam = SubbandsBeamSizeTransformedResults(
        min_value=min_synthesized_beam, max_value=max_synthesized_beam
    )

    max_SBS_sensitivity = get_surface_brightness_sensitivity(
        weighting_subbands[0].sbs_conv_factor, max_total_sensitivity
    )
    min_SBS_sensitivity = get_surface_brightness_sensitivity(
        weighting_subbands[n_weighting_subbands - 1].sbs_conv_factor,
        min_total_sensitivity,
    )
    min_max_max_SBS_sensitivity = SubbandsItemsTransformedResults(
        min_value=min_SBS_sensitivity, max_value=max_SBS_sensitivity
    )

    # subbands warnings
    max_sensitivity_limit_reached = sensitivity_limit_reached(
        max_confusion_noise, max_total_sensitivity
    )
    min_sensitivity_limit_reached = sensitivity_limit_reached(
        min_confusion_noise, min_total_sensitivity
    )

    if max_sensitivity_limit_reached:
        warnings.append({"max_sensitivity": SENS_LIMIT_WARNING})

    if min_sensitivity_limit_reached:
        warnings.append({"min_sensitivity": SENS_LIMIT_WARNING})

    return SubbandsContinuumSensitivityTransformedResults(
        weighted_sensitivity_per_subband=min_max_weightedSensitivity,
        confusion_noise_per_subband=min_max_confusion_noise,
        total_sensitivity_per_subband=min_max_total_sensitivity,
        synthesized_beam_size_per_subband=min_max_synthesized_beam,
        surface_brightness_sensitivity_per_subband=min_max_max_SBS_sensitivity,
        warnings=warnings,
    )


def sensitivity_limit_reached(
    confusion_noise: Quantity | str, total_sensitivity: Quantity | str
) -> bool:
    """
    Checks whether the total sensitivity is approaching confusion noise limit.
    The confusion noise limit is when the total sensitivity equals the confusion noise, so we
    define approaching as any sensitivity within 2 * confusion noise

    :param confusion_noise: the calculated confusion noise for the observation
    :type confusion_noise: float or a string such as "N/A (no beam information available)"
    :param total_sensitivity: the calculated total sensitivity for the observation
    :type total_sensitivity: float or a string such as "N/A (no beam information available)"
    :return: True if the total sensitivity is near the limit - False otherwise
    :rtype: boolean
    """

    if isinstance(confusion_noise, str) or isinstance(total_sensitivity, str):
        return False

    # Ensure confusion_noise is valid and positive
    if not isinstance(confusion_noise, Quantity) or confusion_noise.value <= 0:
        return False

    if confusion_noise.unit.is_equivalent(u.Jy / u.beam):
        confusion_noise = confusion_noise.to(u.Jy / u.beam)
    else:
        confusion_noise = confusion_noise.to(u.Jy)

    # check total sensitivity is in Jy/beam or Jy and to base unit
    if total_sensitivity.unit.is_equivalent(u.Jy / u.beam):
        total_sensitivity = total_sensitivity.to(u.Jy / u.beam)
    else:
        total_sensitivity = total_sensitivity.to(u.Jy)
    # Check if sensitivity is within (confusion_noise, 2 * confusion_noise)
    return confusion_noise.value < total_sensitivity.value < 2 * confusion_noise.value


def calculate_zoom_results(
    transformation_input: ZoomSensitivitiesTransformationsInput, warnings: list[str]
) -> ZoomSensitivityTransformedResults:
    """
    Uses an input containing values from Calculator and Weighting results to generate calculated results to be displayed in the UI.

    :param transformation_input: the transformation input including some of calculate and weighting results values.
    :type transformation_input: ZoomSensitivitiesTransformationsInput
    :return: a data class object representing the transformed results.
    :rtype: ZoomSensitivityTransformedResults
    """
    weighted_spectral_sensitivity = get_weighted_sensitivity(
        transformation_input.spectral_sensitivity,
        transformation_input.weighting_factor,
    )

    spectral_confusion_noise = get_confusion_noise(
        transformation_input.confusion_noise,
        transformation_input.min_beam_size,
        transformation_input.maj_beam_size,
    )

    total_spectral_sensitivity = get_total_sensitivity(
        spectral_confusion_noise, weighted_spectral_sensitivity
    )
    spectral_synthesized_beam_size = get_synthesized_beam_size(
        transformation_input.min_beam_size,
        transformation_input.maj_beam_size,
    )
    spectral_surface_brightness_sensitivity = get_surface_brightness_sensitivity(
        transformation_input.sbs_conv_factor, total_spectral_sensitivity
    )

    # TODO handle warnings

    result = ZoomSensitivityTransformedResults(
        weighted_spectral_sensitivity=weighted_spectral_sensitivity,
        spectral_confusion_noise=spectral_confusion_noise,
        total_spectral_sensitivity=total_spectral_sensitivity,
        spectral_synthesized_beam_size=spectral_synthesized_beam_size,
        spectral_surface_brightness_sensitivity=spectral_surface_brightness_sensitivity,
        warnings=warnings,
    )

    return result


def calculate_zoom_results_without_weighting(
    transformation_input: SensitivitiesTransformationsWithoutWeightingInput,
    warnings: list[str],
) -> ZoomSensitivityTransformedResults:
    """
    Uses an input containing values from Calculator and no Weighting to generate calculated results to be displayed in the UI.
    This is used to calculate results for a custom subarray config.

    :param transformation_input: the transformation input including some of calculate results values.
    :type transformation_input: SensitivitiesTransformationsWithoutWeightingInput
    :param warnings: the list of warnings to be returned.
    :type warnings: List of string
    :return: a data class object representing the transformed results.
    :rtype: ZoomSensitivityTransformedResults
    """

    weighted_spectral_sensitivity: u.Quantity = get_weighted_sensitivity(
        transformation_input.spectral_sensitivity,
        transformation_input.weighting_factor,  # there is no weighting factor, so we use the default value set in the data class
    )

    # warnings: There are no possible sensitivity warnings for this mode

    # the other results from ZoomSensitivityTransformedResults will be returned as null
    return ZoomSensitivityTransformedResults(
        weighted_spectral_sensitivity=weighted_spectral_sensitivity,
        warnings=warnings,
    )


def get_weighted_sensitivity(
    sensitivity: u.Quantity, weighting_factor: float
) -> u.Quantity:
    """Calculates weighted sensitivity based on sensitivity and weighting factor. It caters for different implementations in LOW and MID too.
    :param sensitivity: The sensitivity.
    :type sensitivity: Quantity
    :param weighting_factor: The weighting factor.
    :type weighting_factor: float
    :return: The weighted sensitivity as an Astropy Quantity object with units of Jy or Jy/beam.
    :rtype: astropy.units.Quantity
    """
    if not isinstance(sensitivity, Quantity):
        raise TypeError("Sensitivity must be an astropy Quantity.")
    # Calculates weighted sensitivity & converts to a Quantity object with units of uJy/beam
    if sensitivity.unit == u.Jy:
        return (sensitivity * weighting_factor).to(u.Jy)

    if sensitivity.unit != u.uJy / u.beam:
        sensitivity = sensitivity.to(
            u.uJy / u.beam
        )  # Ensure sensitivity is in uJy/beam
        weighted_sensitivity: u.Quantity = u.Quantity(
            sensitivity.value * weighting_factor, u.uJy / u.beam
        )
    else:
        weighted_sensitivity = sensitivity * weighting_factor

    # Convert weighted_sensitivity to Jy/beam to align with Mid
    return weighted_sensitivity.to(u.Jy / u.beam)


def get_confusion_noise(
    confusion_noise: float, min_beam_size: float = None, maj_beam_size: float = None
) -> u.Quantity:
    """
    Confusion noise comes from the noise (extra emission) that real objects in the Universe, which are in same direction as the target, add to the background noise whilst observing.
    If the beam is 'bigger' than the target, then faint objects which surround the target (but might be much much further away), will contribute to the noise threshold of the observation.
    If the beam is small enough (resolution high enough) then we will avoid this noise as we will be only looking at the target (or part of it).

    :param confusion_noise: The confusion noise value from weighting results.
    :type confusion_noise: float
    :param min_beam_size: The min_beam_size value from weighting results.
    :type min_beam_size: float
    :param maj_beam_size: The maj_beam_size value from weighting results.
    :type maj_beam_size: float
    :return: The confusion_noise as an Astropy Quantity object with units of Jy.
    :rtype: astropy.units.Quantity
    """
    if (
        min_beam_size is not None
        and maj_beam_size is not None
        and is_smaller_than_min_beam_size(min_beam_size, maj_beam_size, 1)
    ):
        return u.Quantity(0, u.Jy)
    else:
        return u.Quantity(confusion_noise, u.Jy)


def get_total_sensitivity(
    confusion_noise: u.Quantity, weighted_sensitivity: u.Quantity
) -> u.Quantity:
    """Calculates total sensitivity based on confusion noise and weighted sensitivity. It caters for different implementations in LOW and MID too.
    :param confusion_noise: The confusion noise value from weighting results.
    :type confusion_noise: Quantity
    :param weighted_sensitivity: The weighted sensitivity.
    :type weighted_sensitivity: Quantity
    :return: The total sensitivty as an Astropy Quantity object with units of Jy or Jy/beam.
    :rtype: astropy.units.Quantity
    """
    if confusion_noise.unit and weighted_sensitivity.unit == u.Jy:
        return np.hypot(confusion_noise, weighted_sensitivity).to(u.Jy)
    # Ensure input values have the correct units
    confusion_noise = confusion_noise.to(u.uJy)  # Ensure confusion_noise is in uJy
    weighted_sensitivity = weighted_sensitivity.to(
        u.uJy / u.beam
    )  # Ensure weighted_sensitivity is in uJy/beam
    # Convert confusion_noise to uJy/beam by assuming same beam size scaling is needed
    confusion_noise_per_beam = confusion_noise / u.beam
    # Compute total sensitivity using np.hypot which computes the quadratic sum
    total_sensitivity = np.hypot(confusion_noise_per_beam, weighted_sensitivity)
    # Convert to Jy/beam
    return total_sensitivity.to(u.Jy / u.beam)


def get_synthesized_beam_size(
    min_beam_size: float, maj_beam_size: float
) -> BeamSizeResult:
    """
    Calculates the synthesized beam size from a weighting response.

    :param min_beam_size: The min_beam_size value from weighting results.
    :type min_beam_size: float
    :param maj_beam_size: The maj_beam_size value from weighting results.
    :type maj_beam_size: float
    :return: A dictionary containing the synthesized beam size value and unit.
    :rtype: dict
    """
    # Convert beam sizes to Quantity objects with units of degrees
    min_beam: u.Quantity = min_beam_size * u.deg
    maj_beam: u.Quantity = maj_beam_size * u.deg

    return BeamSizeResult(
        beam_maj=maj_beam.to(u.arcsec),
        beam_min=min_beam.to(u.arcsec),
    )


def get_surface_brightness_sensitivity(
    sbs_conv_factor: float, total_sensitivity: u.Quantity
) -> u.Quantity:
    """
    Calculate the surface brightness sensitivity given the surface brightness sensitivity conversion factor and total sensitivity.

    :param sbs_conv_factor: The surface brightness sensitivity conversion factor
    :type sbs_conv_factor: float
    :param total_sensitivity: The total sensitivity of the observation.
    :type total_sensitivity: u.Quantity

    :return: The surface brightness sensitivity in micro-Kelvin (uK).
    :rtype: Quantity
    """
    if total_sensitivity.unit == u.Jy:
        return u.Quantity(total_sensitivity.value * sbs_conv_factor, u.K)
    total_sensitivity = total_sensitivity.to(u.Jy / u.beam)
    surface_brightness_sensitivity = total_sensitivity.value * sbs_conv_factor
    return u.Quantity(surface_brightness_sensitivity, u.K)


def calculate_beam_size(
    beam_min_scaled: float, beam_maj_scaled: float, precision: int
) -> float:
    """
    Calculate the beam size in units arcseconds.

    :param beam_min_scaled: The minor beam size in degrees.
    :type beam_min_scaled: float
    :param beam_maj_scaled: The major beam size in degrees.
    :type beam_maj_scaled: float
    :param precision: The number of decimal places to display the result to.
    :type precision: int
    :return: The calculated beam size in square arcseconds.
    :rtype: float
    """
    # Convert beam sizes to Quantity objects with units of degrees
    beam_min = beam_min_scaled * u.deg
    beam_maj = beam_maj_scaled * u.deg

    # Convert beam sizes to arcseconds
    beam_min_arcsec = beam_min.to(u.arcsec)
    beam_maj_arcsec = beam_maj.to(u.arcsec)

    # Calculate beam size in square arcseconds
    beam_size_in_arcseconds = round(
        (beam_maj_arcsec * beam_min_arcsec).value, precision
    )

    return round(beam_size_in_arcseconds, 1)


def is_smaller_than_min_beam_size(
    min_beam_size: float, maj_beam_size: float, precision: int
) -> bool:
    """
    Checks if the beam size is smaller than the minimum beam size limit.

    :param min_beam_size: The minor axe (FWHM) of the synthesized beam size from weighting results.
    :type min_beam_size: float
    :param maj_beam_size: The major axe (FWHM) of the synthesized beam size from weighting results.
    :type maj_beam_size: float
    :param precision: The number of decimal places to display the result to.
    :type precision: int
    :return: True if smaller than min beam size - False otherwise.
    :rtype: bool
    """
    arcsecs_beam_size = calculate_beam_size(
        min_beam_size,
        maj_beam_size,
        precision,
    )
    return arcsecs_beam_size < MINIMUM_GAUSSIAN_BEAM_LOW.value


def get_continuum_weighting_response(
    user_input: ContinuumWeightingRequestParams,
) -> ContinuumWeightingResponse:
    """
    Converts the input into the relevant calculation inputs, calls the calculation functions with them, then combines the results into the correct response body object.
    """
    dec = (
        user_input.dec if hasattr(user_input, "dec") else user_input.pointing_centre.dec
    )
    params = WeightingInput(
        freq_centre=user_input.freq_centre,
        dec=dec,
        weighting_mode=user_input.weighting_mode,
        robustness=user_input.robustness,
        subarray_configuration=user_input.subarray_configuration,
        spectral_mode=user_input.spectral_mode,
        taper=user_input.taper,
        telescope=user_input.telescope,
    )
    main_result = calculate_weighting(params)

    response = ContinuumWeightingResponse(
        weighting_factor=main_result.weighting_factor,
        sbs_conv_factor=main_result.surface_brightness_conversion_factor.value,
        confusion_noise=convert_confusion_noise_to_response(
            main_result.confusion_noise
        ),
        beam_size=_beam_size(main_result.beam_size),
    )

    if user_input.subband_freq_centres:
        subband_params = WeightingMultiInput(
            freq_centres=user_input.subband_freq_centres,
            dec=dec,
            weighting_mode=user_input.weighting_mode,
            robustness=user_input.robustness,
            subarray_configuration=user_input.subarray_configuration,
            spectral_mode=user_input.spectral_mode,
            taper=user_input.taper,
            telescope=user_input.telescope,
        )
        response.subbands = [
            SubbandWeightingResponse(
                subband_freq_centre=subband_result.freq_centre,
                weighting_factor=subband_result.weighting_factor,
                sbs_conv_factor=subband_result.surface_brightness_conversion_factor.value,
                confusion_noise=convert_confusion_noise_to_response(
                    subband_result.confusion_noise
                ),
                beam_size=_beam_size(subband_result.beam_size),
            )
            for subband_result in calculate_multi_weighting(subband_params)
        ]

    return response


def _beam_size(beam_size: BeamSize) -> BeamSizeResponse:
    result = BeamSizeResponse(
        beam_maj_scaled=beam_size.beam_maj.value,
        beam_min_scaled=beam_size.beam_min.value,
        beam_pa=beam_size.beam_pa.value,
    )

    return result


def get_zoom_weighting_response(
    params: ZoomRequestPrepared,
) -> list[SingleZoomWeightingResponse]:
    """
    Converts the input into the relevant calculation inputs, calls the calculation functions with them, then combines the results into the correct response body object.
    """
    # Currently the zoom calculator shows only the spectral results, so always use 'line' in the lookup table
    spectral_mode = WeightingSpectralMode.LINE

    params = WeightingMultiInput(
        freq_centres=params.freq_centres,
        dec=params.pointing_centre.dec,
        weighting_mode=params.weighting_mode,
        robustness=params.robustness,
        subarray_configuration=params.subarray_configuration,
        spectral_mode=spectral_mode,
        taper=params.taper,
        telescope=params.telescope,
    )

    result = []
    for single_result in calculate_multi_weighting(params):
        weighting = SingleZoomWeightingResponse(
            freq_centre=single_result.freq_centre,
            weighting_factor=single_result.weighting_factor,
            sbs_conv_factor=single_result.surface_brightness_conversion_factor.value,
            confusion_noise=convert_confusion_noise_to_response(
                single_result.confusion_noise
            ),
            beam_size=_beam_size(single_result.beam_size),
        )

        result.append(weighting)

    return result


def convert_confusion_noise_to_response(
    confusion_noise: ConfusionNoise,
) -> ConfusionNoiseResponse:
    # If confusion noise is labelled as an upper limit set it to
    # have a value of 0 Jy and a label of 'value'
    if confusion_noise.limit == Limit.UPPER:
        return ConfusionNoiseResponse(value=0, limit_type=Limit.VALUE.value)
    else:
        return ConfusionNoiseResponse(
            value=confusion_noise.value.value,
            limit_type=confusion_noise.limit.value,
        )


def get_subbands(
    n_subbands: int, freq_centre: float, bandwidth: float
) -> Tuple[List[float], float]:
    """
    Function to get the centres (and common width) of the N subbands of
    the spectral window

    Note: the units for the freq_centre and bandwidth should be the same, then the outputs will also be in those units

    :param n_subbands: Number of subbands to split the spectral window into
    :param freq_centre: Central frequency of the spectral window
    :param bandwidth: Total bandwidth of the spectral window
    :return: A tuple containing a list of the frequency centres for the subbands, and the subband width
    """
    left = freq_centre - (0.5 * bandwidth)
    subband_width = bandwidth / n_subbands
    return [
        left + ((i + 0.5) * subband_width) for i in range(n_subbands)
    ], subband_width


def thermal_sensitivity_on_unit(
    sensitivity: float,
    weighting_factor: float,
    sbs_conv_factor: float,
    confusion_noise: float,
    unit: u.Quantity,
) -> Quantity:
    """
    Converts sensitivity using provided unit and conversion and weighting factors

    :param sensitivity: The sensitivity
    :type sensitivity: float
    :param weighting_factor: Weighting factor.
    :type conversion_factor: float
    :param sbs_conv_factor: surface brightness conversion factor
    :type sbs_conv_factor: float
    :param confusion_noise: confusion noise
    :type confusion_noise: float
    :param unit: The unit selected by the user before the calculations started (TODO: use enum once defined).
    :type unit: str

    :return: Sensitivity converted
    :rtype: Quantity
    """
    if unit is None:
        raise ValueError(
            "Parameter 'unit' cannot be None. Please provide appropriate unit"
        )

    if weighting_factor == 0:
        raise ValueError("Weighting factor cannot be zero.")

    sensitivity = _to_base_quantity(sensitivity, unit)

    if sensitivity.value < confusion_noise:
        raise ValueError(
            f"Sensitivity ({sensitivity}) must be greater than or equal to confusion noise ({confusion_noise})."
        )

    conversion_factor = (
        sqrt(sensitivity.value**2 - confusion_noise**2) / weighting_factor
    )
    if sensitivity.unit == u.Jy / u.beam:
        return Quantity(conversion_factor, u.Jy)
    if sensitivity.unit == u.K:
        return Quantity(conversion_factor / sbs_conv_factor, u.Jy)

    raise ValueError(f"Unrecognized or unsupported unit: {sensitivity.unit}")


def _to_base_quantity(value: float, unit: str) -> Quantity:
    try:
        quantity = Quantity(value, u.Unit(unit))
        # Convert flux density variations to Jy/beam
        if quantity.unit.is_equivalent(u.Jy / u.beam):
            return quantity.to(u.Jy / u.beam)
        # Convert temperature variations to K (simple SI conversion)
        elif quantity.unit.is_equivalent(u.K):
            return quantity.to(u.K)
        return quantity
    except ValueError as error:
        raise ValueError(f"Invalid unit '{unit}' or value '{value}': {error}")


def is_beam_non_gaussian(
    weighting_mode: Weighting, robustness: float, taper: float = None
) -> bool:
    """This function detects whether the parameters provided define the calculation to use Gaussian beam logic or not
    Works for Mid and Low by additionally inspecting taper
    """
    robust = (
        weighting_mode == Weighting.ROBUST
        and robustness == 2
        and (taper is None or taper == 0)
    )
    natural = weighting_mode == Weighting.NATURAL and (taper is None or taper == 0)

    return natural or robust


def sub_band_to_frequency_array(
    number_of_sub_bands: int, bandwidth: float, central_frequency: float
) -> list[float]:
    """
    Generate list of frequency base on supplied subband, bandwidth and central frequencies

    :param number_of_sub_bands: number of subbands
    :type sensitivity: int
    :param bandwidth: supplied bandwidth
    :type bandwidth: float
    :param central_frequency: supplied central frequency
    :type central_frequency: float

    :return: list of frequencies
    :rtype: list[float]
    """
    freqs = []
    for i in range(number_of_sub_bands):
        freq = (
            (i / number_of_sub_bands) + (1 / (number_of_sub_bands * 2))
        ) * bandwidth + (central_frequency - bandwidth / 2)
        freqs.append(freq)
    return freqs


def subarray_configuration_from_input(
    user_input: dict, telescope: Telescope, err_msgs: list
) -> MIDArrayConfiguration | LOWArrayConfiguration | None:
    """
    If the user has given number of stations / number of antennas, extract the subarray_configuration from that.
    Otherwise, use the value given by the user.

    Validation has checked that one and only on of these fields is present in the input.

    :param user_input: a data class instance (ContinuumRequest) of the parameters given by the user
    :param telescope: the telescope class
    :param err_msgs: a list of error messages
    :return: the MIDArrayConfiguration or LOWArrayConfiguration Enum to use in the calculation or None for custom subarray
    """

    if "num_stations" in user_input:
        subarray = LOW_SUBARRAY_STORAGE.load_by_num_stations_or_antennas(
            num_stations=user_input["num_stations"]
        )
        if subarray:
            return LOWArrayConfiguration(subarray.label)
        return None
    if (
        "n_ska" in user_input
        and user_input["n_ska"] is not None
        and "n_meer" in user_input
        and user_input["n_meer"] is not None
    ):
        subarray = MID_SUBARRAY_STORAGE.load_by_num_stations_or_antennas(
            num_ska=user_input["n_ska"], num_meer=user_input["n_meer"]
        )
        if subarray:
            return MIDArrayConfiguration(subarray.label)
        return None
    else:
        return (
            MIDArrayConfiguration(user_input["subarray_configuration"])
            if telescope == Telescope.MID
            else EnumConversion.to_array_configuration(
                user_input["subarray_configuration"], err_msgs
            )
        )

"""
The service layer is responsible for turning validated inputs into the relevant calculation inputs,
calling any calculation functions and collating the results.
"""

import dataclasses
from copy import deepcopy
from dataclasses import asdict, dataclass, field
from math import isnan, nan
from typing import Optional, TypedDict

import astropy.units as u
from astropy.coordinates import SkyCoord
from astropy.units import Quantity

from ska_ost_senscalc.common.model import (
    ContinuumRequest,
    ContinuumSensitivitiesTransformationsInput,
    ContinuumSensitivityTransformedResults,
    ContinuumWeightingRequestParams,
    IntegrationTimeTransformationsWithoutWeightingInput,
    SensitivitiesTransformationsWithoutWeightingInput,
    SubbandsBeamSizeTransformedResults,
    SubbandsItemsTransformedResults,
    Weighting,
    WeightingSpectralMode,
    ZoomRequestPrepared,
)
from ska_ost_senscalc.common.service import (
    ContinuumWeightingResponse,
    SingleZoomWeightingResponse,
    _to_base_quantity,
    calculate_continuum_integration_time_results_without_weighting,
    calculate_continuum_results,
    calculate_continuum_results_without_weighting,
    get_confusion_noise,
    get_continuum_weighting_response,
    get_subbands,
    get_surface_brightness_sensitivity,
    get_synthesized_beam_size,
    get_total_sensitivity,
    get_weighted_sensitivity,
    get_zoom_weighting_response,
    is_beam_non_gaussian,
    sensitivity_limit_reached,
    thermal_sensitivity_on_unit,
)
from ska_ost_senscalc.common.spectropolarimetry import (
    SpectropolarimetryInput,
    SpectropolarimetryResults,
    get_spectropolarimetry_results,
)
from ska_ost_senscalc.mid.calculator import (
    calculate_integration_time,
    calculate_sensitivity,
    prepare_integration_input,
    prepare_sensitivity_input,
)
from ska_ost_senscalc.mid.models import ThermalData, ZoomResult
from ska_ost_senscalc.mid.validation import MID_CONTINUUM_CHANNEL_WIDTH_KHZ
from ska_ost_senscalc.subarray import SubarrayStorage
from ska_ost_senscalc.utilities import Telescope

subarray_storage = SubarrayStorage(Telescope.MID)


class SingleSubbandResponse(TypedDict):
    subband_freq_centre: Quantity
    sensitivity: Optional[Quantity]
    integration_time: Optional[Quantity]


class ContinuumSensitivityResponse(TypedDict):
    """
    Typed dictionary constrained to match the OpenAPI schema for the
    response body of a single continuum sensitivity calculation.
    """

    continuum_sensitivity: Optional[Quantity]
    continuum_integration_time: Optional[Quantity]

    spectral_sensitivity: Optional[Quantity]
    spectral_integration_time: Optional[Quantity]

    continuum_subband_sensitivities: Optional[list[SingleSubbandResponse]]
    continuum_subband_integration_times: Optional[list[SingleSubbandResponse]]

    spectropolarimetry_results: SpectropolarimetryResults


@dataclass
class SingleZoomSensitivityResponse:
    """
    Typed dictionary constrained to match the OpenAPI schema for the
    response body of a single zoom sensitivity calculation.
    """

    freq_centre: Quantity
    spectropolarimetry_results: SpectropolarimetryResults
    spectral_sensitivity: Quantity = None
    spectral_integration_time: Quantity = None

    warnings: list[str] = field(default_factory=list)


class ContinuumCalculateResponse(ContinuumSensitivityResponse):
    continuum_weighting: ContinuumWeightingResponse
    spectral_weighting: ContinuumWeightingResponse
    weighted_result: ContinuumSensitivityTransformedResults | ContinuumSensitivityTransformedResults


def convert_continuum_input_and_calculate(
    params: ContinuumRequest,
) -> dict:  #
    warnings = []
    params.target = params.pointing_centre

    # prepare weighting as we need them for both integration/sensitivity
    # TODO: add checking for custom for not sending weighting request

    # we only get weighting results if not a custom subarray
    continuum_weighting = None
    spectral_weighting = None
    if params.subarray_configuration is not None:
        weighting_request_params = ContinuumWeightingRequestParams(
            spectral_mode=WeightingSpectralMode.CONTINUUM,
            telescope=params.telescope,
            weighting_mode=params.weighting_mode,
            subarray_configuration=params.subarray_configuration,
            dec=params.pointing_centre.dec,
            freq_centre=params.freq_centre,
            taper=params.taper,
            robustness=params.robustness,
            subband_freq_centres=params.subband_freq_centres_hz,
        )
        continuum_weighting = get_continuum_weighting_response(weighting_request_params)
        # Spectural/Line Weighting
        spectral_weighting_request_params = deepcopy(weighting_request_params)
        spectral_weighting_request_params.spectral_mode = WeightingSpectralMode.LINE
        spectral_weighting = get_continuum_weighting_response(
            spectral_weighting_request_params
        )

    calculate_result = {}

    # Spectral results, which are returned in the continuum response body
    spectral_params = deepcopy(params)
    # For the spectral calculation, the bandwidth used in the calculation should be the effective resolution,
    # which is the intrinsic channel width multiplied by the spectral_averaging_factor
    effective_resolution_hz = (
        MID_CONTINUUM_CHANNEL_WIDTH_KHZ
        * 1e3
        * spectral_params.spectral_averaging_factor
    )
    spectral_params.bandwidth_hz = effective_resolution_hz

    if integration_time_s := params.integration_time_s:
        sensitivity_input = prepare_sensitivity_input(
            integration_time_s, **asdict(params)
        )
        sensitivity = calculate_sensitivity(sensitivity_input).to(u.Jy)
        calculate_result.update({"continuum_sensitivity": sensitivity[0]})

        # Calculate spectral sensitivity using effective resolution for bandwidth
        sensitivity_input = prepare_sensitivity_input(
            integration_time_s, **asdict(spectral_params)
        )
        spectral_sensitivity = calculate_sensitivity(sensitivity_input).to(u.Jy)
        calculate_result.update({"spectral_sensitivity": spectral_sensitivity[0]})

    if supplied_sensitivity := params.supplied_sensitivity:
        if params.subarray_configuration is not None:
            # prepare thermal sensitivity
            continuum_sensitivity = thermal_sensitivity_on_unit(
                supplied_sensitivity,
                continuum_weighting.weighting_factor,
                continuum_weighting.sbs_conv_factor,
                continuum_weighting.confusion_noise.value,
                params.sensitivity_unit,
            )

            continuum_confusion_noise = _adjust_confusion_noise(
                params, continuum_weighting.confusion_noise.value
            )

            if continuum_confusion_noise >= continuum_sensitivity.value:
                warnings.append(_sens_limit_error(continuum_confusion_noise))

            integration_input = prepare_integration_input(
                continuum_sensitivity.to(u.Jy).value, **asdict(params)
            )

            integration_time = calculate_integration_time(integration_input).to(u.s)
            calculate_result.update({"continuum_integration_time": integration_time[0]})

            # prepare thermal sensitivity for spectral
            spectral_sensitivity = thermal_sensitivity_on_unit(
                supplied_sensitivity,
                spectral_weighting.weighting_factor,
                spectral_weighting.sbs_conv_factor,
                spectral_weighting.confusion_noise.value,
                params.sensitivity_unit,
            )

            # Calculate spectral integration time using resolution for bandwidth
            integration_input = prepare_integration_input(
                spectral_sensitivity.to(u.Jy).value,
                **asdict(spectral_params),
            )
            spectral_integration_time = calculate_integration_time(
                integration_input
            ).to(u.s)

            calculate_result.update(
                {"spectral_integration_time": spectral_integration_time[0]}
            )

        else:
            # For custom subarrays, we don't calculate a thermal sensitivity, we do the calculation with the supplied sensitivity
            custom_continuum_integration_time = _calculate_custom_integration_time(
                supplied_sensitivity, params
            )
            calculate_result.update(
                {"continuum_integration_time": custom_continuum_integration_time[0]}
            )
            custom_spectral_integration_time = _calculate_custom_integration_time(
                supplied_sensitivity, spectral_params
            )
            calculate_result.update(
                {"spectral_integration_time": custom_spectral_integration_time[0]}
            )

    # Subbands - if subbands is 1 then we just return the main sensitivity calculation as the subband is the whole bandwidth
    if params.n_subbands != 1:
        subband_results = []
        subband_freq_centres_hz, subband_bandwidth = get_subbands(
            params.n_subbands, params.freq_centre_hz, params.bandwidth_hz
        )

        if integration_time_s := params.integration_time_s:
            for subband_freq_centre_hz in subband_freq_centres_hz:
                # Create the calculator for each subband result
                subband_params = deepcopy(params)
                subband_params.freq_centre_hz = subband_freq_centre_hz
                subband_params.bandwidth_hz = subband_bandwidth

                sensitivity_input = prepare_sensitivity_input(
                    integration_time_s, **asdict(subband_params)
                )
                sensitivity = calculate_sensitivity(sensitivity_input).to(u.Jy)

                subband_results.append(
                    {
                        "subband_freq_centre": u.Quantity(subband_freq_centre_hz, "Hz"),
                        "sensitivity": sensitivity[0],
                    }
                )
            calculate_result.update(
                {"continuum_subband_sensitivities": subband_results}
            )

        if params.supplied_sensitivity:
            # populate thermal array since we need to check first confusion noise
            subband_thermal_sensitivities = []
            if params.subband_supplied_sensitivities:
                for sensitivity, weighting in zip(
                    params.subband_supplied_sensitivities,
                    continuum_weighting.subbands,
                ):
                    subband_thermal_sensitivities.append(
                        thermal_sensitivity_on_unit(
                            sensitivity,
                            weighting.weighting_factor,
                            weighting.sbs_conv_factor,
                            weighting.confusion_noise.value,
                            params.subband_supplied_sensitivities_unit,
                        )
                    )
            else:
                for weighting in continuum_weighting.subbands:
                    subband_thermal_sensitivities.append(
                        thermal_sensitivity_on_unit(
                            params.supplied_sensitivity,
                            weighting.weighting_factor,
                            weighting.sbs_conv_factor,
                            weighting.confusion_noise.value,
                            params.sensitivity_unit,
                        )
                    )

            first_subband_confusion_noise = continuum_weighting.subbands[
                0
            ].confusion_noise.value

            if first_subband_confusion_noise > subband_thermal_sensitivities[0].value:
                warnings.append(_sens_limit_error(first_subband_confusion_noise))

            for subband_freq_centre_hz, subband_supplied_sensitivity in zip(
                subband_freq_centres_hz, subband_thermal_sensitivities
            ):
                # Create the calculator for each subband result
                subband_params = deepcopy(params)
                subband_params.freq_centre_hz = subband_freq_centre_hz
                subband_params.bandwidth_hz = subband_bandwidth

                integration_input = prepare_integration_input(
                    subband_supplied_sensitivity, **asdict(subband_params)
                )
                integration_time = calculate_integration_time(integration_input).to(u.s)

                subband_results.append(
                    {
                        "subband_freq_centre": u.Quantity(subband_freq_centre_hz, "Hz"),
                        "integration_time": integration_time[0],
                    }
                )
            calculate_result.update(
                {"continuum_subband_integration_times": subband_results}
            )

    spectropolarimetry_input = SpectropolarimetryInput(
        bandwidth=u.Quantity(params.bandwidth_hz, "Hz"),
        frequency=u.Quantity(params.freq_centre_hz, "Hz"),
        effective_channel_width=u.Quantity(effective_resolution_hz, "Hz"),
    )

    calculate_result.update(
        {
            "spectropolarimetry_results": get_spectropolarimetry_results(
                spectropolarimetry_input
            )
        }
    )
    weighted_result = None
    if params.subarray_configuration is not None:
        # weighted result
        # TODO: refactor integration time
        if integration_time_s:
            weighted_result = asdict(
                calculate_continuum_results(  # TODO Jack: this is only for integration time -> sensitivity - consider rename
                    ContinuumSensitivitiesTransformationsInput(
                        continuum_sensitivity=calculate_result["continuum_sensitivity"],
                        spectral_sensitivity=calculate_result["spectral_sensitivity"],
                        continuum_weighting_factor=continuum_weighting.weighting_factor,
                        spectral_weighting_factor=spectral_weighting.weighting_factor,
                        continuum_sbs_conv_factor=continuum_weighting.sbs_conv_factor,
                        spectral_sbs_conv_factor=spectral_weighting.sbs_conv_factor,
                        continuum_conf_noise=continuum_weighting.confusion_noise.value,
                        spectral_conf_noise=spectral_weighting.confusion_noise.value,
                        continuum_beam_min=continuum_weighting.beam_size.beam_min_scaled,
                        continuum_beam_maj=continuum_weighting.beam_size.beam_maj_scaled,
                        spectral_beam_min=spectral_weighting.beam_size.beam_min_scaled,
                        spectral_beam_maj=spectral_weighting.beam_size.beam_maj_scaled,
                    ),
                    warnings,
                )
            )
        elif supplied_sensitivity:
            weighted_result = asdict(
                _continuum_integration_time(
                    continuum_sensitivity,
                    calculate_result,
                    continuum_weighting,
                    spectral_weighting,
                    warnings,
                )
            )

    # custom subarray weighted results
    else:
        weighted_warnings = []
        if integration_time_s:
            continuum_sensitivities_without_weighting_input = (
                SensitivitiesTransformationsWithoutWeightingInput(
                    continuum_sensitivity=sensitivity,
                    spectral_sensitivity=spectral_sensitivity,
                )
            )
            weighted_result = asdict(
                calculate_continuum_results_without_weighting(
                    continuum_sensitivities_without_weighting_input, weighted_warnings
                )
            )
        elif supplied_sensitivity:
            continuum_integration_time_without_weighting_input = (
                IntegrationTimeTransformationsWithoutWeightingInput(
                    spectral_integration_time=custom_spectral_integration_time,
                    continuum_integration_time=custom_continuum_integration_time,
                )
            )
            weighted_result = asdict(
                calculate_continuum_integration_time_results_without_weighting(
                    continuum_integration_time_without_weighting_input,
                    weighted_warnings,
                )
            )

    if continuum_weighting:
        continuum_weighting = asdict(continuum_weighting)
    if spectral_weighting:
        spectral_weighting = asdict(spectral_weighting)

    combined_transformed_result = {
        **calculate_result,
        "continuum_weighting": continuum_weighting,
        "spectral_weighting": spectral_weighting,
        "weighted_result": weighted_result,
    }

    return combined_transformed_result


def _calculate_custom_integration_time(
    supplied_sensitivity: float, params: ContinuumRequest
) -> Quantity:
    supplied_sensitivity_jy = Quantity(
        supplied_sensitivity, params.sensitivity_unit
    ).to(u.Jy / u.beam)
    integration_input = prepare_integration_input(
        supplied_sensitivity_jy.value, **asdict(params)
    )
    return calculate_integration_time(integration_input)


def _continuum_integration_time(
    sensitivity: Quantity,
    calculate_result,
    continuum_weighting: ContinuumWeightingResponse,
    spectral_weighting: ContinuumWeightingResponse,
    warnings: list[str] = None,
) -> ContinuumSensitivityTransformedResults:
    continuum_confusion_noise = get_confusion_noise(
        confusion_noise=continuum_weighting.confusion_noise.value,
        maj_beam_size=continuum_weighting.beam_size.beam_maj_scaled,
        min_beam_size=continuum_weighting.beam_size.beam_min_scaled,
    )

    continuum_synthesized_beam_size = get_synthesized_beam_size(
        continuum_weighting.beam_size.beam_min_scaled,
        continuum_weighting.beam_size.beam_maj_scaled,
    )

    continuum_integration_time = calculate_result["continuum_integration_time"]

    spectral_confusion_noise = get_confusion_noise(
        confusion_noise=spectral_weighting.confusion_noise.value,
        maj_beam_size=spectral_weighting.beam_size.beam_maj_scaled,
        min_beam_size=spectral_weighting.beam_size.beam_min_scaled,
    )

    spectral_synthesized_beam_size = get_synthesized_beam_size(
        spectral_weighting.beam_size.beam_min_scaled,
        spectral_weighting.beam_size.beam_maj_scaled,
    )

    spectral_integration_time = calculate_result["spectral_integration_time"]

    weighted_result = ContinuumSensitivityTransformedResults(
        continuum_confusion_noise=continuum_confusion_noise,
        continuum_synthesized_beam_size=continuum_synthesized_beam_size,
        continuum_integration_time=continuum_integration_time,
        spectral_confusion_noise=spectral_confusion_noise,
        spectral_synthesized_beam_size=spectral_synthesized_beam_size,
        spectral_integration_time=spectral_integration_time,
    )

    # subbands
    if (
        calculate_result.get("continuum_subband_integration_times")
        and continuum_weighting.subbands
    ):
        max_time = calculate_result["continuum_subband_integration_times"][0][
            "integration_time"
        ]
        min_time = calculate_result["continuum_subband_integration_times"][-1][
            "integration_time"
        ]

        max_synthesized_beam = get_synthesized_beam_size(
            maj_beam_size=continuum_weighting.subbands[0].beam_size.beam_maj_scaled,
            min_beam_size=continuum_weighting.subbands[0].beam_size.beam_min_scaled,
        )

        min_synthesized_beam = get_synthesized_beam_size(
            maj_beam_size=continuum_weighting.subbands[-1].beam_size.beam_maj_scaled,
            min_beam_size=continuum_weighting.subbands[-1].beam_size.beam_min_scaled,
        )

        # create datacalss
        confusion_noise_per_subband = SubbandsItemsTransformedResults(
            max_value=continuum_weighting.subbands[0].confusion_noise.value * u.Jy,
            min_value=continuum_weighting.subbands[-1].confusion_noise.value * u.Jy,
        )

        synthesized_beam_size_per_subband = SubbandsBeamSizeTransformedResults(
            max_value=max_synthesized_beam, min_value=min_synthesized_beam
        )

        integration_time_per_subband = SubbandsItemsTransformedResults(
            max_value=max_time, min_value=min_time
        )

        weighted_result.confusion_noise_per_subband = confusion_noise_per_subband
        weighted_result.synthesized_beam_size_per_subband = (
            synthesized_beam_size_per_subband
        )

        weighted_result.integration_time_per_subband = integration_time_per_subband

    # For sensitivity -> integration time we need to compare the entered (total) sensitivity with the up to
    # four confusion noise values that might be shown (full bandwidth, two confusion-noise values and spectral)
    compare_max_confusion_noise_jy = [
        continuum_weighting.confusion_noise.value,
        spectral_weighting.confusion_noise.value,
    ]

    max_confusion_noise_jy = max(compare_max_confusion_noise_jy)

    if calculate_result.get(
        "continuum_subband_integration_times"
    ) or calculate_result.get("continuum_subband_sensitivities"):
        if (
            calculate_result.get("continuum_subband_integration_times")
            and continuum_weighting.subbands
        ):
            compare_max_confusion_noise_jy.append(
                continuum_weighting.subbands[0].confusion_noise.value
            )
            compare_max_confusion_noise_jy.append(
                continuum_weighting.subbands[-1].confusion_noise.value
            )
        max_confusion_noise_jy = max(compare_max_confusion_noise_jy)

    if sensitivity_limit_reached(
        confusion_noise=max_confusion_noise_jy,
        total_sensitivity=sensitivity.value,
    ):
        msg = _sens_limit_error(spectral_confusion_noise)
        warnings.append(msg)

    weighted_result.warnings = warnings

    return weighted_result


def get_zoom_calculate_response(
    params: ZoomRequestPrepared,
) -> dict:
    """
    Extract the params from the request, convert them into the relevant calculator inputs,
    perform the calculations and collect the results into the response body.
    """
    # Parse the target
    target = SkyCoord(
        params.pointing_centre,
        frame="icrs",
        unit=(u.hourangle, u.deg),
    )
    params.target = target

    calculations = []

    if integration_time_s := params.integration_time_s:
        for zoom_freq_centre_hz, zoom_spectral_resolution_hz, total_bandwidth_hz in zip(
            params.freq_centres_hz,
            params.spectral_resolutions_hz,
            params.total_bandwidths_hz,
        ):
            zoom_params = dataclasses.replace(params)
            zoom_params.freq_centre_hz = zoom_freq_centre_hz
            effective_resolution_hz = (
                zoom_spectral_resolution_hz * params.spectral_averaging_factor
            )
            zoom_params.bandwidth_hz = effective_resolution_hz
            sensitivity_input = prepare_sensitivity_input(
                integration_time_s, **asdict(zoom_params)
            )
            sensitivity = calculate_sensitivity(sensitivity_input).to(u.Jy)

            spectropolarimetry_input = SpectropolarimetryInput(
                bandwidth=Quantity(total_bandwidth_hz, "Hz"),
                frequency=Quantity(zoom_freq_centre_hz, "Hz"),
                effective_channel_width=Quantity(effective_resolution_hz, "Hz"),
            )
            spectropolarimetry_results = get_spectropolarimetry_results(
                spectropolarimetry_input
            )

            single = SingleZoomSensitivityResponse(
                freq_centre=zoom_freq_centre_hz * u.Hz,
                spectral_sensitivity=sensitivity[0],
                spectropolarimetry_results=spectropolarimetry_results,
            )
            calculations.append(asdict(single))

    # We need them early for sensitivities calculations
    weightings = get_zoom_weighting_response(params)
    thermals = []

    if params.supplied_sensitivities:
        assert (
            len(params.freq_centres_hz)
            == len(params.spectral_resolutions_hz)
            == len(params.supplied_sensitivities)
            == len(params.total_bandwidths_hz)
        )

        for i in range(len(params.freq_centres_hz)):
            zoom_freq_centre_hz = params.freq_centres_hz[i]
            zoom_spectral_resolution_hz = params.spectral_resolutions_hz[i]
            zoom_supplied_sensitivity = params.supplied_sensitivities[i]
            total_bandwidth_hz = params.total_bandwidths_hz[i]
            try:
                weighting = weightings[i]

                sensitivity = _to_base_quantity(
                    zoom_supplied_sensitivity, params.sensitivity_unit
                )

                confusion_noise = _adjust_confusion_noise(
                    params, weighting.confusion_noise.value
                )

                _thermal_sensitivity = thermal_sensitivity_on_unit(
                    sensitivity.value,
                    weighting.weighting_factor,
                    weighting.sbs_conv_factor,
                    confusion_noise,
                    params.sensitivity_unit,
                )

                thermal = ThermalData(
                    sensitivity,
                    confusion_noise,
                    _thermal_sensitivity,
                )

                thermals.append(thermal)
            except IndexError:
                _thermal_sensitivity = zoom_supplied_sensitivity

            zoom_params = dataclasses.replace(params)
            zoom_params.freq_centre_hz = zoom_freq_centre_hz
            effective_resolution_hz = (
                zoom_spectral_resolution_hz * params.spectral_averaging_factor
            )
            zoom_params.bandwidth_hz = effective_resolution_hz
            integration_input = prepare_integration_input(
                _thermal_sensitivity, **asdict(zoom_params)
            )
            integration_time = calculate_integration_time(integration_input).to(u.s)

            spectropolarimetry_input = SpectropolarimetryInput(
                bandwidth=Quantity(total_bandwidth_hz, "Hz"),
                frequency=Quantity(zoom_freq_centre_hz, "Hz"),
                effective_channel_width=Quantity(effective_resolution_hz, "Hz"),
            )

            spectropolarimetry_results = get_spectropolarimetry_results(
                spectropolarimetry_input
            )

            single = SingleZoomSensitivityResponse(
                freq_centre=zoom_freq_centre_hz * u.Hz,
                spectral_integration_time=integration_time[0],
                spectropolarimetry_results=spectropolarimetry_results,
            )
            calculations.append(asdict(single))

    weighted_result = _zoom_weighted_result(params, calculations, weightings, thermals)
    res = {
        "calculate": calculations,
        "weighting": {"spectral_weighting": [asdict(w) for w in weightings]},
        "weighted_result": weighted_result,
    }
    cleaned_results = _clean_none_fileds(res)

    return cleaned_results


def _zoom_weighted_result(
    params: ZoomRequestPrepared,
    calculations: list[SingleZoomSensitivityResponse],
    weightings: list[SingleZoomWeightingResponse],
    thermals: list[ThermalData],
) -> ZoomResult:
    if params.integration_time_s:
        if params.subarray_configuration is None:
            return _zoom_custom_sensitivity(calculations)
        else:
            if is_beam_non_gaussian(
                params.weighting_mode, params.robustness, params.taper
            ):
                return _zoom_sensitivity_non_gaussian(calculations, weightings)
            else:
                return _zoom_sensitivity(calculations, weightings)

    else:
        if params.subarray_configuration is None:
            return _zoom_custom_integration(calculations)
        else:
            warnings = []
            calculations = deepcopy(calculations)
            weightings = deepcopy(weightings)
            for i, thermal in enumerate(thermals):
                if thermal.confusion_noise > thermal.sensitivity.value:
                    # TODO: not reachable due to thermal_sensitivity_on_unit throwing ValueError - need reviewing
                    msg = _sens_limit_error(Quantity(thermal.confusion_noise, u.Jy))
                    warnings.append(msg)

                    # We are not processing them
                    calculations.pop(i)
                    weightings.pop(i)

            if _is_zoom_beam_non_gaussian(params):
                res = _zoom_integration_time_non_gaussian(calculations)
            else:
                res = _zoom_integration_time(calculations, weightings, thermals)

            res.extend(warnings)
            return res


def _zoom_custom_integration(
    calculations: list[SingleZoomSensitivityResponse],
) -> list[ZoomResult]:
    res = []
    for calculation in calculations:
        zoom_result = ZoomResult(
            spectral_integration_time=calculation["spectral_integration_time"],
        )

        res.append(asdict(zoom_result))

    return res


def _zoom_sensitivity(
    calculations: list[SingleZoomSensitivityResponse],
    weightings: list[SingleZoomWeightingResponse],
) -> list[ZoomResult]:
    res = []

    for calculation, weighting in zip(calculations, weightings):
        try:
            weighted_spectral_sensitivity = get_weighted_sensitivity(
                calculation["spectral_sensitivity"],
                weighting.weighting_factor,
            )
        except KeyError:
            weighted_spectral_sensitivity = nan

        min_beam_size = weighting.beam_size.beam_min_scaled
        maj_beam_size = weighting.beam_size.beam_maj_scaled
        spectral_confusion_noise = get_confusion_noise(
            weighting.confusion_noise.value,
            min_beam_size,
            maj_beam_size,
        )
        total_spectral_sensitivity = get_total_sensitivity(
            spectral_confusion_noise, weighted_spectral_sensitivity
        )
        spectral_synthesized_beam_size = get_synthesized_beam_size(
            min_beam_size,
            maj_beam_size,
        )
        spectral_surface_brightness_sensitivity = get_surface_brightness_sensitivity(
            weighting.sbs_conv_factor,
            total_spectral_sensitivity,
        )

        warnings = []
        if sensitivity_limit_reached(
            confusion_noise=spectral_confusion_noise,
            total_sensitivity=total_spectral_sensitivity,
        ):
            msg = _sens_limit_warning(spectral_confusion_noise)
            warnings.append(msg)

        zoom_result = ZoomResult(
            weighted_spectral_sensitivity=weighted_spectral_sensitivity,
            warnings=warnings,
            spectral_confusion_noise=spectral_confusion_noise,
            total_spectral_sensitivity=total_spectral_sensitivity,
            spectral_synthesized_beam_size=spectral_synthesized_beam_size,
            spectral_surface_brightness_sensitivity=spectral_surface_brightness_sensitivity,
        )
        res.append(asdict(zoom_result))

    return res


def _zoom_sensitivity_non_gaussian(
    calculations: list[SingleZoomSensitivityResponse],
    weightings: list[SingleZoomWeightingResponse],
) -> list[ZoomResult]:
    res = []
    for calculation, weighting in zip(calculations, weightings):
        zoom_result = ZoomResult(
            weighted_spectral_sensitivity=calculation.get("spectral_sensitivity", nan)
            * weighting.weighting_factor,
        )
        res.append(asdict(zoom_result))

    return res


def _zoom_integration_time(
    calculations: list[SingleZoomSensitivityResponse],
    weightings: list[SingleZoomWeightingResponse],
    thermals: list[ThermalData],
) -> list[ZoomResult]:
    res = []

    for calculation, weighting, thermal in zip(calculations, weightings, thermals):
        spectral_integration_time = calculation["spectral_integration_time"]

        min_beam_size = weighting.beam_size.beam_min_scaled
        maj_beam_size = weighting.beam_size.beam_maj_scaled
        spectral_confusion_noise = get_confusion_noise(
            weighting.confusion_noise.value,
            min_beam_size,
            maj_beam_size,
        )

        spectral_synthesized_beam_size = get_synthesized_beam_size(
            min_beam_size,
            maj_beam_size,
        )

        warnings = []
        if sensitivity_limit_reached(spectral_confusion_noise, thermal.sensitivity):
            msg = _sens_limit_warning(Quantity(thermal.confusion_noise, u.Jy))
            warnings.append(msg)

        zoom_result = ZoomResult(
            spectral_integration_time=spectral_integration_time,
            spectral_confusion_noise=spectral_confusion_noise,
            spectral_synthesized_beam_size=spectral_synthesized_beam_size,
            warnings=warnings,
        )
        res.append(asdict(zoom_result))

    return res


def _is_zoom_beam_non_gaussian(params: ZoomRequestPrepared) -> bool:
    """This function detects whether the parameters provided define the calculation to use Gaussian beam logic or not"""
    robust = (
        params.weighting_mode == Weighting.ROBUST
        and params.robustness == 2
        and params.taper == 0
    )
    natural = params.weighting_mode == Weighting.NATURAL and params.taper == 0

    return natural or robust


def _confusion_noise_included(params: ZoomRequestPrepared) -> bool:
    if params.subarray_configuration is None or _is_zoom_beam_non_gaussian(params):
        return False
    else:
        return True


def _adjust_confusion_noise(
    params: ZoomRequestPrepared, confusion_noise: float
) -> float:
    if _confusion_noise_included(params):
        return confusion_noise
    else:
        return 0


def _sens_limit_warning(confusion_noise: Quantity) -> str:
    return f"Warning: You are approaching the confusion limit ({confusion_noise}) given the synthesized beam-size and frequency."


def _sens_limit_error(confusion_noise: Quantity, sensitivity: Quantity) -> str:
    return f"Error: the entered sensitivity ({sensitivity}) is less than or equal to the confusion noise ({confusion_noise})"


def _zoom_integration_time_non_gaussian(
    calculations: list[SingleZoomSensitivityResponse],
) -> list[ZoomResult]:
    res = []
    for calculation in calculations:
        zoom_result = ZoomResult(
            spectral_integration_time=calculation["spectral_integration_time"]
        )

        res.append(asdict(zoom_result))

    return res


def _clean_none_fileds(d: dict | list[dict]) -> dict | list:
    if type(d) == list:
        res = [_clean_none_fileds(elem) for elem in d]
        return res
    elif type(d) == dict:
        res = {}
        for k, v in d.items():
            cleaned = _clean_none_fileds(v)
            if cleaned is not None and not (
                isinstance(cleaned, float) and isnan(cleaned)
            ):
                res[k] = cleaned
        return res
    else:
        return d


# TODO: leaving it for future if need to be
def _replace_na_fileds(d: dict | list[dict]) -> dict | list:
    if type(d) == list:
        res = [_replace_na_fileds(elem) for elem in d]
        return res
    elif type(d) == dict:
        res = {}
        for k, v in d.items():
            cleaned = _replace_na_fileds(v)
            if isinstance(cleaned, float) and isnan(cleaned):
                res[k] = None
            else:
                res[k] = cleaned

        return res
    else:
        return d


def _zoom_custom_sensitivity(
    calculations: list[SingleZoomSensitivityResponse],
) -> list[ZoomResult]:
    res = []
    for calculation in calculations:
        zoom_result = ZoomResult(
            weighted_spectral_sensitivity=calculation["spectral_sensitivity"],
        )

        res.append(asdict(zoom_result))

    return res


def get_subarray_response():
    """
    return the appropriate subarray objects
    """
    return [
        {
            "name": subarray.name,
            "label": subarray.label,
            "n_ska": subarray.n_ska,
            "n_meer": subarray.n_meer,
        }
        for subarray in subarray_storage.list()
    ]

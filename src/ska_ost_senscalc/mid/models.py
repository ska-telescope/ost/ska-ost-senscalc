from dataclasses import dataclass, field

from astropy.units import Quantity


@dataclass
class SensitivityInput:
    integration_time: Quantity  # the integration time
    sefd_array: Quantity  # the SEFD of the dish
    eta_system: float  # the system efficiency
    bandwidth: Quantity  # bandwidth in Hz
    tau: float  # atmospheric optical depth at given elevation


@dataclass
class IntegrationInput:
    sensitivity: Quantity  # required sensitivity
    sefd_array: Quantity  # the SEFD of the dish
    eta_system: float  # the system efficiency
    bandwidth: Quantity  # bandwidth in Hz
    tau: float  # atmospheric optical depth at given elevation
    elevation: Quantity  # elevation


@dataclass
class ThermalData:
    sensitivity: Quantity
    confusion_noise: Quantity
    thermal_sensitivity: Quantity


@dataclass
class ZoomResult:
    weighted_spectral_sensitivity: Quantity = None
    spectral_confusion_noise: Quantity = None
    total_spectral_sensitivity: Quantity = None
    spectral_synthesized_beam_size: Quantity = None
    spectral_surface_brightness_sensitivity: Quantity = None
    spectral_integration_time: Quantity = None

    warnings: list[str] = field(default_factory=list)

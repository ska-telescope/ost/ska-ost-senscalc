"""
These functions map to the API paths, with the returned value being the API response

Connexion maps the function name to the operationId in the OpenAPI document path
"""

import logging
from http import HTTPStatus

from ska_ost_senscalc.common.api import ResponseTuple, error_handler
from ska_ost_senscalc.common.model import ContinuumRequest, ZoomRequest
from ska_ost_senscalc.mid.service import (
    ContinuumCalculateResponse,
    SingleZoomSensitivityResponse,
    convert_continuum_input_and_calculate,
    get_subarray_response,
    get_zoom_calculate_response,
)
from ska_ost_senscalc.mid.validation import (
    validate_and_set_defaults_for_continuum,
    validate_and_set_defaults_for_zoom,
)

LOGGER = logging.getLogger("senscalc")


@error_handler
def continuum_calculate(
    **kwargs,
) -> ResponseTuple[ContinuumCalculateResponse]:
    """
    Function which HTTP GET requests to /mid/continuum/calculate are routed to.

    Combines the request parameters for calculator and weighting calculations and returns a dictionary
    with the sensitivities, the weighting continuum and spectral results, and the transformed results which
    combine sensitivity with the weighting results for the respective spectral mode into a format easy to use
    for the front-end.

    :param kwargs: the HTTP parameters
    :return: a tuple of the response body (which is either a :class:`ska_ost_senscalc.mid.service.ContinuumCalculateResponse`
        or an :class:`ErrorResponse`) and HTTP status, which Connexion will wrap into a Response
    """
    params = ContinuumRequest(**kwargs)

    validated = validate_and_set_defaults_for_continuum(params)

    result = convert_continuum_input_and_calculate(validated)
    return result, HTTPStatus.OK


@error_handler
def zoom_calculate(**kwargs) -> ResponseTuple[list[SingleZoomSensitivityResponse]]:
    """
    Function which HTTP GET requests to /mid/zoom/calculate are routed to.

    Sends the requests parameters to the service layer and builds a Response
    from the calculator output.

    :param kwargs: the HTTP parameters
    :return: a tuple of the response body (which is either a :class:`ska_ost_senscalc.mid.service.SensitivityResponse`
        or an :class:`ErrorResponse`) and HTTP status, which Connexion will wrap into a Response
    """
    params = ZoomRequest(**kwargs)
    validated = validate_and_set_defaults_for_zoom(params)
    result = get_zoom_calculate_response(validated)
    return result, HTTPStatus.OK


def subarrays():
    """
    Function that GET requests to the /subarrays endpoint are mapped to.

    Returns a response containing a list of available subarrays
    """
    try:
        LOGGER.debug("Request received for MID subarrays")
        return (
            get_subarray_response(),
            HTTPStatus.OK,
        )
    except Exception as err:
        LOGGER.exception("Exception occurred with MID api.subarrays")
        return (
            {
                "title": "Internal Server Error",
                "detail": repr(err),
            },
            HTTPStatus.INTERNAL_SERVER_ERROR,
        )

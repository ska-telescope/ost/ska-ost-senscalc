import pandas as pd
import sqlite3
import numpy as np
import h5py
from loguru import logger

from ska_ost_senscalc.utilities import STATIC_DATA_PATH

logger.info("Reading data from SQLite database...")
# Connect to the sqlite database
db_path = STATIC_DATA_PATH / "lookups/ska_station_sensitivity_AAVS2.db"
con = sqlite3.connect(db_path)

# Read required data and convert to pandas dataframe
sql = f"SELECT azim_deg,za_deg,frequency_mhz,polarisation,lst,sensitivity FROM Sensitivity"
df = pd.read_sql_query(sql, con)

logger.info("Creating empty ndarray and setting up indexers...")
# Grab unique keys from the dataframe
# We will use these to create an n-dimensional array
f_mhz = np.sort(df['frequency_mhz'].unique())
lst   = np.sort(df['lst'].unique())
az    = np.sort(df['azim_deg'].unique())
za    = np.sort(df['za_deg'].unique())
pol   = np.sort(df['polarisation'].unique())
lst   = np.sort(df['lst'].unique())
s     = df['sensitivity'].unique()


# Create index arrays corresponding to unique keys
idx_f_mhz = np.arange(len(f_mhz), dtype='int')
idx_lst   = np.arange(len(lst), dtype='int')
idx_az    = np.arange(len(az), dtype='int')
idx_za    = np.arange(len(za), dtype='int')
idx_pol   = np.arange(len(pol), dtype='int')

# Create mapping between array values and indexes
f_map = dict(zip(f_mhz, idx_f_mhz))
l_map = dict(zip(lst,   idx_lst))
a_map = dict(zip(az,    idx_az))
z_map = dict(zip(za,    idx_za))
p_map = dict(zip(pol,   idx_pol))

# Now, we are ready to create a new n-dimensional array containing the data
# First, create an empty array
s_arr = np.zeros(shape=(len(lst), len(az), len(za), len(f_mhz),  len(pol)), dtype='float32')

logger.info("Filling ndarray with database entries...")
# Loop through rows of the table.
# Convert values for freq/lst/az/za/pol into indexes of the sensitivity array
# Then fill that cell with the row's sensitivity value
# This takes about a minute.
for idx_df, row in df.iterrows():
    ff = f_map[row['frequency_mhz']]
    ll = l_map[row['lst']]
    pp = p_map[row['polarisation']]
    aa = a_map[row['azim_deg']]
    zz = z_map[row['za_deg']]

    s_arr[ll, aa, zz, ff, pp] = row['sensitivity']

logger.info("Saving to file...")
# Finally, save the sensitivty array to HDF5
h5_path = STATIC_DATA_PATH / "lookups/ska_station_sensitivity_AAVS2.h5"
with h5py.File(h5_path, 'w') as h5:
    # Main datasets
    h5.create_dataset('a_over_t', data=s_arr)
    h5['a_over_t'].attrs['units'] = 'm^2/K'
    h5['a_over_t'].attrs['description'] = 'Sensitivity: A/T (area / system temperature, in [m^2/K] )'

    # Pre-compute SEFD array, as this is what is returned to user
    sefd = 2 * 1380.0 / np.asarray(s_arr)
    h5.create_dataset('sefd', data=sefd)
    h5['sefd'].attrs['units'] = 'Jy'
    h5['a_over_t'].attrs['description'] = 'Sensitivity: SEFD (System equivalent flux density, in [Jy] )'

    # Create + attach dimension scales
    d5 = h5.create_group('dimensions')
    for ii, (ds, u, d) in enumerate((('frequency', 'MHz', f_mhz),
                                     ('lst', 'hr', lst),
                                     ('azimuth', 'deg', az),
                                     ('zenith_angle', 'deg', za),
                                     ('polarisation', '', pol))
                                   ):

        # Create dataset for dimension scale
        d5.create_dataset(ds, data=d)
        d5[ds].make_scale(ds)
        d5[ds].attrs['units'] = u

        # Attach scale to primary datasets
        for dname in ('a_over_t', 'sefd'):
            h5[dname].dims[ii].label = ds
            h5[dname].dims[ii].attach_scale(d5[ds])

logger.info("Done.")
import os
import subprocess

import numpy as np
from astropy.io import ascii
from astropy.table import QTable, vstack
from astropy import units as u


def calculate(table_name="beam_weighting_mid", debug_table=False):
    """This script uses the rascil_sensitivity app to generate a table
    of point source sensitivities for a range of beam-weighting methods,
    target declinations, elevations, and integration times (track lengths)

    :param table_name: the root name of the table to be constructed.
    :type table_name: str
    :param debug_table: True for a table over a restricted range of parameters.
    :type debug_table: bool

    Before running, install rascil and define RASCIL and RASCIL_DATA
    environment variables.
    """

    # the rascil_sensitivity app

    # usage: rascil_sensitivity.py [-h]
    #                              [--use_dask USE_DASK]
    #                              [--imaging_npixel IMAGING_NPIXEL]
    #                              [--msfile MSFILE]
    #                              [--imaging_cellsize IMAGING_CELLSIZE]
    #                              [--imaging_oversampling IMAGING_OVERSAMPLING]
    #                              [--imaging_weighting IMAGING_WEIGHTING]
    #                              [--imaging_robustness [IMAGING_ROBUSTNESS ...]]
    #                              [--imaging_taper [IMAGING_TAPER ...]]
    #                              [--ra RA]
    #                              [--tsys TSYS]
    #                              [--efficiency EFFICIENCY]
    #                              [--diameter DIAMETER]
    #                              [--declination DECLINATION]
    #                              [--configuration CONFIGURATION]
    #                              [--subarray SUBARRAY]
    #                              [--rmax RMAX]
    #                              [--frequency FREQUENCY]
    #                              [--integration_time INTEGRATION_TIME]
    #                              [--time_range TIME_RANGE TIME_RANGE]
    #                              [--nchan NCHAN]
    #                              [--channel_width CHANNEL_WIDTH]
    #                              [--verbose VERBOSE]
    #                              [--results RESULTS]

    # delete final destination for look-up table if it exists
    outfile = "{table_name}.ecsv".format(table_name=table_name)
    if os.path.exists(outfile):
        os.system("rm %s" % outfile)

    # parameters
    configuration = "MIDR5"
    frequency = 1.4e9  # reference frequency, in Hz
    obsLength = 1.0  # total length of observation, in hours
    avgTimes = [10, 60]  # correlator averaging time (line and cont), in seconds
    nChans = [1, 30]  # number of channels used in line and continuum modes
    chanWidth = frequency * 0.01  # channel separation in continuum mode
    robusts = [-2, -1, 0, 1, 2]
    
    # use small parameter space for debugging
    # tapering values are in arcsec
    if debug_table:
        subarrays = ["MID_AA2_all.json"]
        declinations = [-30]
        tapers = np.array([0.0])
        weighting = ['robust']
    else:
        subarrays = [
            "MID_AA05_all.json",
            "MID_AA1_all.json",
            "MID_AA2_all.json",
            "MID_AAstar_all.json",
            "MID_AAstar_SKA_only.json",
            "MID_AA4_all.json",
            "MID_AA4_SKA_only.json",
            "MID_AA4_MeerKAT_only.json",
        ]
        declinations = [44, 30, 10, -10, -30, -50, -70, -90]
        tapers = np.array([0.0, 0.25, 1.0, 4.0, 16.0, 64.0, 256.0, 1024.0])
        #tapers = np.array([0.0, 0.25, 0.5, 1.0, 2.0, 4.0, 8.0, 16.0, 32.0,
        #                   64.0, 128.0, 256.0, 512.0, 1024.0, 1280.0])
        weighting = ['uniform', 'robust', 'natural']
        oversampling = 8.0

    # delete pre-existing temporary results if they're there for some reason
    if os.path.exists("temporary_mid_sensitivity.csv"):
        os.remove("temporary_mid_sensitivity.csv")

    # set merged_table to None so that we know we're on the first loop
    merged_table = None
        
    # loop over grid, run the app for each point, merge the results
    for subarray in subarrays:

        # remove large tapering values for AA05 and AA1
        if subarray == "MID_AA05_all.json" or subarray == "MID_AA1_all.json":
            tapersSub = tapers[:-2]
        else:
            tapersSub = np.copy(tapers)

        for declination in declinations:
            for nChan in nChans:
                for weight in weighting:
                    for taper in tapersSub:

                        print(
                            "subarray={subarray} "
                            "weighting={weight} "
                            "nchans={nChan} "
                            "dec={declination} "
                            "taper={taper}".format(
                                subarray=subarray,
                                weight=weight,
                                nChan=nChan,
                                declination=declination,
                                taper=taper,
                            )
                        )

                        # will assume that all simulations are done at transit
                        ha = 0.0
                        time_range = [
                            ha - obsLength / 2.0,
                            ha + obsLength / 2.0,
                        ]

                        # convert tapering value to radians
                        taperR = np.deg2rad(taper / 3600.0)

                        # use a cellsize appropriate to each tapering value
                        # 0 will cause the app to use the 'critical' value
                        if taperR == 0:
                            cellsize = None
                        else:
                            cellsize = taperR / oversampling

                        # select correlator averaging time
                        if nChan == nChans[0]:
                            avgTime = avgTimes[0]
                        else:
                            avgTime = avgTimes[1]
                            
                        # set up arguments
                        command = [
                            "python3 $RASCIL/rascil/apps/rascil_sensitivity.py "
                            "--configuration '{configuration}' "
                            "--subarray '{subarray}' "
                            "--frequency {frequency} "
                            "--declination {declination} "
                            "--integration_time {avgTime} "
                            "--time_range {time_range[0]} {time_range[1]} "
                            "--imaging_taper {taperR} "
                            "--imaging_npixel 1024 "
                            "--imaging_oversampling {oversampling} "
                            "--imaging_weighting {weight} "
                            "--nchan {nChan} "
                            "--channel_width {chanWidth} "
                            "--results temporary_mid".format(
                                configuration=configuration,
                                subarray=subarray,
                                frequency=frequency,
                                declination=declination,
                                avgTime=avgTime,
                                time_range=time_range,
                                taperR=taperR,
                                oversampling=oversampling,
                                weight=weight,
                                nChan=nChan,
                                chanWidth=chanWidth,
                            )
                        ]

                        # Insert cellsize if this is defined
                        # Otherwise, let RASCIL choose
                        if cellsize != None:
                            cellsizeString = " --imaging_cellsize " + str(cellsize)
                            command[0] = command[0] + cellsizeString

                        # Insert robust values into arguments
                        robustString = " --imaging_robustness " + " ".join([str(x) for x in robusts])
                        command[0] = command[0] + robustString

                        # The actual command we're trying to run will be shown in debug mode
                        if debug_table:
                            print(command)

                        # run rascil_sensitivity - using Popen as it
                        # redirects output and CTRL-Y works to abort
                        # the called script and this method - on a Mac
                        # anyway
                        pipe = subprocess.Popen(
                            command,
                            shell = True,
                            stdout = subprocess.PIPE,
                            stderr = subprocess.PIPE,
                        )
                        res = pipe.communicate()
                        
                        # read results from this run of the sensitivity app
                        table = ascii.read("temporary_mid_sensitivity.csv")
                    
                        # add important parameters to table
                        table["subarray"] = subarray
                        table["declination"] = declination
                        if nChan == 1:
                            table["spec_mode"] = 'line'
                        else:
                            table["spec_mode"] = 'continuum'

                        # need to add a column for the tapering in arcsec
                        # this makes it easier to extract the right rows
                        # needs to be done for each value of robust
                        if weight == 'robust':
                            table["taper_arcsec"] = np.repeat(taper, len(robusts))
                        else:
                            table["taper_arcsec"] = taper
                        
                        #table["time_start"] = time_range[0]
                        #table["time_end"] = time_range[1]

                        # add this loop's table to the master table if it exists
                        # otherwise, create it
                        # if creating, add the reference frequency to the metadata
                        if merged_table is not None:
                            merged_table = vstack([merged_table, table])
                        else:
                            merged_table = QTable(
                                table,
                                copy=False,
                                meta={'reference_frequency': frequency * u.Hz}
                            )

                        # clean up
                        os.system("rm temporary_mid_sensitivity.csv")

    # add units
    merged_table["declination"].unit = u.deg
    merged_table["pss_casa"].unit = u.Jy
    merged_table["cleanbeam_bmaj"].unit = u.deg
    merged_table["cleanbeam_bmin"].unit = u.deg
    merged_table["cleanbeam_bpa"].unit = u.deg

    # write master table to disk - only write out important columns
    table_to_file = merged_table[
        [
            "subarray",
            "weighting",
            "robustness",
            "taper_arcsec",
            "declination",
            "spec_mode",
            "pss_casa",
            "reltonat_casa",
            "cleanbeam_bmaj",
            "cleanbeam_bmin",
            "cleanbeam_bpa",
        ]
    ]

    ascii.write(table_to_file, outfile, delimiter=',', overwrite=True, format='ecsv')

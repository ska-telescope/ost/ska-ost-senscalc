import logging
import pprint
import sys
import datetime
from itertools import product
import pickle

import matplotlib.pyplot as plt
import numpy
from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.time import Time

from rascil.data_models import PolarisationFrame
from rascil.processing_components import (
    create_image_from_visibility,
    create_blockvisibility,
    create_named_configuration,
    plot_uvcoverage,
    qa_image,
    export_image_to_fits,
    fit_psf,
)
from rascil.workflows import (
    invert_list_rsexecute_workflow,
    sum_invert_results_rsexecute,
    weight_list_rsexecute_workflow,
    taper_list_rsexecute_workflow,
)
from rascil.workflows.rsexecute.execution_support import rsexecute
pp = pprint.PrettyPrinter()


def init_logging():
    logging.basicConfig(
        filename="sensitivity.log",
        filemode="a",
        format="%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s",
        datefmt="%H:%M:%S",
        level=logging.INFO,
    )


init_logging()
log = logging.getLogger("rascil-logger")
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))


start_frequencies = {"B1": 0.765e9, "B2": 1.36e9, "Ku":12.179e9}


weight_types = ["natural", "uniform", "robust"]


def simulate_bvis(
        band="B2", 
        ra=121.405354,
        declination=-30.712925,
        integration_time=740,  # s
        time_range=(-4.0, 4.0),  # Hour angle range in hours
        nchan=12,
        channel_width=1.2e7,  # Hz
        rmax=2e5,  # m
        use_dask=True,
        plot=False,
        **kwargs
    ):
    
    vis_polarisation_frame = PolarisationFrame("stokesI")

    rsexecute.set_client(use_dask=use_dask)

    rsexecute.run(init_logging)

    # Set up details of simulated observation
    start_frequency = start_frequencies[band]

    frequency = numpy.linspace(
        start_frequency, start_frequency + nchan * channel_width, nchan
    )
    channel_bandwidth = numpy.array(nchan * [channel_width])

    phasecentre = SkyCoord(
        ra=ra * u.deg, dec=declination * u.deg, frame="icrs", equinox="J2000"
    )

    config = create_named_configuration("MIDR5", rmax=rmax)

    time_rad = numpy.array(time_range) * numpy.pi / 12.0
    times = numpy.arange(
        time_rad[0], time_rad[1], integration_time * numpy.pi / 43200.0
    )
    # utc_time = Time("2000-01-01T00:00:00", format="isot", scale="utc")

    # Make a list of BlockVisibility's, one for each frequency.
    # This is actually a graph that we will compute later
    # Note that the weight of one sample is set to the time-bandwidth product
    bvis_list = [
        rsexecute.execute(create_blockvisibility)(
            config,
            times=times,
            phasecentre=phasecentre,
            polarisation_frame=vis_polarisation_frame,
            frequency=[frequency[channel]],
            channel_bandwidth=[channel_bandwidth[channel]],
            integration_time=integration_time,
            elevation_limit=numpy.deg2rad(15.0),
            weight=integration_time * channel_width,
        )
        for channel, freq in enumerate(frequency)]
    
    bvis_list = rsexecute.persist(bvis_list)

    if plot:
        plt.clf()
        local_bvis_list = rsexecute.compute(bvis_list, sync=True)
        plot_uvcoverage(local_bvis_list)
        #plt.show(block=False)

        log.info(
            f"Size of BlockVisibility for first channel: {local_bvis_list[0].nbytes * 2**-30:.6f}GB "
        )

    return bvis_list


def image_bvis(
    bvis_list,
    imaging_npixel=1024,
    imaging_cellsize=2e-7,
    imaging_weighting=None,
    robustness=0.,
    imaging_taper=None,  # 6e-7
    verbose=False, 
    **kwargs
    ):
    """Construct the PSF and calculate statistics

    :param args:
    :return: Results in dictionary
    """

    results = {}
    # Now make the model images (actually a graph)
    model_list = [
        rsexecute.execute(create_image_from_visibility)(
            bvis,
            npixel=imaging_npixel,
            cellsize=imaging_cellsize,
        )
        for bvis in bvis_list
    ]

    # Apply weighting
    if imaging_weighting is not None:
        bvis_list = weight_list_rsexecute_workflow(
            bvis_list, model_list, weighting=imaging_weighting, robustness=robustness
        )

    # Apply Gaussian taper
    if imaging_taper is not None:
        bvis_list = taper_list_rsexecute_workflow(
            bvis_list,
            size_required=imaging_taper,
        )

    # Now we can make the PSF
    psf_list = invert_list_rsexecute_workflow(
        bvis_list, model_list, context="ng", dopsf=True, do_wstacking=False
    )
    result = sum_invert_results_rsexecute(psf_list, split=2)

    psf, sumwt = rsexecute.compute(result, sync=True)

    qa_psf = qa_image(psf)
    log.info(f"PSF QA = {qa_psf}")
    for key in qa_psf.data:
        results[f"psf_{key}"] = qa_psf.data[key]
    
    log.info(f"Total time-bandwidth product = {sumwt[0][0]:.3f}")
    results["tb_product"] = sumwt[0][0]

    if verbose:
        export_image_to_fits(psf, "sensitivity_psf.fits")

    clean_beam = fit_psf(psf)
    log.info(f"Clean beam {clean_beam}")
    results["clean_beam"] = clean_beam

    return results


def simulate_params(**kwargs):
    """Simulate completely using a set of params
    """
    bvis = simulate_bvis(**kwargs)
    return image_bvis(bvis, **kwargs)


if __name__ == "__main__":
    log.info("sensitivity: Starting MID sensitivity simulation")
    starttime = datetime.datetime.now()
    log.info("Started : {}".format(starttime))
    log.info("Writing log to {}".format("sensitivity.log"))
    
    params_set = []
    for declination in numpy.linspace(-90, 0, 19):
        d_dec = {"declination": declination}
        for robust in numpy.linspace(-2.5, 2.5, 11):
            if robust == -2.5:
                d_weight = {"imaging_weighting": "uniform", "robustness": -2.5}
            elif robust == 2.5:
                d_weight = {"imaging_weighting": "natural", "robustness": 2.5}
            else:
                d_weight = {"imaging_weighting": "robust", "robustness": robust}
            params_set.append({**d_dec, **d_weight})
    
    with open("sim_output.txt", "w") as fileout, open("sim_output.csv", "w") as csvout:
        csvout.write("dec,weighting,robust,beam_maj,beam_min,beam_pa,sumwt\n")
        results = []
        for params in params_set:
            log.info("Start sim with params: {}".format(params))
            result = simulate_params(**params)
            results.append(result)
            output = "Dec: {:5.1f}; Weighting: {:7s} {:5.1f}; Beam: {:8f} {:8f} {:6.1f}; sumwt: {}".format(
                params['declination'], 
                params["imaging_weighting"], 
                params["robustness"],
                result['clean_beam']['bmaj']*3600,
                result['clean_beam']['bmin']*3600,
                result['clean_beam']['bpa'],
                result["tb_product"]
                )
            log.info(output)
            fileout.write(output+"\n")
            csvout.write("{:.0f},{},{:.1f},{:8f},{:8f},{:.1f},{}\n".format(
                params['declination'], 
                params["imaging_weighting"], 
                params["robustness"],
                result['clean_beam']['bmaj']*3600,
                result['clean_beam']['bmin']*3600,
                result['clean_beam']['bpa'],
                result["tb_product"]
                ))
    log.info("Finished : {}".format(datetime.datetime.now()))



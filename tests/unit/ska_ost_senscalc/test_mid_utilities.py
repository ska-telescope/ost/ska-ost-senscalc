"""
Unit tests for the ska_ost_senscalc.mid_utilities module.
"""

import astropy.units as u
import pytest
from astropy.coordinates import SkyCoord

import ska_ost_senscalc.mid_utilities as mid_utils
from ska_ost_senscalc.utilities import Celestial, DishType, TelParams, Utilities


def test_eta_bandpass():
    """Verify mid_utilities.eta_bandpass"""
    assert mid_utils.eta_bandpass() == 1.0


def test_eta_coherence():
    """Verify mid_utilities.eta_coherence"""
    # print("eta_coherence", mid_utils.eta_coherence(None, 8.0 * u.GHz))
    assert mid_utils.eta_coherence(8.0 * u.GHz) == pytest.approx(0.9945629318496559)


def test_eta_correlation():
    """Verify mid_utilities.eta_correlation"""
    assert mid_utils.eta_correlation() == pytest.approx(0.98)


def test_eta_digitisation():
    """
    Verify mid_utilities.BaseCalculator.eta_digitise
    """
    assert mid_utils.eta_digitisation("Band 1") == 0.999
    assert mid_utils.eta_digitisation("Band 2") == 0.999
    assert mid_utils.eta_digitisation("Band 3") == 0.998
    assert mid_utils.eta_digitisation("Band 5a") == 0.955
    assert mid_utils.eta_digitisation("Band 5b") == 0.955
    with pytest.raises(RuntimeError) as err:
        assert mid_utils.eta_digitisation("other")
    assert str(err.value) == "bad obs_band: other"


def test_eta_dish():
    """Validate mid_utilities.eta_dish"""
    assert mid_utils.eta_dish(
        0.5 * u.GHz, DishType.SKA1
    ) == TelParams.calculate_dish_efficiency(0.5 * u.GHz, DishType.SKA1)
    with pytest.raises(RuntimeError) as err:
        assert mid_utils.eta_dish(0.5 * u.GHz, "other")
    assert str(err.value) == "bad dish_type: other"


def test_eta_point():
    """
    Verify mid_utilities.BaseCalculator.eta_point

    """
    # print("eta_point 1", mid_utils.eta_point(2.0 * u.GHz, DishType.SKA1))
    # print("eta_point 2", mid_utils.eta_point(1.5 * u.GHz, DishType.MeerKAT))
    assert mid_utils.eta_point(2.0 * u.GHz, DishType.SKA1) == pytest.approx(
        0.9999016485055535
    )
    assert mid_utils.eta_point(1.5 * u.GHz, DishType.MeerKAT) == pytest.approx(
        0.9999520116676085
    )
    with pytest.raises(RuntimeError) as err:
        assert mid_utils.eta_point(0.5 * u.GHz, "other")
    assert str(err.value) == "bad dish_type: other"


def test_eta_rfi():
    """Validate mid_utilities.eta_rfi"""
    assert mid_utils.eta_rfi() == 1.00


def test_eta_system():
    """Validate mid_utilities.eta_system"""
    assert mid_utils.eta_system(0.5, 0.5, 0.5, 0.5, 0.5) == 0.5**5


def test_Tgal():
    """Validate mid_utilities.Tgal"""
    south_pole = SkyCoord(0.0, -90.0, unit="deg")
    assert mid_utils.Tgal(
        south_pole, 0.5 * u.GHz, DishType.SKA1, 2.75
    ) == Celestial().calculate_Tgal(south_pole, 0.5 * u.GHz, DishType.SKA1, 2.75)


def test_Trcv():
    """Validate mid_utilities.Trcv"""
    assert mid_utils.Trcv(
        0.5 * u.GHz, "Band 1", DishType.SKA1
    ) == TelParams.calculate_Trcv(0.5 * u.GHz, "Band 1", DishType.SKA1)


def test_Tsky():
    """Validate mid_utilities.Tsky"""
    location = TelParams.mid_core_location()
    south_pole = SkyCoord(0.0, -90.0, unit="deg")
    elevation = 90.0 * u.deg - (location.to_geodetic().lat - south_pole.icrs.dec)
    assert mid_utils.Tsky(5.0 * u.K, 0.5 * u.GHz, elevation, 10).to_value(
        u.K
    ) == pytest.approx(18.9435658)


def test_Tspl():
    """Validate mid_utilities.Tspl"""
    assert mid_utils.Tspl(DishType.SKA1) == TelParams.calculate_Tspl(DishType.SKA1)


def test_Tsys_dish():
    """Validate mid_utilities.Tsys_dish"""
    assert mid_utils.Tsys_dish(
        10.0 * u.K, 10.0 * u.K, 10.0 * u.K, 0.5 * u.GHz
    ) == Utilities.Tx(0.5 * u.GHz, 30.0 * u.K)

"""
Unit tests for the ska_ost_senscalc.mid_utilities module.
"""

import unittest

from ska_ost_senscalc.subarray import SubarrayStorage
from ska_ost_senscalc.utilities import Telescope


class TestMid(unittest.TestCase):
    subarray_storage = SubarrayStorage(Telescope.MID)

    def test_load_by_num_stations_or_antennas_MID_AA4_subarray(self):
        """
        Test that load_by_num_stations_or_antennas() successfuylly retrieve a Mid array given ska and meer antennas
        """

        n_ska = 133
        n_meer = 64
        expected = "AA4"

        subarray = self.subarray_storage.load_by_num_stations_or_antennas(
            num_ska=n_ska, num_meer=n_meer
        )

        assert subarray is not None, "Subarray should not be None"
        assert hasattr(subarray, "label"), "Subarray should have a 'label' attribute"
        assert expected == subarray.label

    def test_load_by_num_stations_or_antennas_MID_AA2_subarray(self):
        """
        Test that load_by_num_stations_or_antennas() successfuylly retrieve a Mid array given ska and meer antennas
        """

        n_ska = 64
        n_meer = 0
        expected = "AA2"

        subarray = self.subarray_storage.load_by_num_stations_or_antennas(
            num_ska=n_ska, num_meer=n_meer
        )

        assert subarray is not None, "Subarray should not be None"
        assert hasattr(subarray, "label"), "Subarray should have a 'label' attribute"
        assert expected == subarray.label

    def test_load_by_num_stations_or_antennas_MID_AA_STAR_AA4_13_5__only_subarray(self):
        """
        Test that load_by_num_stations_or_antennas() successfuylly retrieve a Mid array given ska and meer antennas
        """

        n_ska = 0
        n_meer = 64
        expected = "AA*/AA4 (13.5-m antennas only)"

        subarray = self.subarray_storage.load_by_num_stations_or_antennas(
            num_ska=n_ska, num_meer=n_meer
        )

        assert subarray is not None, "Subarray should not be None"
        assert hasattr(subarray, "label"), "Subarray should have a 'label' attribute"
        assert expected == subarray.label

    def test_load_by_num_stations_or_antennas_MID_custom_subarray(self):
        """
        Test that load_by_num_stations_or_antennas() correctly identifies a Mid array as custom
        """

        n_ska = 129
        n_meer = 61

        subarray = self.subarray_storage.load_by_num_stations_or_antennas(
            num_ska=n_ska, num_meer=n_meer
        )

        assert subarray is None, "Subarray should be None"


class TestLow(unittest.TestCase):
    subarray_storage = SubarrayStorage(Telescope.LOW)

    def test_load_by_num_stations_or_antennas_LOW_custom_subarray(self):
        """
        Test that load_by_num_stations_or_antennas() correctly identifies a Low array as custom
        """

        n_stations = 499

        subarray = self.subarray_storage.load_by_num_stations_or_antennas(
            num_stations=n_stations
        )

        assert subarray is None, "Subarray should be None"

    def test_load_by_num_stations_or_antennas_LOW_AA4_subarray(self):
        """
        Test that load_by_num_stations_or_antennas() successfuylly retrieve a Low array given number of stations
        """

        n_stations = 512
        expected = "LOW_AA4_all"

        subarray = self.subarray_storage.load_by_num_stations_or_antennas(
            num_stations=n_stations
        )

        assert subarray is not None, "Subarray should not be None"
        assert hasattr(subarray, "name"), "Subarray should have a 'name' attribute"
        assert expected == subarray.name

    def test_load_by_num_stations_or_antennas_LOW_AA0_5_subarray(self):
        """
        Test that load_by_num_stations_or_antennas() successfuylly retrieve a Low array given number of stations
        """

        n_stations = 6
        expected = "LOW_AA05_all"

        subarray = self.subarray_storage.load_by_num_stations_or_antennas(
            num_stations=n_stations
        )

        assert subarray is not None, "Subarray should not be None"
        assert hasattr(subarray, "name"), "Subarray should have a 'name' attribute"
        assert expected == subarray.name

    def test_load_by_num_stations_or_antennas_no_antennas_provided_error(self):
        """
        Test that load_by_num_stations_or_antennas() raises errors when no antennas are provided
        """

        with self.assertRaises(ValueError) as context:
            self.subarray_storage.load_by_num_stations_or_antennas()
        self.assertEqual(
            str(context.exception),
            "Either num_stations or both num_ska and num_meer must be provided.",
        )

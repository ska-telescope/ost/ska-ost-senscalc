import math
import unittest
from copy import deepcopy
from unittest import mock

import astropy.units as u
import numpy as np
from astropy.coordinates import Latitude
from astropy.coordinates.sky_coordinate import SkyCoord
from astropy.units import Quantity

from ska_ost_senscalc.common.model import (
    BeamSize,
    BeamSizeResult,
    ConfusionNoise,
    ContinuumIntegrationTimeTransformedResults,
    ContinuumSensitivityTransformationsSubbandsInput,
    ContinuumSensitivityTransformedResults,
    ContinuumWeightingRequestParams,
    IntegrationTimeTransformationsWithoutWeightingInput,
    Limit,
    LOWArrayConfiguration,
    SensitivitiesTransformationsWithoutWeightingInput,
    SubbandsBeamSizeTransformedResults,
    SubbandsItemsTransformedResults,
    Telescope,
    Weighting,
    WeightingMultiResultElement,
    WeightingResult,
    WeightingSpectralMode,
    ZoomRequestPrepared,
    ZoomSensitivityTransformedResults,
)
from ska_ost_senscalc.common.service import (
    SENS_LIMIT_WARNING,
    BeamSizeResponse,
    ConfusionNoiseResponse,
    ContinuumWeightingResponse,
    SingleZoomWeightingResponse,
    SubbandWeightingResponse,
    calculate_continuum_integration_time_results_without_weighting,
    calculate_continuum_results_without_weighting,
    calculate_continuum_sensitivities_subbands_results,
    calculate_zoom_results_without_weighting,
    get_confusion_noise,
    get_continuum_weighting_response,
    get_surface_brightness_sensitivity,
    get_total_sensitivity,
    get_weighted_sensitivity,
    get_zoom_weighting_response,
    sensitivity_limit_reached,
    subarray_configuration_from_input,
    thermal_sensitivity_on_unit,
)
from ska_ost_senscalc.subarray import MIDArrayConfiguration

TEST_WEIGHTING_RESULT = WeightingResult(
    weighting_factor=1.5,
    surface_brightness_conversion_factor=2 * u.K,
    beam_size=BeamSize(1 * u.arcsec, 2 * u.arcsec, 3 * u.arcsec),
    confusion_noise=ConfusionNoise(5 * u.Jy, Limit.VALUE),
)

TEST_WEIGHTING_MULTI_RESULT = [
    WeightingMultiResultElement(
        weighting_factor=1.5,
        surface_brightness_conversion_factor=2 * u.K,
        beam_size=BeamSize(1 * u.arcsec, 2 * u.arcsec, 3 * u.arcsec),
        confusion_noise=ConfusionNoise(5 * u.Jy, Limit.VALUE),
        freq_centre=3e6 * u.Hz,
    ),
    WeightingMultiResultElement(
        weighting_factor=1.5,
        surface_brightness_conversion_factor=2 * u.K,
        beam_size=BeamSize(1 * u.arcsec, 2 * u.arcsec, 3 * u.arcsec),
        confusion_noise=ConfusionNoise(5 * u.Jy, Limit.VALUE),
        freq_centre=4e6 * u.Hz,
    ),
]


@mock.patch("ska_ost_senscalc.common.service.calculate_weighting")
def test_continuum_response_without_subbands(mock_calculate_weighting):
    mock_calculate_weighting.return_value = TEST_WEIGHTING_RESULT

    user_input = ContinuumWeightingRequestParams(
        telescope=Telescope.LOW,
        spectral_mode=WeightingSpectralMode.CONTINUUM,
        weighting_mode=Weighting.UNIFORM,
        subarray_configuration=LOWArrayConfiguration.LOW_AA05_ALL,
        dec=Latitude("48 degrees"),
        freq_centre=6e6 * u.Hz,
    )

    result = get_continuum_weighting_response(user_input)

    expected_result = ContinuumWeightingResponse(
        weighting_factor=TEST_WEIGHTING_RESULT.weighting_factor,
        sbs_conv_factor=TEST_WEIGHTING_RESULT.surface_brightness_conversion_factor.value,
        confusion_noise=ConfusionNoiseResponse(
            value=TEST_WEIGHTING_RESULT.confusion_noise.value.value,
            limit_type=TEST_WEIGHTING_RESULT.confusion_noise.limit.value,
        ),
        beam_size=BeamSizeResponse(
            beam_maj_scaled=TEST_WEIGHTING_RESULT.beam_size.beam_maj.value,
            beam_min_scaled=TEST_WEIGHTING_RESULT.beam_size.beam_min.value,
            beam_pa=TEST_WEIGHTING_RESULT.beam_size.beam_pa.value,
        ),
    )
    assert result == expected_result
    mock_calculate_weighting.assert_called_once()


@mock.patch("ska_ost_senscalc.common.service.calculate_weighting")
@mock.patch("ska_ost_senscalc.common.service.calculate_multi_weighting")
def test_continuum_response_for_subbands(
    mock_calculate_multi_weighting, mock_calculate_weighting
):
    mock_calculate_weighting.return_value = TEST_WEIGHTING_RESULT
    mock_calculate_multi_weighting.return_value = TEST_WEIGHTING_MULTI_RESULT

    user_input = ContinuumWeightingRequestParams(
        telescope=Telescope.LOW,
        spectral_mode=WeightingSpectralMode.CONTINUUM,
        weighting_mode=Weighting.UNIFORM,
        subarray_configuration=LOWArrayConfiguration.LOW_AA05_ALL,
        dec=Latitude("48 degrees"),
        freq_centre=6e6 * u.Hz,
        subband_freq_centres=[8e6 * u.Hz, 9e6 * u.Hz],
    )

    result = get_continuum_weighting_response(user_input)

    expected_result = [
        SubbandWeightingResponse(
            weighting_factor=TEST_WEIGHTING_MULTI_RESULT[0].weighting_factor,
            sbs_conv_factor=TEST_WEIGHTING_MULTI_RESULT[
                0
            ].surface_brightness_conversion_factor.value,
            confusion_noise=ConfusionNoiseResponse(
                value=TEST_WEIGHTING_MULTI_RESULT[0].confusion_noise.value.value,
                limit_type=TEST_WEIGHTING_MULTI_RESULT[0].confusion_noise.limit.value,
            ),
            beam_size=BeamSizeResponse(
                beam_maj_scaled=TEST_WEIGHTING_MULTI_RESULT[0].beam_size.beam_maj.value,
                beam_min_scaled=TEST_WEIGHTING_MULTI_RESULT[0].beam_size.beam_min.value,
                beam_pa=TEST_WEIGHTING_MULTI_RESULT[0].beam_size.beam_pa.value,
            ),
            subband_freq_centre=3e6 * u.Hz,
        ),
        SubbandWeightingResponse(
            weighting_factor=TEST_WEIGHTING_MULTI_RESULT[1].weighting_factor,
            sbs_conv_factor=TEST_WEIGHTING_MULTI_RESULT[
                1
            ].surface_brightness_conversion_factor.value,
            confusion_noise=ConfusionNoiseResponse(
                value=TEST_WEIGHTING_MULTI_RESULT[1].confusion_noise.value.value,
                limit_type=TEST_WEIGHTING_MULTI_RESULT[1].confusion_noise.limit.value,
            ),
            beam_size=BeamSizeResponse(
                beam_maj_scaled=TEST_WEIGHTING_MULTI_RESULT[1].beam_size.beam_maj.value,
                beam_min_scaled=TEST_WEIGHTING_MULTI_RESULT[1].beam_size.beam_min.value,
                beam_pa=TEST_WEIGHTING_MULTI_RESULT[1].beam_size.beam_pa.value,
            ),
            subband_freq_centre=4e6 * u.Hz,
        ),
    ]
    assert result.subbands == expected_result
    mock_calculate_multi_weighting.assert_called_once()
    mock_calculate_weighting.assert_called_once()


@mock.patch("ska_ost_senscalc.common.service.calculate_multi_weighting")
def test_zoom_response(mock_calculate_multi_weighting):
    mock_calculate_multi_weighting.return_value = TEST_WEIGHTING_MULTI_RESULT

    params = ZoomRequestPrepared(
        rx_band="Does not matter",
        freq_centres_hz=[],
        spectral_resolutions_hz=[],
        total_bandwidths_hz=[],
        telescope=Telescope.LOW,
        weighting_mode=Weighting.UNIFORM,
        subarray_configuration=LOWArrayConfiguration.LOW_AA05_ALL,
        pointing_centre=SkyCoord(0, 48, unit="deg"),
        freq_centres=[3e6 * u.Hz, 4e6 * u.Hz],
    )

    result = get_zoom_weighting_response(params)

    expected = [
        SingleZoomWeightingResponse(
            weighting_factor=TEST_WEIGHTING_MULTI_RESULT[0].weighting_factor,
            sbs_conv_factor=TEST_WEIGHTING_MULTI_RESULT[
                0
            ].surface_brightness_conversion_factor.value,
            confusion_noise=ConfusionNoiseResponse(
                value=TEST_WEIGHTING_MULTI_RESULT[0].confusion_noise.value.value,
                limit_type=TEST_WEIGHTING_MULTI_RESULT[0].confusion_noise.limit.value,
            ),
            beam_size=BeamSizeResponse(
                beam_maj_scaled=TEST_WEIGHTING_MULTI_RESULT[0].beam_size.beam_maj.value,
                beam_min_scaled=TEST_WEIGHTING_MULTI_RESULT[0].beam_size.beam_min.value,
                beam_pa=TEST_WEIGHTING_MULTI_RESULT[0].beam_size.beam_pa.value,
            ),
            freq_centre=3e6 * u.Hz,
        ),
        SingleZoomWeightingResponse(
            weighting_factor=TEST_WEIGHTING_MULTI_RESULT[0].weighting_factor,
            sbs_conv_factor=TEST_WEIGHTING_MULTI_RESULT[
                1
            ].surface_brightness_conversion_factor.value,
            confusion_noise=ConfusionNoiseResponse(
                value=TEST_WEIGHTING_MULTI_RESULT[1].confusion_noise.value.value,
                limit_type=TEST_WEIGHTING_MULTI_RESULT[1].confusion_noise.limit.value,
            ),
            beam_size=BeamSizeResponse(
                beam_maj_scaled=TEST_WEIGHTING_MULTI_RESULT[1].beam_size.beam_maj.value,
                beam_min_scaled=TEST_WEIGHTING_MULTI_RESULT[1].beam_size.beam_min.value,
                beam_pa=TEST_WEIGHTING_MULTI_RESULT[1].beam_size.beam_pa.value,
            ),
            freq_centre=4e6 * u.Hz,
        ),
    ]

    assert result == expected
    mock_calculate_multi_weighting.assert_called_once()


@mock.patch("ska_ost_senscalc.common.service.calculate_weighting")
def test_upper_limits_are_transformed(mock_calculate_weighting):
    """
    The service should convert any upper limit labels and values in
    the confusion noise results to 0 Jy with a label of value.
    """
    confusion_noise = ConfusionNoise(
        Quantity(99, unit="Jy"),
        Limit.UPPER,
    )
    user_input = ContinuumWeightingRequestParams(
        telescope=Telescope.LOW,
        spectral_mode=WeightingSpectralMode.CONTINUUM,
        weighting_mode=Weighting.UNIFORM,
        subarray_configuration=LOWArrayConfiguration.LOW_AA05_ALL,
        dec=Latitude("48 degrees"),
        freq_centre=6e6 * u.Hz,
    )

    mock_output = deepcopy(TEST_WEIGHTING_RESULT)
    mock_output.confusion_noise = confusion_noise
    mock_calculate_weighting.return_value = mock_output

    result = get_continuum_weighting_response(user_input)

    # we expect any upper limit to have been converted to a confusion noise of
    # 0 Jy and its limit transformed from UPPER to a plain value. Other values
    # and limit labels should remain unchanged.
    assert result.confusion_noise.value == 0
    assert result.confusion_noise.limit_type == "value"


def test_calculate_continuum_sensitivities_subbands_results():
    """
    The service should calculate the correct subband weighted results
    when provided with a subbands_input
    """

    calculate_subbands = [
        {
            "subband_freq_centre": Quantity(125, u.MHz),
            "sensitivity": Quantity(14.06239332, u.uJy / u.beam),
        },
        {
            "subband_freq_centre": Quantity(275, u.MHz),
            "sensitivity": Quantity(7.51588275, u.uJy / u.beam),
        },
    ]
    weighting_subbands = [
        SubbandWeightingResponse(
            subband_freq_centre=Quantity(1.25e08, u.Hz),
            weighting_factor=np.float64(10.193990989271752),
            sbs_conv_factor=np.float64(3253529.6874249526),
            confusion_noise=ConfusionNoiseResponse(
                value=np.float64(3.6996961166710036e-06),
                limit_type="value",
            ),
            beam_size=BeamSizeResponse(
                beam_maj_scaled=np.float64(0.0015172288220633018),
                beam_min_scaled=np.float64(0.0012225963303635374),
                beam_pa=np.float64(375.66579891311113),
            ),
        ),
        SubbandWeightingResponse(
            subband_freq_centre=Quantity(2.75e08, u.Hz),
            weighting_factor=np.float64(10.193990989271752),
            sbs_conv_factor=np.float64(3253529.6874249536),
            confusion_noise=ConfusionNoiseResponse(
                value=np.float64(2.2044595679752553e-07),
                limit_type="value",
            ),
            beam_size=BeamSizeResponse(
                beam_maj_scaled=np.float64(0.000689649464574228),
                beam_min_scaled=np.float64(0.0005557256047106988),
                beam_pa=np.float64(375.66579891311113),
            ),
        ),
    ]

    subbands_input = ContinuumSensitivityTransformationsSubbandsInput(
        calculate_subbands=calculate_subbands, weighting_subbands=weighting_subbands
    )

    warnings = []
    result = calculate_continuum_sensitivities_subbands_results(
        subbands_input, warnings
    )

    expected_weighted_sensitivity_per_subband = SubbandsItemsTransformedResults(
        min_value=Quantity(7.661684102992299e-05, u.Jy / u.beam),
        max_value=Quantity(0.00014335191079167526, u.Jy / u.beam),
    )
    assert (
        expected_weighted_sensitivity_per_subband.min_value.value
        == result.weighted_sensitivity_per_subband.min_value.value
    )
    assert (
        expected_weighted_sensitivity_per_subband.min_value.unit
        == result.weighted_sensitivity_per_subband.min_value.unit
    )
    assert (
        expected_weighted_sensitivity_per_subband.max_value.value
        == result.weighted_sensitivity_per_subband.max_value.value
    )
    assert (
        expected_weighted_sensitivity_per_subband.max_value.unit
        == result.weighted_sensitivity_per_subband.max_value.unit
    )

    expected_confusion_noise_per_subband = SubbandsItemsTransformedResults(
        min_value=Quantity(2.2044595679752553e-07, u.Jy),
        max_value=Quantity(3.6996961166710036e-06, u.Jy),
    )
    assert (
        expected_confusion_noise_per_subband.min_value.value
        == result.confusion_noise_per_subband.min_value.value
    )
    assert (
        expected_confusion_noise_per_subband.min_value.unit
        == result.confusion_noise_per_subband.min_value.unit
    )
    assert (
        expected_confusion_noise_per_subband.max_value.value
        == result.confusion_noise_per_subband.max_value.value
    )
    assert (
        expected_confusion_noise_per_subband.max_value.unit
        == result.confusion_noise_per_subband.max_value.unit
    )

    expected_total_sensitivity_per_subband = SubbandsItemsTransformedResults(
        min_value=Quantity(7.661715816854838e-05, u.Jy / u.beam),
        max_value=Quantity(0.0001433996446264081, u.Jy / u.beam),
    )
    assert (
        expected_total_sensitivity_per_subband.min_value.value
        == result.total_sensitivity_per_subband.min_value.value
    )
    assert (
        result.total_sensitivity_per_subband.min_value.unit
        == expected_total_sensitivity_per_subband.min_value.unit
    )
    assert (
        expected_total_sensitivity_per_subband.max_value.value
        == result.total_sensitivity_per_subband.max_value.value
    )
    assert (
        expected_total_sensitivity_per_subband.max_value.unit
        == result.total_sensitivity_per_subband.max_value.unit
    )

    expected_synthesized_beam_size_per_subband = SubbandsBeamSizeTransformedResults(
        min_value=BeamSizeResult(
            beam_min=Quantity(2.0006121769585157, u.arcsec),
            beam_maj=Quantity(2.482738072467221, u.arcsec),
        ),
        max_value=BeamSizeResult(
            beam_min=Quantity(4.401346789308734, u.arcsec),
            beam_maj=Quantity(5.462023759427886, u.arcsec),
        ),
    )
    assert (
        expected_synthesized_beam_size_per_subband.min_value.beam_min.value
        == result.synthesized_beam_size_per_subband.min_value.beam_min.value
    )
    assert (
        expected_synthesized_beam_size_per_subband.min_value.beam_min.unit
        == result.synthesized_beam_size_per_subband.min_value.beam_min.unit
    )

    assert (
        expected_synthesized_beam_size_per_subband.min_value.beam_maj.value
        == result.synthesized_beam_size_per_subband.min_value.beam_maj.value
    )
    assert (
        result.synthesized_beam_size_per_subband.min_value.beam_maj.unit
        == expected_synthesized_beam_size_per_subband.min_value.beam_maj.unit
    )

    assert (
        expected_synthesized_beam_size_per_subband.max_value.beam_min.value
        == result.synthesized_beam_size_per_subband.max_value.beam_min.value
    )
    assert (
        expected_synthesized_beam_size_per_subband.max_value.beam_min.unit
        == result.synthesized_beam_size_per_subband.max_value.beam_min.unit
    )

    assert (
        expected_synthesized_beam_size_per_subband.max_value.beam_maj.value
        == result.synthesized_beam_size_per_subband.max_value.beam_maj.value
    )
    assert (
        expected_synthesized_beam_size_per_subband.max_value.beam_maj.unit
        == result.synthesized_beam_size_per_subband.max_value.beam_maj.unit
    )

    expected_surface_brightness_sensitivity_per_subband = (
        SubbandsItemsTransformedResults(
            min_value=Quantity(249.27619866750544, u.K),
            max_value=Quantity(466.55500095820685, u.K),
        )
    )
    assert (
        expected_surface_brightness_sensitivity_per_subband.min_value.value
        == result.surface_brightness_sensitivity_per_subband.min_value.value
    )
    assert (
        expected_surface_brightness_sensitivity_per_subband.min_value.unit
        == result.surface_brightness_sensitivity_per_subband.min_value.unit
    )
    assert (
        expected_surface_brightness_sensitivity_per_subband.max_value.value
        == result.surface_brightness_sensitivity_per_subband.max_value.value
    )
    assert (
        expected_surface_brightness_sensitivity_per_subband.max_value.unit
        == result.surface_brightness_sensitivity_per_subband.max_value.unit
    )

    expected = []

    # A sensitivity limit waring should NOT be added to warning when the total sensitivity
    # is not approaching confusion noise limit
    assert expected == warnings


def test_append_subbands_results_warning():
    """
    A sensitivity limit waring should be added to warning when the total sensitivity
    is approaching confusion noise limit
    """

    calculate_subbands = [
        {
            "subband_freq_centre": Quantity(125, u.MHz),
            "sensitivity": Quantity(10.219825522215212, u.uJy / u.beam),
        },
        {
            "subband_freq_centre": Quantity(175, u.MHz),
            "sensitivity": Quantity(10.155555049054804, u.uJy / u.beam),
        },
    ]
    weighting_subbands = [
        SubbandWeightingResponse(
            subband_freq_centre=Quantity(50000000, u.Hz),
            weighting_factor=np.float64(1),
            sbs_conv_factor=np.float64(188864.8223806142),
            confusion_noise=ConfusionNoiseResponse(
                value=np.float64(0.0007041273763771605),
                limit_type="value",
            ),
            beam_size=BeamSizeResponse(
                beam_maj_scaled=np.float64(0.015708119985065785),
                beam_min_scaled=np.float64(0.01271433584105161),
                beam_pa=np.float64(2.6713358651967023),
            ),
        ),
        SubbandWeightingResponse(
            subband_freq_centre=Quantity(150000000, u.Hz),
            weighting_factor=np.float64(1),
            sbs_conv_factor=np.float64(188864.82238061418),
            confusion_noise=ConfusionNoiseResponse(
                value=np.float64(0.00005123723014831154),
                limit_type="value",
            ),
            beam_size=BeamSizeResponse(
                beam_maj_scaled=np.float64(0.005236039995021929),
                beam_min_scaled=np.float64(0.0042381119470172035),
                beam_pa=np.float64(2.6713358651967023),
            ),
        ),
    ]

    subbands_input = ContinuumSensitivityTransformationsSubbandsInput(
        calculate_subbands=calculate_subbands, weighting_subbands=weighting_subbands
    )

    expected = [
        {"max_sensitivity": SENS_LIMIT_WARNING},
        {"min_sensitivity": SENS_LIMIT_WARNING},
    ]

    warnings = []
    calculate_continuum_sensitivities_subbands_results(subbands_input, warnings)
    assert expected == warnings


def test_calculated_sensitivity_results_without_weighting_low_continuum():
    """
    Calculates the expected sensitivity weighted results without weighting
    for low continuum (custom subarray config)
    """

    continuum_sensitivity = Quantity(5.506078437533309, u.uJy / u.beam)
    spectral_sensitivity = Quantity(1056.5633778803517, u.uJy / u.beam)

    transformation_input = SensitivitiesTransformationsWithoutWeightingInput(
        continuum_sensitivity=continuum_sensitivity,
        spectral_sensitivity=spectral_sensitivity,
    )

    warning = []
    results = calculate_continuum_results_without_weighting(
        transformation_input, warning
    )

    expected_results = ContinuumSensitivityTransformedResults(
        weighted_continuum_sensitivity=Quantity(0.000005506078437533308, u.Jy / u.beam),
        weighted_spectral_sensitivity=Quantity(0.0010565633778803517, u.Jy / u.beam),
        continuum_confusion_noise=None,
        total_continuum_sensitivity=None,
        continuum_synthesized_beam_size=None,
        continuum_surface_brightness_sensitivity=None,
        spectral_confusion_noise=None,
        total_spectral_sensitivity=None,
        spectral_synthesized_beam_size=None,
        spectral_surface_brightness_sensitivity=None,
    )

    assert expected_results == results


def test_calculated_sensitivity_results_without_weighting_low_zoom():
    """
    Calculates the expected sensitivity weighted results without weighting
    for low zoom (custom subarray config)
    """

    spectral_sensitivity = Quantity(22307.044765155348, u.uJy / u.beam)

    transformation_input = SensitivitiesTransformationsWithoutWeightingInput(
        spectral_sensitivity=spectral_sensitivity,
    )

    warnings = []
    results = calculate_zoom_results_without_weighting(transformation_input, warnings)

    expected_results = ZoomSensitivityTransformedResults(
        weighted_spectral_sensitivity=Quantity(0.022307044765155347, u.Jy / u.beam),
        spectral_confusion_noise=None,
        total_spectral_sensitivity=None,
        spectral_synthesized_beam_size=None,
        spectral_surface_brightness_sensitivity=None,
        warnings=warnings,
    )

    assert expected_results == results


def test_calculated_sensitivity_results_without_weighting_mid_continuum_supplied_integration_time():
    """
    Calculates the expected sensitivity weighted results without weighting
    for mid continuum supplied integration time (custom subarray config)
    once sensitivities have been calculated
    """

    continuum_sensitivity = Quantity(0.000431328855641859, u.Jy)
    spectral_sensitivity = Quantity(0.07759853233471885, u.Jy)

    transformation_input = SensitivitiesTransformationsWithoutWeightingInput(
        continuum_sensitivity=continuum_sensitivity,
        spectral_sensitivity=spectral_sensitivity,
    )

    warning = []
    results = calculate_continuum_results_without_weighting(
        transformation_input, warning
    )

    expected_results = ContinuumSensitivityTransformedResults(
        weighted_continuum_sensitivity=Quantity(0.000431328855641859, u.Jy),
        weighted_spectral_sensitivity=Quantity(0.07759853233471885, u.Jy),
        continuum_confusion_noise=None,
        total_continuum_sensitivity=None,
        continuum_synthesized_beam_size=None,
        continuum_surface_brightness_sensitivity=None,
        spectral_confusion_noise=None,
        total_spectral_sensitivity=None,
        spectral_synthesized_beam_size=None,
        spectral_surface_brightness_sensitivity=None,
    )

    assert expected_results == results


def test_calculated_integration_time_results_without_weighting_mid_continuum_supplied_sensitivity():
    """
    Calculates the expected integration time weighted results without weighting
    for mid continuum supplied sensitivity (custom subarray config)
    once integration times have been calculated
    """

    continuum_integration_time = Quantity(4.6927673330844174e-08, u.s)
    spectral_integration_time = Quantity(0.001518864427002769, u.s)

    transformation_input = IntegrationTimeTransformationsWithoutWeightingInput(
        continuum_integration_time=continuum_integration_time,
        spectral_integration_time=spectral_integration_time,
    )

    warning = []
    results = calculate_continuum_integration_time_results_without_weighting(
        transformation_input, warning
    )

    expected_results = ContinuumIntegrationTimeTransformedResults(
        continuum_confusion_noise=None,
        continuum_synthesized_beam_size=None,
        continuum_integration_time=Quantity(4.6927673330844174e-08, u.s),
        spectral_confusion_noise=None,
        spectral_synthesized_beam_size=None,
        spectral_integration_time=Quantity(0.001518864427002769, u.s),
        warnings=warning,
    )

    assert expected_results == results


# Unit tests for functions in common services
class TestThermalSensitivityOnUnit(unittest.TestCase):
    """
    Unit tests for `thermal_sensitivity_on_unit` to verify correctness across different cases,
    including valid computations, edge cases, and expected errors.

    The function should:
    - Accurately compute thermal sensitivity with different units.
    - Handle extreme values (very large or very small).
    - Raise errors for invalid inputs (e.g., unsupported units, zero weighting factor).
    """

    # Define valid test cases
    valid_test_cases = [
        {
            "name": "Case 1: Jy/beam",
            "sensitivity": 100.0,
            "weighting_factor": 2.0,
            "sbs_conv_factor": 1.5,
            "confusion_noise": 50.0,
            "unit": u.Jy / u.beam,
        },
        {
            "name": "Case 2: Kelvin",
            "sensitivity": 200.0,
            "weighting_factor": 3.0,
            "sbs_conv_factor": 2.0,
            "confusion_noise": 100.0,
            "unit": u.K,
        },
        {
            "name": "Case 3: Float with unit conversion",
            "sensitivity": 100.0,
            "weighting_factor": 2.0,
            "sbs_conv_factor": 1.5,
            "confusion_noise": 50.0,
            "unit": u.Jy / u.beam,
        },
        {
            "name": "Case 4: Large Values",
            "sensitivity": 1e6,
            "weighting_factor": 5.0,
            "sbs_conv_factor": 1.2,
            "confusion_noise": 5e5,
            "unit": u.Jy / u.beam,
        },
        {
            "name": "Case 5: Small Values",
            "sensitivity": 1e-6,
            "weighting_factor": 0.5,
            "sbs_conv_factor": 1.2,
            "confusion_noise": 5e-7,
            "unit": u.Jy / u.beam,
        },
    ]

    def test_valid_sensitivity_cases(self):
        """
        Test valid scenarios for thermal sensitivity calculations using subTest().

        The following cases are tested:
        - Case 1: Standard input in Jy/beam.
        - Case 2: Input in Kelvin (valid but converted internally).
        - Case 3: Standard float input with unit conversion.
        - Case 4: Large values to check numerical stability.
        - Case 5: Small values to check numerical precision.
        """

        for case in self.valid_test_cases:
            with self.subTest(name=case["name"]):
                result = thermal_sensitivity_on_unit(
                    sensitivity=case["sensitivity"],
                    weighting_factor=case["weighting_factor"],
                    sbs_conv_factor=case["sbs_conv_factor"],
                    confusion_noise=case["confusion_noise"],
                    unit=case["unit"],
                )

                if case["unit"] == u.K:
                    expected_value = math.sqrt(
                        (case["sensitivity"] ** 2) - (case["confusion_noise"] ** 2)
                    ) / (case["weighting_factor"] * case["sbs_conv_factor"])
                else:
                    expected_value = (
                        math.sqrt(
                            (case["sensitivity"] ** 2) - (case["confusion_noise"] ** 2)
                        )
                        / case["weighting_factor"]
                    )

                expected = Quantity(expected_value, u.Jy)

                self.assertAlmostEqual(
                    result.value,
                    expected.value,
                    places=6,
                    msg=f"Failed for {case['name']}",
                )
                self.assertEqual(result.unit, u.Jy, f"Unit mismatch for {case['name']}")

    def test_error_cases(self):
        """
        Test invalid inputs that should raise ValueError exceptions.

        The following cases are tested:
        - Case 1: `unit=None` should raise an error.
        - Case 2: `weighting_factor=0` should raise an error.
        - Case 3: `sensitivity < confusion_noise` should raise an error.
        - Case 4: Unsupported unit type (meters).
        """
        # Define error test cases
        error_test_cases = [
            {
                "name": "None unit",
                "sensitivity": 100.0,
                "weighting_factor": 2.0,
                "sbs_conv_factor": 1.5,
                "confusion_noise": 50.0,
                "unit": None,
                "expected_msg": "Parameter 'unit' cannot be None. Please provide appropriate unit",
            },
            {
                "name": "Zero weighting factor",
                "sensitivity": 100.0,
                "weighting_factor": 0.0,
                "sbs_conv_factor": 1.5,
                "confusion_noise": 50.0,
                "unit": u.Jy / u.beam,
                "expected_msg": "Weighting factor cannot be zero.",
            },
            {
                "name": "Sensitivity < confusion noise",
                "sensitivity": 40.0,
                "weighting_factor": 2.0,
                "sbs_conv_factor": 1.5,
                "confusion_noise": 50.0,
                "unit": u.Jy / u.beam,
                "expected_msg": "Sensitivity (40.0 Jy / beam) must be greater than or equal to confusion noise (50.0).",
            },
            {
                "name": "Invalid unit (meters)",
                "sensitivity": 100.0,
                "weighting_factor": 2.0,
                "sbs_conv_factor": 1.5,
                "confusion_noise": 50.0,
                "unit": u.m,
                "expected_msg": "Unrecognized or unsupported unit: m",
            },
        ]

        for test_case in error_test_cases:
            with self.subTest(name=test_case["name"]):
                with self.assertRaises(ValueError) as context:
                    thermal_sensitivity_on_unit(
                        sensitivity=test_case["sensitivity"],
                        weighting_factor=test_case["weighting_factor"],
                        sbs_conv_factor=test_case["sbs_conv_factor"],
                        confusion_noise=test_case["confusion_noise"],
                        unit=test_case["unit"],
                    )
                self.assertEquals(test_case["expected_msg"], str(context.exception))


class TestGetWeightedSensitivity(unittest.TestCase):
    """
    Unit tests for `get_weighted_sensitivity` to verify correctness across different cases,
    including valid computations, edge cases, and expected errors.

    The function should:
    - Correctly compute weighted sensitivity in different units.
    - Handle extreme values (very large or very small).
    - Raise errors for invalid inputs (e.g., non-Quantity sensitivity).
    """

    # Define valid test cases
    valid_test_cases = [
        {
            "name": "Case 1: Jy",
            "sensitivity": Quantity(1.0, u.Jy),
            "weighting_factor": 2.0,
            "expected": Quantity(2.0, u.Jy),
        },
        {
            "name": "Case 2: uJy/beam",
            "sensitivity": Quantity(500, u.uJy / u.beam),
            "weighting_factor": 3.0,
            "expected": Quantity(1500, u.uJy / u.beam).to(u.Jy / u.beam),
        },
        {
            "name": "Case 3: mJy/beam",
            "sensitivity": Quantity(0.5, u.mJy / u.beam),
            "weighting_factor": 4.0,
            "expected": Quantity(2.0, u.mJy / u.beam).to(u.Jy / u.beam),
        },
        {
            "name": "Case 4: Large Values",
            "sensitivity": Quantity(1e6, u.Jy),
            "weighting_factor": 1e3,
            "expected": Quantity(1e9, u.Jy),
        },
        {
            "name": "Case 5: Small Values",
            "sensitivity": Quantity(1e-6, u.Jy),
            "weighting_factor": 1e-3,
            "expected": Quantity(1e-9, u.Jy),
        },
        {
            "name": "Case 6: Negative Weighting Factor",
            "sensitivity": Quantity(100, u.uJy / u.beam),
            "weighting_factor": -2.0,
            "expected": Quantity(-200, u.uJy / u.beam).to(u.Jy / u.beam),
        },
    ]

    # Define edge case tests
    edge_case_tests = [
        {
            "name": "Case 7: Weighting Factor Zero",
            "sensitivity": Quantity(1.0, u.Jy),
            "weighting_factor": 0.0,
            "expected": Quantity(0.0, u.Jy),
        },
        {
            "name": "Case 8: Weighting Factor One",
            "sensitivity": Quantity(250, u.uJy / u.beam),
            "weighting_factor": 1.0,
            "expected": Quantity(250, u.uJy / u.beam).to(u.Jy / u.beam),
        },
    ]

    # Define invalid test cases
    invalid_test_cases = [
        {
            "name": "Invalid Sensitivity Type",
            "sensitivity": "invalid",
            "weighting_factor": 2.0,
            "expected_exception": TypeError,
        },
    ]

    def test_valid_sensitivity_cases(self):
        """
        Test valid scenarios for `get_weighted_sensitivity` using subTest().

        The following cases are tested:
        - Case 1: Standard input in Jy.
        - Case 2: Input in uJy/beam.
        - Case 3: Input in mJy/beam.
        - Case 4: Large values.
        - Case 5: Small values.
        - Case 6: Negative weighting factor.
        """
        for case in self.valid_test_cases:
            with self.subTest(name=case["name"]):
                result = get_weighted_sensitivity(
                    sensitivity=case["sensitivity"],
                    weighting_factor=case["weighting_factor"],
                )
                self.assertAlmostEqual(
                    result.value,
                    case["expected"].value,
                    places=6,
                    msg=f"Failed for {case['name']}",
                )
                self.assertEqual(
                    result.unit,
                    case["expected"].unit,
                    f"Unit mismatch for {case['name']}",
                )

    def test_edge_cases(self):
        """
        Test edge scenarios, including:
        - Case 7: Weighting factor is zero (should return zero sensitivity).
        - Case 8: Weighting factor is one (should return the same sensitivity).
        """
        for case in self.edge_case_tests:
            with self.subTest(name=case["name"]):
                result = get_weighted_sensitivity(
                    sensitivity=case["sensitivity"],
                    weighting_factor=case["weighting_factor"],
                )
                self.assertAlmostEqual(
                    result.value,
                    case["expected"].value,
                    places=6,
                    msg=f"Failed for {case['name']}",
                )
                self.assertEqual(
                    result.unit,
                    case["expected"].unit,
                    f"Unit mismatch for {case['name']}",
                )

    def test_invalid_cases(self):
        """
        Test invalid inputs that should raise exceptions.

        The following cases are tested:
        - Case 9: Invalid sensitivity type (non-Quantity input).
        """
        for case in self.invalid_test_cases:
            with self.subTest(name=case["name"]):
                with self.assertRaises(case["expected_exception"]):
                    get_weighted_sensitivity(
                        sensitivity=case["sensitivity"],
                        weighting_factor=case["weighting_factor"],
                    )


class TestGetTotalSensitivity(unittest.TestCase):
    """
    Unit tests for `get_total_sensitivity` to verify correctness across different cases,
    including valid computations, edge cases, and expected errors.

    The function should:
    - Correctly compute total sensitivity using `np.hypot()`.
    - Handle extreme values (very large or very small).
    - Raise errors for invalid units.
    """

    # Define valid test cases
    valid_test_cases = [
        {
            "name": "Case 1: Jy",
            "confusion_noise": Quantity(3.0, u.Jy),
            "weighted_sensitivity": Quantity(4.0, u.Jy),
            "expected": Quantity(np.hypot(3.0, 4.0), u.Jy),
        },
        {
            "name": "Case 2: uJy and uJy/beam",
            "confusion_noise": Quantity(100, u.uJy),
            "weighted_sensitivity": Quantity(200, u.uJy / u.beam),
            "expected": Quantity(np.hypot(100, 200), u.uJy / u.beam).to(u.Jy / u.beam),
        },
        {
            "name": "Case 3: mJy/beam",
            "confusion_noise": Quantity(0.5, u.mJy),
            "weighted_sensitivity": Quantity(1.0, u.mJy / u.beam),
            "expected": Quantity(np.hypot(0.5, 1.0), u.mJy / u.beam).to(u.Jy / u.beam),
        },
        {
            "name": "Case 4: Large Values",
            "confusion_noise": Quantity(1e6, u.Jy),
            "weighted_sensitivity": Quantity(2e6, u.Jy),
            "expected": Quantity(np.hypot(1e6, 2e6), u.Jy),
        },
        {
            "name": "Case 5: Small Values",
            "confusion_noise": Quantity(1e-6, u.Jy),
            "weighted_sensitivity": Quantity(2e-6, u.Jy),
            "expected": Quantity(np.hypot(1e-6, 2e-6), u.Jy),
        },
    ]

    # Define edge case tests
    edge_case_tests = [
        {
            "name": "Case 6: Confusion Noise Zero",
            "confusion_noise": Quantity(0.0, u.Jy),
            "weighted_sensitivity": Quantity(5.0, u.Jy),
            "expected": Quantity(5.0, u.Jy),
        },
        {
            "name": "Case 7: Weighted Sensitivity Zero",
            "confusion_noise": Quantity(5.0, u.Jy),
            "weighted_sensitivity": Quantity(0.0, u.Jy),
            "expected": Quantity(5.0, u.Jy),
        },
    ]

    # Define invalid test cases
    invalid_test_cases = [
        {
            "name": "Invalid Unit for Confusion Noise",
            "confusion_noise": Quantity(100, u.m),
            "weighted_sensitivity": Quantity(50, u.Jy),
            "expected_exception": ValueError,
        },
        {
            "name": "Invalid Unit for Weighted Sensitivity",
            "confusion_noise": Quantity(50, u.Jy),
            "weighted_sensitivity": Quantity(100, u.s),
            "expected_exception": ValueError,
        },
    ]

    def test_valid_sensitivity_cases(self):
        """
        Test valid scenarios for `get_total_sensitivity` using subTest().

        The following cases are tested:
        - Case 1: Both values in Jy.
        - Case 2: Mixed uJy and uJy/beam.
        - Case 3: Both values in mJy/beam.
        - Case 4: Large values.
        - Case 5: Small values.
        """
        for case in self.valid_test_cases:
            with self.subTest(name=case["name"]):
                result = get_total_sensitivity(
                    confusion_noise=case["confusion_noise"],
                    weighted_sensitivity=case["weighted_sensitivity"],
                )
                self.assertAlmostEqual(
                    result.value,
                    case["expected"].value,
                    places=9,
                    msg=f"Failed for {case['name']}",
                )
                self.assertEqual(
                    result.unit,
                    case["expected"].unit,
                    f"Unit mismatch for {case['name']}",
                )

    def test_edge_cases(self):
        """
        Test edge scenarios, including:
        - Case 6: Confusion noise is zero (should return weighted_sensitivity).
        - Case 7: Weighted sensitivity is zero (should return confusion_noise).
        """
        for case in self.edge_case_tests:
            with self.subTest(name=case["name"]):
                result = get_total_sensitivity(
                    confusion_noise=case["confusion_noise"],
                    weighted_sensitivity=case["weighted_sensitivity"],
                )
                self.assertAlmostEqual(
                    result.value,
                    case["expected"].value,
                    places=6,
                    msg=f"Failed for {case['name']}",
                )
                self.assertEqual(
                    result.unit,
                    case["expected"].unit,
                    f"Unit mismatch for {case['name']}",
                )

    def test_invalid_cases(self):
        """
        Test invalid inputs that should raise exceptions.

        The following cases are tested:
        - Case 8: Invalid unit for confusion_noise.
        - Case 9: Invalid unit for weighted_sensitivity.
        """
        for case in self.invalid_test_cases:
            with self.subTest(name=case["name"]):
                with self.assertRaises(case["expected_exception"]):
                    get_total_sensitivity(
                        confusion_noise=case["confusion_noise"],
                        weighted_sensitivity=case["weighted_sensitivity"],
                    )


class TestGetSurfaceBrightnessSensitivity(unittest.TestCase):
    """
    Unit tests for `get_surface_brightness_sensitivity` to verify correctness across different cases,
    including valid computations, edge cases, and expected errors.

    The function should:
    - Correctly compute surface brightness sensitivity using `sbs_conv_factor`.
    - Directly multiply when `total_sensitivity` is in `Jy`.
    - Convert `total_sensitivity` to `Jy/beam` before applying `sbs_conv_factor` otherwise.
    - Return results in Kelvin (`K`).
    - Raise errors for invalid inputs.
    """

    # Define valid test cases
    valid_test_cases = [
        {
            "name": "Jy",
            "sbs_conv_factor": 2.0,
            "total_sensitivity": Quantity(5.0, u.Jy),
            "expected": Quantity(5.0 * 2.0, u.K),
        },
        {
            "name": "Jy/beam",
            "sbs_conv_factor": 1.5,
            "total_sensitivity": Quantity(3.0, u.Jy / u.beam),
            "expected": Quantity(3.0 * 1.5, u.K),
        },
        {
            "name": "uJy/beam",
            "sbs_conv_factor": 10.0,
            "total_sensitivity": Quantity(200, u.uJy / u.beam),
            "expected": Quantity(
                (200 * u.uJy / u.beam).to(u.Jy / u.beam).value * 10.0, u.K
            ),
        },
        {
            "name": "Large Values",
            "sbs_conv_factor": 1e6,
            "total_sensitivity": Quantity(1e3, u.Jy),
            "expected": Quantity(1e3 * 1e6, u.K),
        },
        {
            "name": "Small Values",
            "sbs_conv_factor": 1e-6,
            "total_sensitivity": Quantity(1e-3, u.Jy),
            "expected": Quantity(1e-3 * 1e-6, u.K),
        },
        {
            "name": "Negative sbs_conv_factor",
            "sbs_conv_factor": -3.0,
            "total_sensitivity": Quantity(4.0, u.Jy),
            "expected": Quantity(-12.0, u.K),
        },
    ]

    # Define edge case tests
    edge_case_tests = [
        {
            "name": "sbs_conv_factor Zero",
            "sbs_conv_factor": 0.0,
            "total_sensitivity": Quantity(5.0, u.Jy),
            "expected": Quantity(0.0, u.K),
        },
    ]

    # Define invalid test cases
    invalid_test_cases = [
        {
            "name": "Invalid sbs_conv_factor Type",
            "sbs_conv_factor": "invalid",
            "total_sensitivity": Quantity(5.0, u.Jy),
            "expected_exception": TypeError,
        },
        {
            "name": "Invalid Unit for total_sensitivity",
            "sbs_conv_factor": 2.0,
            "total_sensitivity": Quantity(100, u.m),
            "expected_exception": ValueError,
        },
    ]

    def test_valid_sensitivity_cases(self):
        """
        Test valid scenarios for `get_surface_brightness_sensitivity` using subTest().

        The following cases are tested:
        - total_sensitivity in Jy, Jy/beam, and uJy/beam.
        - Large and small values.
        - Negative `sbs_conv_factor`.
        """
        for case in self.valid_test_cases:
            with self.subTest(name=case["name"]):
                result = get_surface_brightness_sensitivity(
                    sbs_conv_factor=case["sbs_conv_factor"],
                    total_sensitivity=case["total_sensitivity"],
                )
                self.assertAlmostEqual(
                    result.value,
                    case["expected"].value,
                    places=6,
                    msg=f"Failed for {case['name']}",
                )
                self.assertEqual(
                    result.unit,
                    case["expected"].unit,
                    f"Unit mismatch for {case['name']}",
                )

    def test_edge_cases(self):
        """
        Test edge scenarios, including:
        - sbs_conv_factor is zero (should return zero).
        """
        for case in self.edge_case_tests:
            with self.subTest(name=case["name"]):
                result = get_surface_brightness_sensitivity(
                    sbs_conv_factor=case["sbs_conv_factor"],
                    total_sensitivity=case["total_sensitivity"],
                )
                self.assertAlmostEqual(
                    result.value,
                    case["expected"].value,
                    places=6,
                    msg=f"Failed for {case['name']}",
                )
                self.assertEqual(
                    result.unit,
                    case["expected"].unit,
                    f"Unit mismatch for {case['name']}",
                )

    def test_invalid_cases(self):
        """
        Test invalid inputs that should raise exceptions.

        The following cases are tested:
        - Invalid type for `sbs_conv_factor`.
        - Invalid unit for `total_sensitivity`.
        """
        for case in self.invalid_test_cases:
            with self.subTest(name=case["name"]):
                with self.assertRaises(case["expected_exception"]):
                    get_surface_brightness_sensitivity(
                        sbs_conv_factor=case["sbs_conv_factor"],
                        total_sensitivity=case["total_sensitivity"],
                    )


class TestGetConfusionNoise(unittest.TestCase):
    """
    Unit tests for `get_confusion_noise` to verify correctness across different cases,
    including valid computations, edge cases, and expected errors.

    The function should:
    - Correctly return confusion noise as a Quantity in Jy.
    - Handle cases with or without beam size parameters.
    - Raise errors for invalid inputs.
    """

    # Define valid test cases
    valid_test_cases = [
        {
            "name": "Case 1: Confusion Noise Only",
            "confusion_noise": 5.0,
            "min_beam_size": None,
            "maj_beam_size": None,
            "expected": Quantity(5.0, u.Jy),
        },
        {
            "name": "Case 2: With Beam Sizes",
            "confusion_noise": 3.0,
            "min_beam_size": 2.0,
            "maj_beam_size": 4.0,
            "expected": Quantity(3.0, u.Jy),
        },
        {
            "name": "Case 3: min_beam_size None",
            "confusion_noise": 2.0,
            "min_beam_size": None,
            "maj_beam_size": 5.0,
            "expected": Quantity(2.0, u.Jy),
        },
        {
            "name": "Case 4: maj_beam_size None",
            "confusion_noise": 1.5,
            "min_beam_size": 3.0,
            "maj_beam_size": None,
            "expected": Quantity(1.5, u.Jy),
        },
        {
            "name": "Case 5: Both Beam Sizes None",
            "confusion_noise": 4.0,
            "min_beam_size": None,
            "maj_beam_size": None,
            "expected": Quantity(4.0, u.Jy),
        },
        {
            "name": "Case 6: Confusion Noise Zero",
            "confusion_noise": 0.0,
            "min_beam_size": None,
            "maj_beam_size": None,
            "expected": Quantity(0.0, u.Jy),
        },
        {
            "name": "Case 7: Negative Confusion Noise",
            "confusion_noise": -2.0,
            "min_beam_size": None,
            "maj_beam_size": None,
            "expected": Quantity(-2.0, u.Jy),
        },
    ]

    # Define invalid test cases
    invalid_test_cases = [
        {
            "name": "Invalid Type for Confusion Noise",
            "confusion_noise": "invalid",
            "expected_exception": TypeError,
        },
    ]

    def test_valid_confusion_noise_cases(self):
        """
        Test valid scenarios for `get_confusion_noise` using subTest().

        The following cases are tested:
        - Case 1: Only confusion_noise.
        - Case 2: Confusion_noise with beam sizes.
        - Case 3: min_beam_size is None.
        - Case 4: maj_beam_size is None.
        - Case 5: Both min and maj beam sizes are None.
        - Case 6: Confusion_noise is zero.
        - Case 7: Confusion_noise is negative.
        """
        for case in self.valid_test_cases:
            with self.subTest(name=case["name"]):
                result = get_confusion_noise(
                    confusion_noise=case["confusion_noise"],
                    min_beam_size=case["min_beam_size"],
                    maj_beam_size=case["maj_beam_size"],
                )
                self.assertAlmostEqual(
                    result.value,
                    case["expected"].value,
                    places=6,
                    msg=f"Failed for {case['name']}",
                )
                self.assertEqual(
                    result.unit,
                    case["expected"].unit,
                    f"Unit mismatch for {case['name']}",
                )

    def test_invalid_cases(self):
        """
        Test invalid inputs that should raise exceptions.

        The following cases are tested:
        - Case 8: Invalid type for `confusion_noise`.
        """
        for case in self.invalid_test_cases:
            with self.subTest(name=case["name"]):
                with self.assertRaises(case["expected_exception"]):
                    get_confusion_noise(confusion_noise=case["confusion_noise"])


class TestSensitivityLimitReached(unittest.TestCase):
    """
    Unit tests for `sensitivity_limit_reached` to verify correctness across various cases,
    including valid computations, edge cases, and error handling.
    """

    # Define valid test cases
    valid_test_cases = [
        {
            "name": "Within Confusion Noise Range",
            "confusion_noise": Quantity(5.0, u.Jy),
            "total_sensitivity": Quantity(6.0, u.Jy),
            "expected": True,
        },
        {
            "name": "Exactly at Confusion Noise",
            "confusion_noise": Quantity(5.0, u.Jy),
            "total_sensitivity": Quantity(5.0, u.Jy),
            "expected": False,
        },
        {
            "name": "Exactly at 2x Confusion Noise",
            "confusion_noise": Quantity(5.0, u.Jy),
            "total_sensitivity": Quantity(10.0, u.Jy),
            "expected": False,
        },
        {
            "name": "Below Confusion Noise",
            "confusion_noise": Quantity(5.0, u.Jy),
            "total_sensitivity": Quantity(3.0, u.Jy),
            "expected": False,
        },
        {
            "name": "Above 2x Confusion Noise",
            "confusion_noise": Quantity(5.0, u.Jy),
            "total_sensitivity": Quantity(12.0, u.Jy),
            "expected": False,
        },
        {
            "name": "Within Range Jy/beam",
            "confusion_noise": Quantity(2.0, u.Jy / u.beam),
            "total_sensitivity": Quantity(3.0, u.Jy / u.beam),
            "expected": True,
        },
    ]

    # Define edge case tests
    edge_case_tests = [
        {
            "name": "Confusion Noise as String",
            "confusion_noise": "N/A",
            "total_sensitivity": Quantity(5.0, u.Jy),
            "expected": False,
        },
        {
            "name": "Total Sensitivity as String",
            "confusion_noise": Quantity(5.0, u.Jy),
            "total_sensitivity": "N/A",
            "expected": False,
        },
        {
            "name": "Negative Confusion Noise",
            "confusion_noise": Quantity(-5.0, u.Jy),
            "total_sensitivity": Quantity(6.0, u.Jy),
            "expected": False,
        },
        {
            "name": "Zero Confusion Noise",
            "confusion_noise": Quantity(0.0, u.Jy),
            "total_sensitivity": Quantity(5.0, u.Jy),
            "expected": False,
        },
    ]

    def test_valid_cases(self):
        """
        Test valid scenarios for `sensitivity_limit_reached`.
        """
        for case in self.valid_test_cases:
            with self.subTest(case["name"]):
                result = sensitivity_limit_reached(
                    confusion_noise=case["confusion_noise"],
                    total_sensitivity=case["total_sensitivity"],
                )
                self.assertEqual(result, case["expected"], f"Failed for {case['name']}")

    def test_edge_cases(self):
        """
        Test edge scenarios for `sensitivity_limit_reached`.
        """
        for case in self.edge_case_tests:
            with self.subTest(case["name"]):
                result = sensitivity_limit_reached(
                    confusion_noise=case["confusion_noise"],
                    total_sensitivity=case["total_sensitivity"],
                )
                self.assertEqual(result, case["expected"], f"Failed for {case['name']}")

    def test_invalid_units(self):
        """
        Test invalid units for confusion_noise and total_sensitivity.
        """
        with self.assertRaises(u.UnitConversionError):
            sensitivity_limit_reached(
                confusion_noise=Quantity(5.0, u.m),
                total_sensitivity=Quantity(6.0, u.Jy),
            )

        with self.assertRaises(u.UnitConversionError):
            sensitivity_limit_reached(
                confusion_noise=Quantity(5.0, u.Jy),
                total_sensitivity=Quantity(6.0, u.s),
            )


class TestSubarrayConfigurationFromInput:
    """
    Tests for TestSubarrayConfigurationFromInput() in common services. Checks that correct  subarray configs
    are retrieved for Mid and Low subarrays and that custom mode is set correctly.
    """

    def test_low_AA1(self):
        """
        Test low AA1 subarray configuration correctly retrieved from subarray input.
        """
        user_input = {
            "subarray_configuration": "LOW_AA1_all",
            "integration_time_h": 1,
            "pointing_centre": "00:00:00.0 00:00:00.0",
            "elevation_limit": 20,
            "freq_centre_mhz": 200,
            "spectral_averaging_factor": 1,
            "bandwidth_mhz": 75,
        }
        telescope = Telescope.LOW
        error_messages = []

        subarray = subarray_configuration_from_input(
            user_input, telescope, error_messages
        )

        assert subarray == LOWArrayConfiguration.LOW_AA1_ALL

    def test_low_AA4_CORE_ONLY(self):
        """
        Test low AA4* core only subarray configuration correctly retrieved from subarray input.
        """
        user_input = {
            "subarray_configuration": "LOW_AAstar_core_only",
            "integration_time_h": 1,
            "pointing_centre": "00:00:00.0 00:00:00.0",
            "elevation_limit": 20,
            "freq_centre_mhz": 200,
            "spectral_averaging_factor": 1,
            "bandwidth_mhz": 75,
        }
        telescope = Telescope.LOW
        error_messages = []

        subarray = subarray_configuration_from_input(
            user_input, telescope, error_messages
        )

        assert subarray == LOWArrayConfiguration.LOW_AASTAR_CORE_ONLY

    def test_low_custom(self):
        """
        Test low custom subarray configuration correctly retrieved from input.
        """
        user_input = {
            "num_stations": 360,
            "integration_time_h": 1,
            "pointing_centre": "00:00:00.0 00:00:00.0",
            "elevation_limit": 20,
            "freq_centre_mhz": 200,
            "spectral_averaging_factor": 1,
            "bandwidth_mhz": 75,
        }
        telescope = Telescope.LOW
        error_messages = []

        subarray = subarray_configuration_from_input(
            user_input, telescope, error_messages
        )

        assert subarray is None

    def test_low_AA4_from_antennas(self):
        """
        Test low AA4 subarray configuration correctly retrieved from antennas input.
        """
        user_input = {
            "num_stations": 512,
            "integration_time_h": 1,
            "pointing_centre": "00:00:00.0 00:00:00.0",
            "elevation_limit": 20,
            "freq_centre_mhz": 200,
            "spectral_averaging_factor": 1,
            "bandwidth_mhz": 75,
        }
        telescope = Telescope.LOW
        error_messages = []

        subarray = subarray_configuration_from_input(
            user_input, telescope, error_messages
        )

        assert subarray == LOWArrayConfiguration.LOW_AA4_ALL

    def test_mid_AA4_from_antennas(self):
        """
        Test mid AA4 subarray configuration correctly retrieved from antennas input.
        """
        user_input = {
            "integration_time_s": 600,
            "rx_band": "Band 2",
            "n_ska": 133,
            "n_meer": 64,
            "freq_centre_hz": 1355000000,
            "bandwidth_hz": 200000000,
            "spectral_averaging_factor": 1,
            "pointing_centre": "00:00:00.0 00:00:00.0",
            "pwv": 10,
            "el": 45,
        }
        telescope = Telescope.MID
        error_messages = []

        subarray = subarray_configuration_from_input(
            user_input, telescope, error_messages
        )

        assert subarray == MIDArrayConfiguration.MID_AA4_ALL

    def test_mid_AA_STAR_AA4_13_5_ONLY_from_antennas(self):
        """
        Test mid AA*/AA4 (13.5-m antennas only) subarray configuration correctly retrieved from antennas input.
        """
        user_input = {
            "integration_time_s": 600,
            "rx_band": "Band 2",
            "n_ska": 0,
            "n_meer": 64,
            "freq_centre_hz": 1355000000,
            "bandwidth_hz": 200000000,
            "spectral_averaging_factor": 1,
            "pointing_centre": "00:00:00.0 00:00:00.0",
            "pwv": 10,
            "el": 45,
        }
        telescope = Telescope.MID
        error_messages = []

        subarray = subarray_configuration_from_input(
            user_input, telescope, error_messages
        )

        assert subarray == MIDArrayConfiguration.MID_AA4_MEERKAT_ONLY

    def test_mid_AA4_AA4_15_ONLY_from_antennas(self):
        """
        Test mid AA4 (15-m antennas only) subarray configuration correctly retrieved from antennas input.
        """
        user_input = {
            "integration_time_s": 600,
            "rx_band": "Band 2",
            "n_ska": 133,
            "n_meer": 0,
            "freq_centre_hz": 1355000000,
            "bandwidth_hz": 200000000,
            "spectral_averaging_factor": 1,
            "pointing_centre": "00:00:00.0 00:00:00.0",
            "pwv": 10,
            "el": 45,
        }
        telescope = Telescope.MID
        error_messages = []

        subarray = subarray_configuration_from_input(
            user_input, telescope, error_messages
        )

        assert subarray == MIDArrayConfiguration.MID_AA4_SKA_ONLY

    def test_mid_AA2(self):
        """
        Test mid AA4 (15-m antennas only) subarray configuration correctly retrieved from input.
        """
        user_input = {
            "integration_time_s": 600,
            "rx_band": "Band 2",
            "subarray_configuration": "AA2",
            "freq_centre_hz": 1355000000,
            "bandwidth_hz": 200000000,
            "spectral_averaging_factor": 1,
            "pointing_centre": "00:00:00.0 00:00:00.0",
            "pwv": 10,
            "el": 45,
        }
        telescope = Telescope.MID
        error_messages = []

        subarray = subarray_configuration_from_input(
            user_input, telescope, error_messages
        )

        assert subarray == MIDArrayConfiguration.MID_AA2_ALL

    def test_mid_custom(self):
        """
        Test mid custom subarray configuration correctly retrieved from antennas input.
        """
        user_input = {
            "integration_time_s": 600,
            "rx_band": "Band 2",
            "n_ska": 45,
            "n_meer": 33,
            "freq_centre_hz": 1355000000,
            "bandwidth_hz": 200000000,
            "spectral_averaging_factor": 1,
            "pointing_centre": "00:00:00.0 00:00:00.0",
            "pwv": 10,
            "el": 45,
        }
        telescope = Telescope.MID
        error_messages = []

        subarray = subarray_configuration_from_input(
            user_input, telescope, error_messages
        )

        assert subarray is None

import pytest
from astropy import units as u

from ska_ost_senscalc.common.confusion_noise_lookup import ConfusionNoiseTable
from ska_ost_senscalc.common.model import BeamSize, Limit

confusion_noise_table = ConfusionNoiseTable()


def test_confusion_noise_returns_upperlimit():
    """
    test that the confusion noise method correctly returns value as an upper limit
    when the beam size is less than 3.817 arcsec.
    """
    beam_size = BeamSize(
        beam_maj=0.0002391291822091305 * u.deg,
        beam_min=0.00018151659471931777 * u.deg,
        beam_pa=-0.3363289977274213 * u.deg,
    )
    confusion_noise = confusion_noise_table.get_confusion_noise(
        350000000.0 * u.Hz, beam_size
    )

    assert confusion_noise.limit == Limit.UPPER


def test_confusion_noise_returns_correct_value():
    """
    A 4.5 arcsec beam at 1.4GHz should return a confusion noise of 3.68902241e-06
    """
    beam_size = BeamSize(
        beam_maj=0.001236945952920966 * u.deg,
        beam_min=0.0012011782702890043 * u.deg,
        beam_pa=-20.804439485320415 * u.deg,
    )
    confusion_noise = confusion_noise_table.get_confusion_noise(
        1400000000.0 * u.Hz, beam_size
    )

    assert confusion_noise.value.value == pytest.approx(5.87596192e-07, rel=0.05)
    assert confusion_noise.limit == Limit.VALUE

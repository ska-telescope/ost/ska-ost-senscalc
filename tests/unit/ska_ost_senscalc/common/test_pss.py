import numpy
import pytest
from astropy.units import Quantity

from ska_ost_senscalc.common.model import CalculatorInputPSS, PulsarMode
from ska_ost_senscalc.common.pss import (
    _find_dispersion_broadening,
    _find_observed_pulse_width,
    _find_scatter_broadening,
    convert_continuum_to_bf_sensitivity,
)


def test_find_scatter_broadening():
    dm = 10.0
    freq_ghz = 0.150
    expected_result = 0.008796051026028125
    assert _find_scatter_broadening(dm, freq_ghz) == expected_result


def test_find_scatter_broadening_0_dm():
    dm = 0.0
    freq_ghz = 0.150
    expected_result = 0.0
    assert _find_scatter_broadening(dm, freq_ghz) == expected_result


def test_find_dispersion_broadening():
    dm = 10.0
    freq_mhz = 150.0
    chan_width_mhz = 14.5 / 1e3
    expected_result = 0.35659259259259257
    assert _find_dispersion_broadening(dm, freq_mhz, chan_width_mhz) == expected_result


def test_find_observed_pulse_width():
    dm = 10.0
    freq_mhz = 150.0
    chan_width_mhz = 14.5 / 1e3
    intrinsic_width = 1.0
    sampling_time = 0.0
    expected_result = 1.061713543101697
    assert numpy.isclose(
        _find_observed_pulse_width(
            intrinsic_width, dm, freq_mhz, chan_width_mhz, sampling_time
        ),
        expected_result,
    )


def test_convert_continuum_to_bf_sensitivity_folded():
    calculator_input = CalculatorInputPSS(
        num_stations=100,
        intrinsic_pulse_width=0.1,
        pulse_period=1.0,
        dm=0.0,
        freq_centre_mhz=150.0,
        chan_width=14467.592,
        pointing_centre="03:30:00 -30:00:00",
        integration_time_h=1,
        bandwidth_mhz=118,
        elevation_limit=20,
    )
    continuum_sensitivity = Quantity(100.0, "uJy / beam")

    expected_result = Quantity(49.74937186, "uJy")

    assert numpy.isclose(
        convert_continuum_to_bf_sensitivity(
            continuum_sensitivity,
            calculator_input,
            calculation_type=PulsarMode.FOLDED_PULSE,
        ),
        expected_result,
    )


def test_convert_continuum_to_bf_sensitivity_single():
    calculator_input = CalculatorInputPSS(
        num_stations=100,
        intrinsic_pulse_width=0.1,
        pulse_period=0.0,
        dm=0.0,
        freq_centre_mhz=150.0,
        chan_width=14467.592,
        pointing_centre="03:30:00 -30:00:00",
        integration_time_h=1,
        bandwidth_mhz=118,
        elevation_limit=20,
    )
    continuum_sensitivity = Quantity(100.0, "uJy / beam")

    expected_result = Quantity(3.55885204, "uJy")

    assert numpy.isclose(
        convert_continuum_to_bf_sensitivity(
            continuum_sensitivity,
            calculator_input,
            calculation_type=PulsarMode.SINGLE_PULSE,
        ),
        expected_result,
    )


def test_convert_continuum_to_bf_supplied_sensitivity_error():
    """
    In the folded-pulse mode, convert_continuum_to_bf_sensitivity() shoud through
    a ValueError if the observed pulse is broader (due to interstellar scattering
    and dispersion) than the pulse period."""
    calculator_input = CalculatorInputPSS(
        num_stations=100,
        intrinsic_pulse_width=1.0,
        pulse_period=7.2,
        dm=10000.0,
        freq_centre_mhz=150.0,
        chan_width=14467.592,
        pointing_centre="03:30:00 -30:00:00",
        integration_time_h=1,
        bandwidth_mhz=118,
        elevation_limit=20,
    )
    continuum_sensitivity = Quantity(100.0, "uJy")

    with pytest.raises(ValueError) as err:
        convert_continuum_to_bf_sensitivity(
            continuum_sensitivity,
            calculator_input,
            calculation_type=PulsarMode.FOLDED_PULSE,
        )

    assert err.value.args == (
        "The effective pulse width (due to interstellar dispersion and"
        " scattering) is larger than the pulse period. Check your inputs.",
    )

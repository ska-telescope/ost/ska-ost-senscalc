"""
Unit tests for the ska_ost_senscalc.mid.api module
At the moment, test the subarrays REST interface.
"""

from http import HTTPStatus
from unittest import mock

import pytest
from flask import json

from ska_ost_senscalc.subarray import MIDArrayConfiguration
from tests import OPEN_API

from ..conftest import DEFAULT_API_PATH

CONFIGURATIONS = [m.value for m in list(MIDArrayConfiguration)]

MID_API_URL = f"{DEFAULT_API_PATH}/mid"

calculate_continuum_params = {
    "n_subbands": 1,
    "supplied_sensitivity": 0.001,
    "sensitivity_unit": "Jy/beam",
    "rx_band": "Band 1",
    "subarray_configuration": "AA4",
    "freq_centre_hz": 797500000.0,
    "bandwidth_hz": 435000000.0,
    "spectral_averaging_factor": 1.0,
    "pointing_centre": "00:00:00.0 00:00:00.0",
    "pwv": 10.0,
    "el": 45.0,
    "subband_supplied_sensitivities_unit": "Jy/beam",
    "weighting_mode": "uniform",
    "robustness": 0.0,
    "alpha": 2.75,
}


calculate_zoom_params = {
    "supplied_sensitivities": [1],
    "sensitivity_unit": "Jy/beam",
    "subarray_configuration": "AA4",
    "rx_band": "Band 1",
    "freq_centres_hz": [0.7e9],
    "spectral_resolutions_hz": [6720.0],
    "total_bandwidths_hz": [100e6],
    "taper": 1,
    "robust": 1,
    "pointing_centre": "13:25:27.60 -43:01:09.00",
    "weighting_mode": "natural",
}

weighting_continuum_params = {
    "spectral_mode": "line",
    "freq_centre_hz": 1,
    "pointing_centre": "13:25:27.60 -43:01:09.00",
    "weighting_mode": "natural",
    "subarray_configuration": "AA4",
    "taper": 1,
    "robust": 1,
    "subband_freq_centres_hz": [1],
}


class TestContinuum:
    @mock.patch("ska_ost_senscalc.mid.api.convert_continuum_input_and_calculate")
    def test_calculate(self, mock_calculate_fn, client):
        """
        Test the happy path for the continuum calculate endpoint should
        return wrap the response returned from the service
        """

        mock_calculate_fn.return_value = {"mock_service_result": True}
        rv = client.get(
            f"{MID_API_URL}/continuum/calculate",
            query_string=calculate_continuum_params,
        )

        response = json.loads(rv.data)

        assert rv.status_code == HTTPStatus.OK
        assert response == {"mock_service_result": True}

    @pytest.mark.parametrize(
        "optional_params, error_message",
        [
            (dict(eta_coherence=100), "100.0 is greater than the maximum of 1"),
            (
                dict(rx_band="Not a valid band"),
                "'Not a valid band' is not one of ['Band 1', 'Band 2', 'Band 3', 'Band 4', 'Band 5a', 'Band 5b']",
            ),
            (
                dict(el="not a number"),
                "Wrong type, expected 'number' for query parameter 'el'",
            ),
            (dict(n_subbands=0), "0 is less than the minimum of 1"),
            (
                dict(subarray_configuration="not a subarray_configuration"),
                f"'not a subarray_configuration' is not one of {CONFIGURATIONS}",
            ),
        ],
    )
    def test_calculate_openapi_validation_exception(
        self, optional_params, error_message, client
    ):
        """
        Test basic validation is done by Connexion using the OpenAPI spec,
        eg min and max of numeric parameters or enums
        """
        params = calculate_continuum_params | optional_params
        rv = client.get(
            f"{MID_API_URL}/continuum/calculate",
            query_string=params,
        )

        response = json.loads(rv.data)

        assert rv.status_code == HTTPStatus.BAD_REQUEST
        assert all(
            field in response.keys()
            for field in get_response_fields("/continuum/calculate", "400")
        )
        assert error_message in response["detail"]

    @mock.patch("ska_ost_senscalc.mid.api.convert_continuum_input_and_calculate")
    def test_calculate_general_exception(self, mock_calculate_fn, client):
        """
        Test calculate handles and formats other exceptions into and Internal Server Error
        """
        mock_calculate_fn.side_effect = Exception("Mock Exception")

        rv = client.get(
            f"{MID_API_URL}/continuum/calculate",
            query_string=calculate_continuum_params,
        )
        response = json.loads(rv.data)

        assert rv.status_code == HTTPStatus.INTERNAL_SERVER_ERROR
        assert "Internal Server Error" == response["title"]


class TestZoom:
    def test_calculate(self, client):
        """
        Test the happy path for the zoom calculate endpoint
        """

        params = {
            "supplied_sensitivities": [1],
            "sensitivity_unit": "Jy/beam",
            "subarray_configuration": "AA4",
            "rx_band": "Band 1",
            "freq_centres_hz": [0.7e9],
            "spectral_resolutions_hz": [6720.0],
            "total_bandwidths_hz": [100e6],
            "taper": 1,
            "robust": 1,
            "pointing_centre": "13:25:27.60 -43:01:09.00",
            "weighting_mode": "natural",
        }

        response = client.get(f"{MID_API_URL}/zoom/calculate", query_string=params)

        res = json.loads(response.data)

        assert response.status_code == HTTPStatus.OK

        expected = {
            "calculate": [
                {
                    "freq_centre": {"value": 700000000.0, "unit": "Hz"},
                    "spectropolarimetry_results": {
                        "fwhm_of_the_rmsf": {
                            "value": 65.43350823944124,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth_extent": {
                            "value": 19.66197331442827,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth": {
                            "value": 393791.8807704723,
                            "unit": "rad / m2",
                        },
                    },
                    "spectral_integration_time": {
                        "value": pytest.approx(0.005732881676504537, rel=1e-14),
                        "unit": "s",
                    },
                    "warnings": [],
                }
            ],
            "weighting": {
                "spectral_weighting": [
                    {
                        "freq_centre": {"value": 700000000.0, "unit": "Hz"},
                        "weighting_factor": 1.1144779900294846,
                        "sbs_conv_factor": 31066.871881372997,
                        "confusion_noise": {
                            "value": 5.156636554295171e-06,
                            "limit_type": "value",
                        },
                        "beam_size": {
                            "beam_maj_scaled": 0.0025185386431054633,
                            "beam_min_scaled": 0.0024596128128419686,
                            "beam_pa": -8.817971193186484,
                        },
                    }
                ]
            },
            "weighted_result": [
                {
                    "spectral_confusion_noise": {
                        "value": 5.156636554295171e-06,
                        "unit": "Jy",
                    },
                    "spectral_synthesized_beam_size": {
                        "beam_maj": {"value": 9.066739115179669, "unit": "arcsec"},
                        "beam_min": {"value": 8.854606126231086, "unit": "arcsec"},
                    },
                    "spectral_integration_time": {
                        "value": pytest.approx(0.005732881676504537, rel=1e-14),
                        "unit": "s",
                    },
                    "warnings": [],
                }
            ],
        }
        assert expected == res

    @pytest.mark.parametrize(
        "optional_params, error_message",
        [
            (dict(eta_coherence=100), "100.0 is greater than the maximum of 1"),
            (
                dict(rx_band="Not a valid band"),
                "'Not a valid band' is not one of ['Band 1', 'Band 2', 'Band 3', 'Band 4', 'Band 5a', 'Band 5b']",
            ),
            (
                dict(el="not a number"),
                "Wrong type, expected 'number' for query parameter 'el'",
            ),
        ],
    )
    def test_calculate_openapi_validation_exception(
        self, optional_params, error_message, client
    ):
        """
        Test basic validation is done by Connexion using the OpenAPI spec,
        eg min and max of numeric parameters or enums
        """
        params = {
            "supplied_sensitivities": [1],
            "sensitivity_unit": "Jy/beam",
            "subarray_configuration": "AA4",
            "rx_band": "Band 1",
            "freq_centres_hz": [0.7e9],
            "spectral_resolutions_hz": [6720.0],
            "total_bandwidths_hz": [100e6],
            "taper": 1,
            "robust": 1,
            "pointing_centre": "13:25:27.60 -43:01:09.00",
            "weighting_mode": "natural",
        }

        params = params | optional_params
        rv = client.get(f"{MID_API_URL}/zoom/calculate", query_string=params)

        response = json.loads(rv.data)

        assert rv.status_code == HTTPStatus.BAD_REQUEST
        assert all(
            field in response.keys()
            for field in get_response_fields("/zoom/calculate", "400")
        )
        assert error_message in response["detail"]

    @mock.patch("ska_ost_senscalc.mid.api.get_zoom_calculate_response")
    def test_calculate_general_exception(self, mock_calculate_fn, client):
        """
        Test calculate handles and formats other exceptions into and Internal Server Error
        """
        e_msg = "Mock Exception"

        mock_calculate_fn.side_effect = Exception(e_msg)

        params = {
            "supplied_sensitivities": [1],
            "sensitivity_unit": "Jy/beam",
            "subarray_configuration": "AA4",
            "rx_band": "Band 1",
            "freq_centres_hz": [0.7e9],
            "spectral_resolutions_hz": [6720.0],
            "total_bandwidths_hz": [100e6],
            "taper": 1,
            "robust": 1,
            "pointing_centre": "13:25:27.60 -43:01:09.00",
            "weighting_mode": "natural",
        }

        rv = client.get(f"{MID_API_URL}/zoom/calculate", query_string=params)
        response = json.loads(rv.data)

        assert rv.status_code == HTTPStatus.INTERNAL_SERVER_ERROR
        assert "Internal Server Error" == response["title"]

    def test_weighting(self, client):
        """
        Test the happy path for the weighting endpoint
        """

        params = {
            "supplied_sensitivities": [11.5],
            "sensitivity_unit": "Jy/beam",
            "subarray_configuration": "AA4",
            "rx_band": "Band 1",
            "freq_centres_hz": [0.7e9],
            "spectral_resolutions_hz": [6720.0],
            "total_bandwidths_hz": [100e6],
            "taper": 4,
            "robust": 2,
            "pointing_centre": "13:25:27.60 -43:01:09.00",
            "weighting_mode": "natural",
        }
        response = client.get(f"{MID_API_URL}/zoom/calculate", query_string=params)
        res = json.loads(response.data)

        assert response.status_code == HTTPStatus.OK

        expected = {
            "calculate": [
                {
                    "freq_centre": {"value": 700000000.0, "unit": "Hz"},
                    "spectropolarimetry_results": {
                        "fwhm_of_the_rmsf": {
                            "value": 65.43350823944124,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth_extent": {
                            "value": 19.66197331442827,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth": {
                            "value": 393791.8807704723,
                            "unit": "rad / m2",
                        },
                    },
                    "spectral_integration_time": {
                        "value": pytest.approx(5.5884417186210086e-05, rel=1e-17),
                        "unit": "s",
                    },
                    "warnings": [],
                }
            ],
            "weighting": {
                "spectral_weighting": [
                    {
                        "freq_centre": {"value": 700000000.0, "unit": "Hz"},
                        "weighting_factor": 1.2654011074844083,
                        "sbs_conv_factor": 3398.3877064433764,
                        "confusion_noise": {
                            "value": 4.260610407459923e-05,
                            "limit_type": "value",
                        },
                        "beam_size": {
                            "beam_maj_scaled": 0.007908979215740795,
                            "beam_min_scaled": 0.0071601073511640585,
                            "beam_pa": 691.1923137737365,
                        },
                    }
                ]
            },
            "weighted_result": [
                {
                    "spectral_confusion_noise": {
                        "value": 4.260610407459923e-05,
                        "unit": "Jy",
                    },
                    "spectral_synthesized_beam_size": {
                        "beam_maj": {"value": 28.47232517666686, "unit": "arcsec"},
                        "beam_min": {"value": 25.77638646419061, "unit": "arcsec"},
                    },
                    "spectral_integration_time": {
                        "value": pytest.approx(5.5884417186210086e-05, rel=1e-17),
                        "unit": "s",
                    },
                    "warnings": [],
                }
            ],
        }
        assert expected == res

    def test_weighting_openapi_validation_exception(self, client):
        """
        Test basic validation is done by Connexion using the OpenAPI spec,
        eg min and max of numeric parameters or enums
        """
        params = {
            "supplied_sensitivities": [1],
            "sensitivity_unit": "Jy/beam",
            "subarray_configuration": "AA4",
            "rx_band": "Band 1",
            "freq_centres_hz": [0.7e9],
            "spectral_resolutions_hz": [6720.0],
            "total_bandwidths_hz": [100e6],
            "taper": 1,
            "robust": 1,
            "pointing_centre": "13:25:27.60 -43:01:09.00",
            "weighting_mode": "natural",
        }
        params = params | dict(subarray_configuration="not a subarray_configuration")
        rv = client.get(f"{MID_API_URL}/zoom/calculate", query_string=params)

        response = json.loads(rv.data)

        assert rv.status_code == HTTPStatus.BAD_REQUEST
        msg = f"'not a subarray_configuration' is not one of {CONFIGURATIONS}"
        assert msg in response["detail"]

    @mock.patch("ska_ost_senscalc.mid.service.get_zoom_weighting_response")
    def test_weighting_exception(self, mock_weighting_fn, client):
        """
        Test weighting handles and formats other exceptions into and Internal Server Error
        """
        e_msg = "Mock Exception"

        mock_weighting_fn.side_effect = Exception(e_msg)

        params = {
            "supplied_sensitivities": [1],
            "sensitivity_unit": "Jy/beam",
            "subarray_configuration": "AA4",
            "rx_band": "Band 1",
            "freq_centres_hz": [0.7e9],
            "spectral_resolutions_hz": [6720.0],
            "total_bandwidths_hz": [100e6],
            "taper": 1,
            "robust": 1,
            "pointing_centre": "13:25:27.60 -43:01:09.00",
            "weighting_mode": "natural",
        }
        rv = client.get(f"{MID_API_URL}/zoom/calculate", query_string=params)
        response = json.loads(rv.data)

        assert rv.status_code == HTTPStatus.INTERNAL_SERVER_ERROR
        assert "Internal Server Error" == response["title"]


def test_subarrays_list(client):
    """Test the subarrays entry point."""
    rv = client.get(f"{MID_API_URL}/subarrays")
    data = json.loads(rv.data)
    # Check that the placeholder subarray entered in AT2-606 is removed when necessary
    assert {
        "name": "MID_AA4_all",
        "label": "AA4",
        "n_ska": 133,
        "n_meer": 64,
    } in data
    assert {
        "name": "MID_AA4_MeerKAT_only",
        "label": "AA*/AA4 (13.5-m antennas only)",
        "n_ska": 0,
        "n_meer": 64,
    } in data
    assert {
        "name": "MID_AA4_SKA_only",
        "label": "AA4 (15-m antennas only)",
        "n_ska": 133,
        "n_meer": 0,
    } in data


def get_response_fields(path: str, http_status: str):
    return OPEN_API["paths"][path]["get"]["responses"][http_status]["content"][
        "application/json"
    ]["schema"]["properties"].keys()

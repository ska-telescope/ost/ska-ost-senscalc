from unittest.mock import patch

import astropy.units as u
import pytest
from astropy.coordinates import SkyCoord
from astropy.units.quantity import Quantity

from ska_ost_senscalc.mid.calculator import _t_sys_ska_meer  # noqa
from ska_ost_senscalc.mid.calculator import (
    DEFAULT_ALPHA,
    DEFAULT_EL,
    DEFAULT_PWV,
    _eta_system,
    _n_ska_meer,
    _prepare_input,
    _t_gal_ska_meer,
    _t_rx_ska_meer,
    _t_sky_ska_meer,
    _t_spl_ska_meer,
    calculate_integration_time,
    calculate_sensitivity,
    prepare_integration_input,
    prepare_sensitivity_input,
)
from ska_ost_senscalc.subarray import MIDArrayConfiguration

TARGET = SkyCoord(359.94423568 * u.deg, -00.04616002 * u.deg, frame="galactic")
FREQUENCY = 0.5 * u.GHz
BAND = "Band 1"
El_90 = 90 * u.deg


def test_cascade_from_eta_point_to_eta_system():
    eta_system = _eta_system(
        frequency=FREQUENCY,
        rx_band=BAND,
        eta_system=None,
        eta_pointing=None,
        eta_coherence=None,
        eta_digitisation=None,
        eta_correlation=None,
        eta_bandpass=None,
    )
    assert eta_system == pytest.approx(0.9789931320953944)

    eta_system = _eta_system(
        frequency=FREQUENCY,
        rx_band=BAND,
        eta_system=None,
        eta_pointing=0.5,
        eta_coherence=None,
        eta_digitisation=None,
        eta_correlation=None,
        eta_bandpass=None,
    )
    assert eta_system < 0.9789931320953944


def test_cascade_from_eta_point_to_eta_system_sensitivity_and_integration_time():
    common = dict(
        rx_band=BAND,
        freq_centre_hz=FREQUENCY,
        target=TARGET,
        bandwidth_hz=0.2 * u.GHz,
        subarray_configuration=MIDArrayConfiguration.MID_AA4_SKA_ONLY,
        el=El_90,
    )
    sensitivity_input = prepare_sensitivity_input(integration_time=100, **common)
    integration_input = prepare_integration_input(
        sensitivity=(10 * u.uJy).to(u.Jy).value, **common
    )

    sensitivity = calculate_sensitivity(sensitivity_input)
    integration_time = calculate_integration_time(integration_input)
    assert pytest.approx(0.00039411, abs=1e-8) == sensitivity.to_value(u.Jy)
    assert pytest.approx(155325, abs=1) == integration_time.to_value(u.s)

    common = dict(
        rx_band=BAND,
        freq_centre_hz=FREQUENCY,
        target=TARGET,
        bandwidth_hz=0.2 * u.GHz,
        subarray_configuration=MIDArrayConfiguration.MID_AA4_SKA_ONLY,
        el=El_90,
        eta_pointing=0.5,
    )
    sensitivity_input = prepare_sensitivity_input(integration_time=1000, **common)
    integration_input = prepare_integration_input(
        sensitivity=(100 * u.uJy).to(u.Jy).value, **common
    )

    sensitivity = calculate_sensitivity(sensitivity_input)
    integration_time = calculate_integration_time(integration_input)
    assert pytest.approx(0.00024926, abs=1e-8) == sensitivity.to_value(u.Jy)
    assert pytest.approx(6213, abs=1) == integration_time.to_value(u.s)


@patch("ska_ost_senscalc.mid_utilities.eta_point")
def test_does_not_override_of_eta_pointing_on_init(fake_eta_point):
    res = _eta_system(
        frequency=FREQUENCY,
        rx_band=BAND,
        eta_system=0.5,
        eta_pointing=None,
        eta_coherence=None,
        eta_digitisation=None,
        eta_correlation=None,
        eta_bandpass=None,
    )
    assert res == 0.5
    assert not fake_eta_point.called


def test_override_n_antennas():
    n_ska, n_meer = _n_ska_meer(
        subarray_configuration=MIDArrayConfiguration.MID_AA4_ALL, n_ska=100, n_meer=100
    )

    assert n_ska == 133
    assert n_meer == 64

    with patch(
        "ska_ost_senscalc.mid.calculator.subarray_storage"
    ) as fake_load_by_label:
        n_ska, n_meer = _n_ska_meer(subarray_configuration=None, n_ska=32, n_meer=20)
        assert n_ska == 32
        assert n_meer == 20
        assert not fake_load_by_label.called


# NOTE: These tests consider T_gal to be an array always
def test_t_gal_default_values():
    t_gal_ska, t_gal_meer = _t_gal_ska_meer(
        target=TARGET,
        frequency=FREQUENCY,
        t_gal_ska=None,
        t_gal_meer=None,
        alpha=DEFAULT_ALPHA,
    )

    assert t_gal_ska.value == pytest.approx(395.3821529908282)
    assert t_gal_meer.value == pytest.approx(374.6589205729342)


def test_override_t_gal():
    t_gal_ska, t_gal_meer = _t_gal_ska_meer(
        target=TARGET,
        frequency=FREQUENCY,
        t_gal_ska=300.0 * u.K,
        t_gal_meer=None,
        alpha=DEFAULT_ALPHA,
    )

    assert t_gal_ska.value == 300.0
    assert t_gal_meer.value == pytest.approx(374.65892057)

    t_gal_ska, t_gal_meer = _t_gal_ska_meer(
        target=TARGET,
        frequency=FREQUENCY,
        t_gal_ska=300.0 * u.K,
        t_gal_meer=200.0 * u.K,
        alpha=DEFAULT_ALPHA,
    )

    assert t_gal_ska.value == 300.0
    assert t_gal_meer.value == 200.0


def test_t_sky_default_values():
    t_sky_ska, t_sky_meer = _t_sky_ska_meer(
        frequency=FREQUENCY,
        el=DEFAULT_EL,
        pwv=DEFAULT_PWV,
        t_sky_ska=None,
        t_sky_meer=None,
        t_gal_ska=395.38215299082833 * u.K,
        t_gal_meer=374.6589205729345 * u.K,
    )
    assert t_sky_ska.value == pytest.approx(404.0202599679612)
    assert t_sky_meer.value == pytest.approx(383.41392490804935)


def test_simple_cascade_of_overrides_from_t_sky():
    t_gal_ska, t_gal_meer = _t_gal_ska_meer(
        target=TARGET,
        frequency=FREQUENCY,
        t_gal_ska=None,
        t_gal_meer=None,
        alpha=DEFAULT_ALPHA,  # TODO: move to constants everywhere and maybe pwv
    )

    t_sky_ska, t_sky_meer = _t_sky_ska_meer(
        frequency=FREQUENCY,
        el=DEFAULT_EL,
        pwv=DEFAULT_PWV,
        t_sky_ska=300.0 * u.K,
        t_sky_meer=None,
        t_gal_ska=t_gal_ska,
        t_gal_meer=t_gal_meer,
    )
    assert t_gal_ska.value == pytest.approx(395.38215299082833)
    assert t_gal_meer.value == pytest.approx(374.6589205729345)
    assert t_sky_ska.value == 300.0
    assert t_sky_meer.value == pytest.approx(383.41392491)

    t_gal_ska, t_gal_meer = _t_gal_ska_meer(
        target=TARGET,
        frequency=FREQUENCY,
        t_gal_ska=None,
        t_gal_meer=None,
        alpha=DEFAULT_ALPHA,  # TODO: move to constants everywhere and maybe pwv
    )

    t_sky_ska, t_sky_meer = _t_sky_ska_meer(
        frequency=FREQUENCY,
        el=DEFAULT_EL,
        pwv=DEFAULT_PWV,
        t_sky_ska=300.0 * u.K,
        t_sky_meer=200.0 * u.K,
        t_gal_ska=t_gal_ska,
        t_gal_meer=t_gal_meer,
    )

    assert t_gal_ska.value == pytest.approx(395.38215299082833)
    assert t_gal_meer.value == pytest.approx(374.6589205729345)
    assert t_sky_ska.value == 300.0
    assert t_sky_meer.value == 200.0


def test_complex_cascade_of_overrides_from_t_sky():
    t_gal_ska, t_gal_meer = _t_gal_ska_meer(
        target=TARGET,
        frequency=FREQUENCY,
        t_gal_ska=300.0 * u.K,
        t_gal_meer=None,
        alpha=DEFAULT_ALPHA,  # TODO: move to constants everywhere and maybe pwv
    )

    t_sky_ska, t_sky_meer = _t_sky_ska_meer(
        frequency=FREQUENCY,
        el=DEFAULT_EL,
        pwv=DEFAULT_PWV,
        t_sky_ska=None,
        t_sky_meer=None,
        t_gal_ska=t_gal_ska,
        t_gal_meer=t_gal_meer,
    )
    # Manually set Tgal
    assert t_gal_ska.value == 300.0
    assert t_gal_meer.value == pytest.approx(374.6589205729345)
    assert t_sky_ska.value == pytest.approx(309.17614667376097)
    assert t_sky_meer.value == pytest.approx(383.41392491)

    t_gal_ska, t_gal_meer = _t_gal_ska_meer(
        target=TARGET,
        frequency=FREQUENCY,
        t_gal_ska=300.0 * u.K,
        t_gal_meer=200.0 * u.K,
        alpha=DEFAULT_ALPHA,  # TODO: move to constants everywhere and maybe pwv
    )

    t_sky_ska, t_sky_meer = _t_sky_ska_meer(
        frequency=FREQUENCY,
        el=DEFAULT_EL,
        pwv=DEFAULT_PWV,
        t_sky_ska=None,
        t_sky_meer=None,
        t_gal_ska=t_gal_ska,
        t_gal_meer=t_gal_meer,
    )
    assert t_gal_ska.value == 300.0
    assert t_gal_meer.value == 200.0
    assert t_sky_ska.value == pytest.approx(309.17614667)
    assert t_sky_meer.value == pytest.approx(209.74023511)

    # Manually set t_sky
    t_gal_ska, t_gal_meer = _t_gal_ska_meer(
        target=TARGET,
        frequency=FREQUENCY,
        t_gal_ska=300.0 * u.K,
        t_gal_meer=200.0 * u.K,
        alpha=DEFAULT_ALPHA,  # TODO: move to constants everywhere and maybe pwv
    )

    t_sky_ska, t_sky_meer = _t_sky_ska_meer(
        frequency=FREQUENCY,
        el=DEFAULT_EL,
        pwv=DEFAULT_PWV,
        t_sky_ska=350.0 * u.K,
        t_sky_meer=None,
        t_gal_ska=t_gal_ska,
        t_gal_meer=t_gal_meer,
    )
    assert t_gal_ska == Quantity(300, "K")
    assert t_gal_meer.value == 200.0
    assert t_sky_ska.value == 350.0
    assert t_sky_meer.value == pytest.approx(209.74023511)

    t_gal_ska, t_gal_meer = _t_gal_ska_meer(
        target=TARGET,
        frequency=FREQUENCY,
        t_gal_ska=300.0 * u.K,
        t_gal_meer=200.0 * u.K,
        alpha=DEFAULT_ALPHA,
    )

    t_sky_ska, t_sky_meer = _t_sky_ska_meer(
        frequency=FREQUENCY,
        el=DEFAULT_EL,
        pwv=DEFAULT_PWV,
        t_sky_ska=350.0 * u.K,
        t_sky_meer=250.0 * u.K,
        t_gal_ska=t_gal_ska,
        t_gal_meer=t_gal_meer,
    )
    assert t_gal_ska == Quantity(300, "K")
    assert t_gal_meer.value == 200.0
    assert t_sky_ska.value == 350.0
    assert t_sky_meer.value == 250


def test_t_spl_default_values():
    t_spl_ska, t_spl_meer = _t_spl_ska_meer(t_spl_ska=None, t_spl_meer=None)

    assert t_spl_ska.value == 3.0
    assert t_spl_meer.value == 4.0


def test_t_rx_default_values():
    t_rx_ska, t_rx_meer = _t_rx_ska_meer(
        frequency=FREQUENCY, rx_band=BAND, t_rx_ska=None, t_rx_meer=None
    )

    assert t_rx_ska.value == pytest.approx(16.875)
    assert t_rx_meer.value == pytest.approx(11.36)


def test_t_sys_default_values():
    t_gal_ska, t_gal_meer = _t_gal_ska_meer(
        target=TARGET,
        frequency=FREQUENCY,
        t_gal_ska=None,
        t_gal_meer=None,
        alpha=DEFAULT_ALPHA,
    )

    t_sky_ska, t_sky_meer = _t_sky_ska_meer(
        frequency=FREQUENCY,
        el=DEFAULT_EL,
        pwv=DEFAULT_PWV,
        t_sky_ska=None,
        t_sky_meer=None,
        t_gal_ska=t_gal_ska,
        t_gal_meer=t_gal_meer,
    )
    t_spl_ska, t_spl_meer = _t_spl_ska_meer(t_spl_ska=None, t_spl_meer=None)

    t_rx_ska, t_rx_meer = _t_rx_ska_meer(
        frequency=FREQUENCY,
        rx_band=BAND,
        t_rx_ska=None,
        t_rx_meer=None,
    )

    t_sys_ska, t_sys_meer = _t_sys_ska_meer(
        frequency=FREQUENCY,
        t_sky_ska=t_sky_ska,
        t_sky_meer=t_sky_meer,
        t_spl_ska=t_spl_ska,
        t_spl_meer=t_spl_meer,
        t_rx_ska=t_rx_ska,
        t_rx_meer=t_rx_meer,
        t_sys_ska=None,
        t_sys_meer=None,
    )

    assert t_sys_ska.value == pytest.approx(423.88326197299244)
    assert t_sys_meer.value == pytest.approx(398.76192692133105)


@patch("ska_ost_senscalc.mid.calculator.SEFD_array")
def test_sefd_default_values(fake_SEFD_array):
    _prepare_input(
        rx_band=BAND,
        freq_centre_hz=FREQUENCY,
        target=TARGET,
        subarray_configuration=MIDArrayConfiguration.MID_AA4_ALL,
    )

    assert fake_SEFD_array.called
    n_ska, n_meer, sefd_ska, sefd_meer = fake_SEFD_array.call_args[0]
    assert (n_ska, n_meer) == (133, 64)
    assert sefd_ska.value == pytest.approx(1.022529020598906e-22)
    assert sefd_meer.value == pytest.approx(1.1090031168189728e-22)


def test_sefd_array_default_value():
    sefd_array, _, _, _, _ = _prepare_input(
        rx_band=BAND,
        freq_centre_hz=FREQUENCY,
        target=TARGET,
        subarray_configuration=MIDArrayConfiguration.MID_AA4_ALL,
    )
    assert sefd_array.value == pytest.approx(5.338992088808425e-25)


def test_tau_default_values():
    _, _, _, tau, _ = _prepare_input(
        rx_band=BAND,
        freq_centre_hz=FREQUENCY,
        target=TARGET,
        subarray_configuration=MIDArrayConfiguration.MID_AA4_ALL,
    )
    assert tau.value == pytest.approx(0.00565685424949238)

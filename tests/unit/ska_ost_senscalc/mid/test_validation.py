from dataclasses import asdict

import astropy.units as u
import pytest
from astropy.coordinates import SkyCoord
from astropy.units import Hz, Quantity, arcsec, deg, hourangle

from ska_ost_senscalc.common.model import (
    ContinuumRequest,
    Robustness,
    ZoomRequest,
    ZoomRequestPrepared,
)
from ska_ost_senscalc.mid.calculator import DEFAULT_EL, DEFAULT_PWV
from ska_ost_senscalc.mid.validation import (
    Telescope,
    Weighting,
    validate_and_convert_zoom_weighting_params,
    validate_and_set_defaults_for_continuum,
    validate_and_set_defaults_for_zoom,
)
from ska_ost_senscalc.subarray import MIDArrayConfiguration
from tests import ErrorMessages as err_mess

VALID_INPUTS = {
    "rx_band": "Band 1",
    "freq_centre_hz": 0.7e9,
    "bandwidth_hz": 1e9,
    "supplied_sensitivity": 1,
}

DEFAULT_ZOOM_PARAMS = dict(
    rx_band="Band 1",
    subarray_configuration="AA4",
    freq_centres_hz=[797500000],
    spectral_resolutions_hz=[210],
    total_bandwidths_hz=[3125000],
    pointing_centre="10:00:00.0 -30:00:00.0",
)


class TestZoomValidation:
    def test_calculate_defaults_are_set(self):
        pointing_centre = "10:00:00.0 -30:00:00.0"
        params = ZoomRequest(
            rx_band="Band 1",
            freq_centres_hz=[0.7e9],
            total_bandwidths_hz=[0.1e9],
            spectral_resolutions_hz=[0.1e8],
            supplied_sensitivities=[1],
            sensitivity_unit="Jy/beam",
            subarray_configuration="AA4",
            pointing_centre=pointing_centre,
        )

        pointing_centre = SkyCoord(
            pointing_centre,
            frame="icrs",
            unit=(u.hourangle, u.deg),
        )
        expected = {
            "rx_band": "Band 1",
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_ALL,
            "freq_centres_hz": [700000000.0],
            "pointing_centre": pointing_centre,
            "spectral_resolutions_hz": [10000000.0],
            "total_bandwidths_hz": [100000000.0],
            "n_ska": None,
            "n_meer": None,
            "pwv": DEFAULT_PWV,
            "el": DEFAULT_EL.value,
            "spectral_averaging_factor": 1,
            "supplied_sensitivities": [1],
            "sensitivity_unit": "Jy/beam",
            "integration_time_s": None,
            "eta_system": None,
            "eta_pointing": None,
            "eta_coherence": None,
            "eta_digitisation": None,
            "eta_correlation": None,
            "eta_bandpass": None,
            "t_sys_ska": None,
            "t_rx_ska": None,
            "t_spl_ska": None,
            "t_sys_meer": None,
            "t_rx_meer": None,
            "t_spl_meer": None,
            "t_sky_ska": None,
            "t_gal_ska": None,
            "t_gal_meer": None,
            "alpha": 2.75,
            "eta_meer": None,
            "eta_ska": None,
            "weighting_mode": Weighting.UNIFORM,
            "robustness": Robustness.ZERO,
            "taper": Quantity(0, "arcsec"),
            "telescope": Telescope.MID,
            "target": None,
            "freq_centre_hz": None,
            "freq_centres": [Quantity(7.0e08, "Hz")],
            "bandwidth_hz": None,
        }

        res = validate_and_set_defaults_for_zoom(params)
        d = asdict(res)
        assert expected == d

    @pytest.mark.parametrize(
        "params",
        [
            {},
            {"supplied_sensitivities": [1], "integration_time_s": 1},
        ],
    )
    def test_sensitivity_and_integration_time_set_raises_error(self, params):
        params = ZoomRequest(
            **params,
            **DEFAULT_ZOOM_PARAMS,
        )
        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_zoom(params)

        assert err.value.args == (
            "Either 'supplied_sensitivities' or 'integration_time_s' must be specified, they are mutually exclusive.",
        )

    @pytest.mark.parametrize(
        "params",
        [
            {"supplied_sensitivities": [1]},
            {"supplied_sensitivities": [1], "n_ska": 12},
            {"supplied_sensitivities": [1], "n_meer": 12},
            {
                "supplied_sensitivities": [1],
                "subarray_configuration": "AA4",
                "n_meer": 12,
            },
            {
                "supplied_sensitivities": [1],
                "subarray_configuration": "AA4",
                "n_ska": 12,
                "n_meer": 12,
            },
        ],
    )
    def test_array_configuration_with_n_antennas_raises_error(self, params):
        defaults = DEFAULT_ZOOM_PARAMS.copy()
        defaults["subarray_configuration"] = params.get("subarray_configuration")
        params = defaults | params

        params = ZoomRequest(
            **params,
        )

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_zoom(params)

        assert err.value.args == (
            "Only 'array_configuration' or the number of antennas ('n_ska' AND 'n_meer') should be specified.",
        )

    @pytest.mark.parametrize(
        "subarray_configuration",
        ["AA0.5", "AA1", "AA2"],
    )
    def test_subarray_configuration_without_zoom_modes_raises_error(
        self, subarray_configuration
    ):
        params = ZoomRequest(
            rx_band="Band 1",
            total_bandwidths_hz=[0.1e9],
            supplied_sensitivities=[1],
            freq_centres_hz=[1],
            spectral_resolutions_hz=[1],
            subarray_configuration=subarray_configuration,
            pointing_centre=DEFAULT_ZOOM_PARAMS["pointing_centre"],
        )

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_zoom(params)

        assert err.value.args == (
            "No zoom modes are available for this array assembly.",
        )

    @pytest.mark.parametrize(
        "params",
        [
            {  # 'spectral_resolutions_hz' not in input
                "total_bandwidths_hz": [1, 2],
                "freq_centres_hz": [1, 2],
                "rx_band": "Band 1",
                "supplied_sensitivities": [1, 2],
                "subarray_configuration": "AA4",
            },
            {  # 'total_bandwidths_hz' not in input
                "spectral_resolutions_hz": [1, 2],
                "freq_centres_hz": [1, 2],
                "rx_band": "Band 1",
                "supplied_sensitivities": [1, 2],
                "subarray_configuration": "AA4",
            },
            {  # 'freq_centres_hz' not in input
                "total_bandwidths_hz": [1, 2],
                "spectral_resolutions_hz": [1, 2],
                "rx_band": "Band 1",
                "supplied_sensitivities": [1, 2],
                "subarray_configuration": "AA4",
            },
            {  # inputs are different lengths
                "total_bandwidths_hz": [1, 2, 3],
                "spectral_resolutions_hz": [1, 2],
                "freq_centres_hz": [1, 2, 3, 4, 5],
                "rx_band": "Band 1",
                "supplied_sensitivities": [1, 2],
                "subarray_configuration": "AA4",
            },
        ],
    )
    def test_spectral_resolutions_hz_and_freq_centres_hz_and_total_bandwidths_hz_are_set_together(
        self, params
    ):
        params = DEFAULT_ZOOM_PARAMS | params
        params = ZoomRequest(**params)

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_zoom(params)

        assert err.value.args == (
            "Parameters 'freq_centres_hz', 'spectral_resolutions_hz' and 'total_bandwidths_hz' must all be set together and have the same length.",
        )

    def test_supplied_sensitivities_has_correct_length(self):
        params = DEFAULT_ZOOM_PARAMS | {
            "total_bandwidths_hz": [1, 2],
            "spectral_resolutions_hz": [1, 2],
            "freq_centres_hz": [1, 2],
            "rx_band": "Band 1",
            "supplied_sensitivities": [1, 2, 3, 5],
            "subarray_configuration": "AA4",
        }
        params = ZoomRequest(**params)

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_zoom(params)

        assert err.value.args == (
            "Parameter 'supplied_sensitivities' must be set to calculate an integration time for the zoom window. It should have the same length as 'freq_centres_hz', 'spectral_resolutions_hz' and 'total_bandwidths_hz'.",
        )

    @pytest.mark.parametrize(
        "rx_band,array_configuration",
        [
            ("Band 5a", "AA4"),
            ("Band 5b", "AA*"),
        ],
    )
    def test_subarray_allowed_for_band(self, rx_band, array_configuration):
        """
        Bands 5a and 5b are not allowed for subarrays with 13.5 antennas
        """
        params = DEFAULT_ZOOM_PARAMS | {
            "supplied_sensitivities": [1],
            "freq_centres_hz": [200e6],
            "total_bandwidths_hz": [20e6],
            "spectral_resolutions_hz": [200],
            "rx_band": rx_band,
            "subarray_configuration": array_configuration,
        }
        params = ZoomRequest(**params)

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_zoom(params)

        assert err.value.args == (err_mess.SUBARRAY_FOR_BAND,)

    @pytest.mark.parametrize(
        "rx_band,array_configuration,freq_centres_hz,total_bandwidths_hz",
        [
            ("Band 1", "AA4", [0.59e9], [0.7e9]),
            ("Band 1", "AA*/AA4 (13.5-m antennas only)", [0.59e9], [0.7e9]),
            ("Band 2", "AA4", [0.96e9], [0.7e9]),
            ("Band 2", "AA*/AA4 (13.5-m antennas only)", [0.96e9], [0.7e9]),
        ],
    )
    def test_spectral_window_below_minimum(
        self, rx_band, array_configuration, freq_centres_hz, total_bandwidths_hz
    ):
        """
        The size of the band depends on whether it has 15m, 13.5 or a mix of dishes (which is stored in the array_configuration)
        The spectral window centred at the input frequency must fit within this band, so must be greater than the lower limit of the band.
        """
        params = ZoomRequest(
            supplied_sensitivities=[1],
            freq_centres_hz=freq_centres_hz,
            total_bandwidths_hz=total_bandwidths_hz,
            spectral_resolutions_hz=[200],
            rx_band=rx_band,
            subarray_configuration=array_configuration,
            pointing_centre="10:00:00.0 -30:00:00.0",
        )

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_zoom(params)

        assert err.value.args == (err_mess.SPECTRAL_WINDOW,)

    @pytest.mark.parametrize(
        "rx_band,array_configuration,freq_centres_hz,total_bandwidths_hz",
        [
            ("Band 1", "AA4", [1.04e9], [0.7e9]),
            ("Band 1", "AA*/AA4 (13.5-m antennas only)", [1e9], [0.7e9]),
            ("Band 2", "AA4", [1.75e9], [0.7e9]),
            ("Band 2", "AA*/AA4 (13.5-m antennas only)", [1.66e9], [0.7e9]),
        ],
    )
    def test_spectral_window_above_maximum(
        self, rx_band, array_configuration, freq_centres_hz, total_bandwidths_hz
    ):
        """
        The size of the band depends on whether it has 15m, 13.5 or a mix of dishes (which is stored in the array_configuration)
        The spectral window centred at the input frequency must fit within this band, so must be lower than the upper limit of the band.
        """
        params = ZoomRequest(
            supplied_sensitivities=[1],
            freq_centres_hz=freq_centres_hz,
            total_bandwidths_hz=total_bandwidths_hz,
            spectral_resolutions_hz=[200],
            rx_band=rx_band,
            subarray_configuration=array_configuration,
            pointing_centre="10:00:00.0 -30:00:00.0",
        )

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_zoom(params)

        assert err.value.args == (err_mess.SPECTRAL_WINDOW,)


class TestWeightingValidation:
    def test_weighting_zoom_params_happy_path(self):
        pointing_centre = "13:25:27.60 -43:01:09.00"
        params = ZoomRequest(
            weighting_mode="uniform",
            freq_centres_hz=[1.23e6],
            subarray_configuration=MIDArrayConfiguration.MID_AA05_ALL,
            pointing_centre=pointing_centre,
            rx_band=DEFAULT_ZOOM_PARAMS["rx_band"],
            spectral_resolutions_hz=DEFAULT_ZOOM_PARAMS["spectral_resolutions_hz"],
            total_bandwidths_hz=DEFAULT_ZOOM_PARAMS["total_bandwidths_hz"],
        )

        pointing_centre = SkyCoord(
            pointing_centre,
            frame="icrs",
            unit=(u.hourangle, u.deg),
        )
        expected = ZoomRequestPrepared(
            rx_band="Band 1",
            subarray_configuration=MIDArrayConfiguration.MID_AA05_ALL,
            freq_centres_hz=[1230000.0],
            pointing_centre=pointing_centre,
            spectral_resolutions_hz=[210],
            total_bandwidths_hz=[3125000],
            weighting_mode=Weighting.UNIFORM,
            telescope=Telescope.MID,
            taper=0 * u.arcsec,
            robustness=0,
            freq_centres=[Quantity(1230000, "Hz")],
        )

        res = validate_and_convert_zoom_weighting_params(params)
        assert expected == res


class TestContinuumValidation:
    def test_all_default_are_set(self):
        pointing_centre = "10:00:00.0 -30:00:00.0"
        user_input = {
            "integration_time_s": 3600.0,
            "rx_band": "Band 5a",
            "subarray_configuration": "AA0.5",
            "freq_centre_hz": 6.5e9,
            "bandwidth_hz": 4e8,
            "spectral_averaging_factor": 1.0,
            "pointing_centre": pointing_centre,
            "pwv": DEFAULT_PWV,
            "weighting_mode": "uniform",
            "robustness": 0.0,
        }

        pointing_centre = SkyCoord(pointing_centre, frame="icrs", unit=(hourangle, deg))
        expected = ContinuumRequest(
            freq_centre_hz=6500000000.0,
            rx_band="Band 5a",
            bandwidth_hz=400000000.0,
            pointing_centre=pointing_centre,
            weighting_mode=Weighting.UNIFORM,
            subarray_configuration=MIDArrayConfiguration.MID_AA05_ALL,
            robustness=0.0,
            n_subbands=1,
            subband_freq_centres=[],
            spectral_averaging_factor=1.0,
            integration_time_s=3600.0,
            supplied_sensitivity=None,
            sensitivity_unit=u.Jy / u.beam,
            n_ska=None,
            n_meer=None,
            pwv=DEFAULT_PWV,
            el=DEFAULT_EL.value,
            eta_system=None,
            eta_pointing=None,
            eta_coherence=None,
            eta_digitisation=None,
            eta_correlation=None,
            eta_bandpass=None,
            t_sys_ska=None,
            t_rx_ska=None,
            t_spl_ska=None,
            t_sys_meer=None,
            t_rx_meer=None,
            t_spl_meer=None,
            t_sky_ska=None,
            t_gal_ska=None,
            t_gal_meer=None,
            alpha=2.75,
            eta_meer=None,
            eta_ska=None,
            taper=Quantity(unit=arcsec, value=0),
            freq_centre=Quantity(value=6.5e09, unit=Hz),
            telescope=Telescope.MID,
            subband_freq_centres_hz=[],
            subband_supplied_sensitivities=[],
            subband_supplied_sensitivities_unit=u.Jy / u.beam,
            target=None,
        )

        params = ContinuumRequest(**user_input)
        result = validate_and_set_defaults_for_continuum(params)
        assert result == expected

    @pytest.mark.parametrize(
        "user_input",
        [
            {
                "n_subbands": 1,
                "integration_time_s": 3600.0,
                "supplied_sensitivity": 1,
                "rx_band": "Band 5a",
                "subarray_configuration": "AA0.5",
                "freq_centre_hz": 6.5e9,
                "bandwidth_hz": 4e8,
                "spectral_averaging_factor": 1.0,
                "pointing_centre": "10:00:00.0 -30:00:00.0",
                "pwv": DEFAULT_PWV,
                "el": DEFAULT_EL.value,
                "weighting_mode": "uniform",
                "robustness": 0.0,
            }
        ],
    )
    def test_sensitivity_and_integration_time_set_raises_error(self, user_input):
        with pytest.raises(ValueError) as e:
            params = ContinuumRequest(**user_input)
            validate_and_set_defaults_for_continuum(params)
        assert e.value.args == (
            "Either 'supplied_sensitivity' or 'integration_time_s' must be specified, they are mutually exclusive.",
        )

    @pytest.mark.parametrize(
        "user_input",
        [
            {
                "n_subbands": 1,
                "integration_time_s": 3600.0,
                "rx_band": "Band 5a",
                "subarray_configuration": "AA0.5",
                "n_ska": 1,
                "freq_centre_hz": 6.5e9,
                "bandwidth_hz": 4e8,
                "spectral_averaging_factor": 1.0,
                "pointing_centre": "10:00:00.0 -30:00:00.0",
                "pwv": DEFAULT_PWV,
                "el": DEFAULT_EL.value,
                "weighting_mode": "uniform",
                "robustness": 0.0,
            },
            {
                "n_subbands": 1,
                "integration_time_s": 3600.0,
                "rx_band": "Band 5a",
                "subarray_configuration": "AA0.5",
                "n_meer": 1,
                "freq_centre_hz": 6.5e9,
                "bandwidth_hz": 4e8,
                "spectral_averaging_factor": 1.0,
                "pointing_centre": "10:00:00.0 -30:00:00.0",
                "pwv": DEFAULT_PWV,
                "el": DEFAULT_EL.value,
                "weighting_mode": "uniform",
                "robustness": 0.0,
            },
            {
                "n_subbands": 1,
                "integration_time_s": 3600.0,
                "rx_band": "Band 5a",
                "subarray_configuration": "AA0.5",
                "n_ska": 1,
                "n_meer": 1,
                "freq_centre_hz": 6.5e9,
                "bandwidth_hz": 4e8,
                "spectral_averaging_factor": 1.0,
                "pointing_centre": "10:00:00.0 -30:00:00.0",
                "pwv": DEFAULT_PWV,
                "el": DEFAULT_EL.value,
                "weighting_mode": "uniform",
                "robustness": 0.0,
            },
        ],
    )
    def test_array_configuration_with_n_antennas_raises_error(self, user_input):
        with pytest.raises(ValueError) as e:
            params = ContinuumRequest(**user_input)
            validate_and_set_defaults_for_continuum(params)

        assert e.value.args == (
            "Only 'array_configuration' or the number of antennas ('n_ska' AND 'n_meer') should be specified.",
        )

    @pytest.mark.skip(
        reason="refactor test after sensitivity precisely subband_supplied_sensitivities is reworked"
    )
    def test_subband_sensitivities_and_n_subbands_are_set_together(self):
        # 'subband_supplied_sensitivities' not in input
        user_input = {
            "rx_band": "Band 1",
            "freq_centre_hz": 0.7e9,
            "bandwidth_hz": 0.1e9,
            "subarray_configuration": "AA4",
            "n_subbands": 2,
            "supplied_sensitivity": 1,
        }

        with pytest.raises(ValueError) as e:
            params = ContinuumRequest(**user_input)
            validate_and_set_defaults_for_continuum(params)

        assert e.value.args == (
            "Parameter 'subband_supplied_sensitivities' must be set when setting 'supplied_sensitivity' and"
            "'n_subbands' is greater than 1.",
        )

    def test_subband_sensitivities_same_size_as_n_subbands(self):
        user_input = {
            "rx_band": "Band 1",
            "freq_centre_hz": 0.7e9,
            "bandwidth_hz": 0.1e9,
            "subarray_configuration": "AA4",
            "n_subbands": 2,
            "subband_supplied_sensitivities": [1, 2, 3],
            "supplied_sensitivity": 1,
            "pointing_centre": "10:00:00.0 -30:00:00.0",
        }

        user_input = ContinuumRequest(**user_input)

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_continuum(user_input)

        assert err.value.args == (
            "Parameter 'subband_supplied_sensitivities' must have the same length as the value of 'n_subbands' for"
            " 'n_subbands' greater than 1.",
        )

    @pytest.mark.parametrize(
        "rx_band,array_configuration",
        [
            ("Band 5a", "AA4"),
            ("Band 5b", "AA*"),
        ],
    )  # TODO BTN-2206 should refactor the validation logic so that you can test the valid case
    def test_subarray_allowed_for_band(self, rx_band, array_configuration):
        """
        Bands 5a and 5b are not allowed for subarrays with 13.5 antennas
        """
        user_input = {
            "supplied_sensitivity": 1,
            "rx_band": rx_band,
            "subarray_configuration": array_configuration,
            "pointing_centre": "10:00:00.0 -30:00:00.0",
            "freq_centre_hz": 0.7e9,
            "bandwidth_hz": 0.1e9,
        }

        user_input = ContinuumRequest(**user_input)

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_continuum(user_input)

        assert err.value.args == (err_mess.SUBARRAY_FOR_BAND,)

    @pytest.mark.parametrize(
        "rx_band,array_configuration,freq_centre_hz,bandwidth_hz",
        [
            ("Band 1", "AA4", 0.59e9, 0.7e9),
            ("Band 1", "AA2", 0.36e9, 0.7e9),
            ("Band 1", "AA*/AA4 (13.5-m antennas only)", 0.59e9, 0.7e9),
            ("Band 2", "AA4", 0.96e9, 0.7e9),
            ("Band 2", "AA2", 0.96e9, 0.7e9),
            ("Band 2", "AA*/AA4 (13.5-m antennas only)", 0.96e9, 0.7e9),
            ("Band 5a", "AA2", 4.5e9, 0.7e9),
            ("Band 5b", "AA2", 8e9, 0.7e9),
        ],
    )  # TODO BTN-2206 should refactor the validation logic so that you can test the valid case
    def test_spectral_window_below_minimum(
        self, rx_band, array_configuration, freq_centre_hz, bandwidth_hz
    ):
        """
        The size of the band depends on whether it has 15m, 13.5 or a mix of dishes (which is stored in the array_configuration)
        The spectral window centred at the input frequency must fit within this band, so must be greater than the lower limit of the band.
        """
        user_input = {
            "supplied_sensitivity": 1,
            "freq_centre_hz": freq_centre_hz,
            "bandwidth_hz": bandwidth_hz,
            "rx_band": rx_band,
            "subarray_configuration": array_configuration,
            "pointing_centre": "10:00:00.0 -30:00:00.0",
        }

        user_input = ContinuumRequest(**user_input)

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_continuum(user_input)

        assert err.value.args == (err_mess.SPECTRAL_WINDOW,)

    @pytest.mark.parametrize(
        "rx_band,array_configuration,freq_centre_hz,bandwidth_hz",
        [
            ("Band 1", "AA4", 1.04e9, 0.7e9),
            ("Band 1", "AA2", 1e9, 0.7e9),
            ("Band 1", "AA*/AA4 (13.5-m antennas only)", 1e9, 0.7e9),
            ("Band 2", "AA4", 1.75e9, 0.7e9),
            ("Band 2", "AA2", 1.66e9, 0.7e9),
            ("Band 2", "AA*/AA4 (13.5-m antennas only)", 1.66e9, 0.7e9),
            ("Band 5a", "AA2", 8.4e9, 0.7e9),
            ("Band 5b", "AA2", 15.36e9, 0.7e9),
        ],
    )
    def test_spectral_window_above_maximum(
        self, rx_band, array_configuration, freq_centre_hz, bandwidth_hz
    ):
        """
        The size of the band depends on whether it has 15m, 13.5 or a mix of dishes (which is stored in the array_configuration)
        The spectral window centred at the input frequency must fit within this band, so must be lower than the upper limit of the band.
        """
        user_input = {
            "supplied_sensitivity": 1,
            "freq_centre_hz": freq_centre_hz,
            "bandwidth_hz": bandwidth_hz,
            "rx_band": rx_band,
            "subarray_configuration": array_configuration,
            "pointing_centre": "10:00:00.0 -30:00:00.0",
        }

        user_input = ContinuumRequest(**user_input)

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_continuum(user_input)

        assert err.value.args == (err_mess.SPECTRAL_WINDOW,)

    # @pytest.mark.skip(reason="refactor test after sensitivity is reworked")
    @pytest.mark.parametrize(
        "rx_band,array_configuration,bandwidth_hz",
        [
            ("Band 1", "AA0.5", 1e9),
            ("Band 1", "AA1", 1e9),
            ("Band 2", "AA2", 1e9),
        ],
    )  # TODO BTN-2206 should refactor the validation logic so that you can test the valid case
    def test_maximum_bandwidth_exceeded_for_subarray(
        self, rx_band, array_configuration, bandwidth_hz
    ):
        """
        The earlier subarrays have a lower maximum continuum bandwidth. The later ones can use the full bandwidth.
        """
        user_input = {
            "supplied_sensitivity": 1,
            "bandwidth_hz": bandwidth_hz,
            "rx_band": rx_band,
            "subarray_configuration": array_configuration,
            "pointing_centre": "10:00:00.0 -30:00:00.0",
            "freq_centre_hz": 0.7e9,
        }

        user_input = ContinuumRequest(**user_input)

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_continuum(user_input)

        assert err.value.args == (err_mess.MAX_BANDWIDTH_FOR_SUBARRAY(800.0),)

    def test_invalid_pointing(self):
        user_input = {
            "n_subbands": 1,
            "integration_time_s": 3600.0,
            "rx_band": "Band 5a",
            "subarray_configuration": "AA0.5",
            "freq_centre_hz": 6.5e9,
            "bandwidth_hz": 4e8,
            "spectral_averaging_factor": 1.0,
            "pointing_centre": "not a pointing centre",
            "pwv": DEFAULT_PWV,
            "el": DEFAULT_EL.value,
            "weighting_mode": "uniform",
            "robustness": 0.0,
        }

        with pytest.raises(ValueError) as e:
            params = ContinuumRequest(**user_input)
            validate_and_set_defaults_for_continuum(params)

        assert e.value.args == (err_mess.INVALID_POINTING_CENTRE,)

from importlib.metadata import version
from os import getenv

SENSCALC_MAJOR_VERSION = version("ska-ost-senscalc").split(".")[0]
KUBE_NAMESPACE = getenv("KUBE_NAMESPACE", "ska-ost-senscalc")
DEFAULT_API_PATH = f"{KUBE_NAMESPACE}/senscalc/api/v{SENSCALC_MAJOR_VERSION}"
SENS_CALC_URL = getenv(
    "SENS_CALC_URL", f"http://ska-ost-senscalc-rest-test:5000/{DEFAULT_API_PATH}"
)
MID_API_URL = f"{SENS_CALC_URL}/mid"
LOW_API_URL = f"{SENS_CALC_URL}/low"

"""
Unit tests for the ska_ost_senscalc.mid.api module
At the moment, test the subarrays REST interface.
"""

import unittest
from http import HTTPStatus

import numpy
import pytest
import requests
from astropy import units as u
from astropy.coordinates.sky_coordinate import SkyCoord
from flask import json

from ska_ost_senscalc.common.model import MidBand, Weighting
from ska_ost_senscalc.mid.calculator import DEFAULT_ALPHA, DEFAULT_EL, DEFAULT_PWV
from ska_ost_senscalc.subarray import MIDArrayConfiguration

from . import MID_API_URL

CONTINUUM_CALCULATE_PARAMS = {
    "freq_centre_hz": 0.79e9,
    "rx_band": "Band 1",
    "bandwidth_hz": 0.4e9,
    "pointing_centre": "13:25:27.60 -43:01:09.00",
    "n_ska": 130,
    "n_meer": 64,
    "spectral_averaging_factor": 1,
}

WEIGHTING_INPUT_PARAMS = {
    "spectral_mode": "line",
    "freq_centre_hz": 0.79e9,
    "subarray_configuration": "AA4",
    "weighting_mode": "uniform",
    "pointing_centre": "00:00:00.00 10:00:00.00",
    "taper": 4.0,
}

WEIGHTING_INPUT_FOR_COMBINE = {
    "spectral_mode": "continuum",
    "weighting_mode": "uniform",
    # "subarray_configuration": "AA4"
}

CONTINUUM_CALCULATE_INPUT_FOR_COMBINE = {
    "rx_band": "Band 1",
    "bandwidth_hz": 0.4e9,
    "spectral_averaging_factor": 1,
}

TARGET_1 = SkyCoord(359.94423568 * u.deg, -00.04616002 * u.deg, frame="galactic")
POINTING_CENTRE_1: str = TARGET_1.icrs.to_string("hmsdms")

TARGET2 = SkyCoord("13h25m27.6s", "-43d01m09.00s", frame="icrs")
POINTING_CENTRE_2: str = TARGET2.icrs.to_string("hmsdms")

ETA_SYSTEM = 1.0

# Information on these values:
# https://developer.skao.int/projects/ska-ost-senscalc/en/latest/mid_background.html
# It is a Galactic temperature in Kelvin [K] at a given frequency, scaled from the temperature at 408MHz (T_gal_408) using the equation T_gal_nu =  T_gal_408 * (0.408/nu_ghz)^alpha
# alpha is set to 2.75K and the TGAL_1 to 4 change the nu_ghz (where nu = frequency) to those required for the tests.
TGAL_500MHz = 17.1 * (0.408 / 0.5) ** 2.75 * u.K
TGAL_2GHz = 17.1 * (0.408 / 2.0) ** 2.75 * u.K
TGAL_3GHz = 17.1 * (0.408 / 6.0) ** 2.75 * u.K
TGAL_4GHz = 17.1 * (0.408 / 0.7) ** 2.75 * u.K


def _filter_dict_by_key(dict_to_filter, keys_to_filter):
    return dict((k, dict_to_filter[k]) for k in keys_to_filter)


class TestZoomCalculatorCases:
    def test_calculate_sensitivity_band1_zoom(self):
        """
        Converted from test_calculator_case -> test_calculate_sensitivity -> TESTCASE8
        """
        params = {
            "rx_band": "Band 1",
            "freq_centres_hz": (0.7 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_2,
            "bandwidth_hz": (0.21 * u.kHz).to(u.Hz).value,
            "pwv": 5,
            "el": 90,
            "eta_pointing": 0.5,
            "eta_coherence": 0.5,
            "eta_digitisation": 0.5,
            "eta_correlation": 0.5,
            "eta_bandpass": 0.5,
            "n_ska": 100,
            "eta_ska": 0.7,
            "t_spl_ska": 10.0,
            "t_rx_ska": 20.0,
            "n_meer": 30,
            "eta_meer": 0.8,
            "t_spl_meer": 15.0,
            "t_rx_meer": 25.0,
            "t_sky_ska": 15.0,
            "t_sky_meer": 15.0,
            "t_gal_ska": TGAL_4GHz.value,
            "t_gal_meer": TGAL_4GHz.value,
            "integration_time_s": 10,
            "spectral_resolutions_hz": 210,
            "total_bandwidths_hz": 3125000,
            "weighting_mode": "uniform",
        }
        response = requests.get(f"{MID_API_URL}/zoom/calculate", params=params)

        res = json.loads(response.content)

        expected = {
            "calculate": [
                {
                    "freq_centre": {"value": 700000000.0, "unit": "Hz"},
                    "spectral_sensitivity": {"value": 3.911175274870794, "unit": "Jy"},
                    "spectropolarimetry_results": {
                        "fwhm_of_the_rmsf": {
                            "value": 2115.3789494825605,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth_extent": {
                            "value": 17.204460990377626,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth": {
                            "value": 15633376.041209446,
                            "unit": "rad / m2",
                        },
                    },
                    "warnings": [],
                }
            ],
            "weighting": {"spectral_weighting": []},
            "weighted_result": [
                {
                    "weighted_spectral_sensitivity": {
                        "value": 3.911175274870794,
                        "unit": "Jy",
                    },
                    "warnings": [],
                }
            ],
        }
        assert expected == res


class TestZoom(unittest.TestCase):
    def test_calculate_returns_sensitivity(self):
        """
        Test /zoom/calculate returns the correct sensitivities when given an integration time.
        """
        params = {
            "freq_centres_hz": [0.79e9, 0.95e9],
            "rx_band": "Band 1",
            "spectral_resolutions_hz": [1680.0, 3360.0],
            "total_bandwidths_hz": [25e6, 50e6],
            "pointing_centre": "13:25:27.60 -43:01:09.00",
            "n_ska": 130,
            "n_meer": 64,
            "spectral_averaging_factor": 2,
            "integration_time_s": 1000,
            "weighting_mode": "uniform",
        }
        response = requests.get(f"{MID_API_URL}/zoom/calculate", params=params)

        res = json.loads(response.content)

        assert response.status_code == HTTPStatus.OK
        expected = {
            "calculate": [
                {
                    "freq_centre": {"value": 790000000.0, "unit": "Hz"},
                    "spectral_sensitivity": {
                        "value": 0.002602077011576058,
                        "unit": "Jy",
                    },
                    "spectropolarimetry_results": {
                        "fwhm_of_the_rmsf": {
                            "value": 379.9281130382068,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth_extent": {
                            "value": 22.51110069516272,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth": {
                            "value": 1347888.4651955517,
                            "unit": "rad / m2",
                        },
                    },
                    "warnings": [],
                },
                {
                    "freq_centre": {"value": 950000000.0, "unit": "Hz"},
                    "spectral_sensitivity": {
                        "value": pytest.approx(0.0014830014064904186, abs=1e-16),
                        "unit": "Jy",
                    },
                    "spectropolarimetry_results": {
                        "fwhm_of_the_rmsf": {
                            "value": 330.04780108217886,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth_extent": {
                            "value": 33.22880360852665,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth": {
                            "value": 1134879.846123796,
                            "unit": "rad / m2",
                        },
                    },
                    "warnings": [],
                },
            ],
            "weighting": {"spectral_weighting": []},
            "weighted_result": [
                {
                    "weighted_spectral_sensitivity": {
                        "value": 0.002602077011576058,
                        "unit": "Jy",
                    },
                    "warnings": [],
                },
                {
                    "weighted_spectral_sensitivity": {
                        "value": pytest.approx(0.0014830014064904186, abs=1e-16),
                        "unit": "Jy",
                    },
                    "warnings": [],
                },
            ],
        }

        assert res == expected

    def test_calculate_returns_integration_time(self):
        """
        Test /zoom/calculate returns the correct integration time when given a sensitivity
        """
        params = params = {
            "freq_centres_hz": [0.79e9, 0.95e9],
            "rx_band": "Band 1",
            "spectral_resolutions_hz": [1680.0, 3360.0],
            "total_bandwidths_hz": [25e6, 50e6],
            "pointing_centre": "13:25:27.60 -43:01:09.00",
            "n_ska": 130,
            "n_meer": 64,
            "spectral_averaging_factor": 2,
            "supplied_sensitivities": [1.5, 3],
            "weighting_mode": "uniform",
        }
        response = requests.get(
            f"{MID_API_URL}/zoom/calculate",
            params=params,
        )

        res = json.loads(response.content)

        assert response.status_code == HTTPStatus.OK

        expected = {
            "calculate": [
                {
                    "freq_centre": {"value": 790000000.0, "unit": "Hz"},
                    "spectral_integration_time": {
                        "value": 0.003009246566298929,
                        "unit": "s",
                    },
                    "spectropolarimetry_results": {
                        "fwhm_of_the_rmsf": {
                            "value": 379.9281130382068,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth_extent": {
                            "value": 22.51110069516272,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth": {
                            "value": 1347888.4651955517,
                            "unit": "rad / m2",
                        },
                    },
                    "warnings": [],
                },
                {
                    "freq_centre": {"value": 950000000.0, "unit": "Hz"},
                    "spectral_integration_time": {
                        "value": pytest.approx(0.00024436590796139565, abs=1e-18),
                        "unit": "s",
                    },
                    "spectropolarimetry_results": {
                        "fwhm_of_the_rmsf": {
                            "value": 330.04780108217886,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth_extent": {
                            "value": 33.22880360852665,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth": {
                            "value": 1134879.846123796,
                            "unit": "rad / m2",
                        },
                    },
                    "warnings": [],
                },
            ],
            "weighting": {"spectral_weighting": []},
            "weighted_result": [
                {
                    "spectral_integration_time": {
                        "value": 0.003009246566298929,
                        "unit": "s",
                    },
                    "warnings": [],
                },
                {
                    "spectral_integration_time": {
                        "value": pytest.approx(0.00024436590796139565, abs=1e-18),
                        "unit": "s",
                    },
                    "warnings": [],
                },
            ],
        }
        assert res == expected

    def test_calculate_spectropolarimetry_values(self):
        params = params = {
            "freq_centres_hz": [0.79e9, 0.95e9],
            "rx_band": "Band 1",
            "spectral_resolutions_hz": [1680.0, 3360.0],
            "total_bandwidths_hz": [25e6, 50e6],
            "pointing_centre": "13:25:27.60 -43:01:09.00",
            "n_ska": 130,
            "n_meer": 64,
            "spectral_averaging_factor": 2,
            "integration_time_s": 1000,
            "weighting_mode": "uniform",
        }
        response = requests.get(f"{MID_API_URL}/zoom/calculate", params=params)

        res = json.loads(response.content)

        assert response.status_code == HTTPStatus.OK

        expected = {
            "calculate": [
                {
                    "freq_centre": {"value": 790000000.0, "unit": "Hz"},
                    "spectral_sensitivity": {
                        "value": 0.002602077011576058,
                        "unit": "Jy",
                    },
                    "spectropolarimetry_results": {
                        "fwhm_of_the_rmsf": {
                            "value": 379.9281130382068,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth_extent": {
                            "value": 22.51110069516272,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth": {
                            "value": 1347888.4651955517,
                            "unit": "rad / m2",
                        },
                    },
                    "warnings": [],
                },
                {
                    "freq_centre": {"value": 950000000.0, "unit": "Hz"},
                    "spectral_sensitivity": {
                        "value": pytest.approx(0.0014830014064904186, abs=1e-16),
                        "unit": "Jy",
                    },
                    "spectropolarimetry_results": {
                        "fwhm_of_the_rmsf": {
                            "value": 330.04780108217886,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth_extent": {
                            "value": 33.22880360852665,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth": {
                            "value": 1134879.846123796,
                            "unit": "rad / m2",
                        },
                    },
                    "warnings": [],
                },
            ],
            "weighting": {"spectral_weighting": []},
            "weighted_result": [
                {
                    "weighted_spectral_sensitivity": {
                        "value": 0.002602077011576058,
                        "unit": "Jy",
                    },
                    "warnings": [],
                },
                {
                    "weighted_spectral_sensitivity": {
                        "value": pytest.approx(0.0014830014064904186, abs=1e-16),
                        "unit": "Jy",
                    },
                    "warnings": [],
                },
            ],
        }
        assert res == expected

    def test_calculate_exception(self):
        """
        Test /zoom/calculate returns an error response as expected
        """
        params = {
            "freq_centres_hz": [0.79e9, 0.95e9],
            "rx_band": "Band 1",
            "spectral_resolutions_hz": [1680.0, 3360.0],
            "total_bandwidths_hz": [25e6, 50e6],
            "pointing_centre": "13:25:27.60 -43:01:09.00",
            "n_meer": 64,
            "spectral_averaging_factor": 2,
            "supplied_sensitivities": [1.5],
            "n_ska": 230,
            "weighting_mode": "uniform",
        }  # Should not be more than 133

        response = requests.get(f"{MID_API_URL}/zoom/calculate", params=params)
        response_body = json.loads(response.content)

        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert "230 is greater than the maximum of 133" in response_body["detail"]

    def test_integration_time(self):
        params = {
            "rx_band": "Band 1",
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_ALL.value,
            "freq_centres_hz": 797500000,
            "pointing_centre": "10:00:00.0 -30:00:00.0",
            "pwv": DEFAULT_PWV,
            "el": DEFAULT_EL.value,
            "spectral_averaging_factor": 1,
            "spectral_resolutions_hz": 210,
            "total_bandwidths_hz": 3125000,
            "sensitivity_unit": "Jy/beam",
            "integration_time_s": 600,
            "weighting_mode": "uniform",
            "robustness": 0,
            "alpha": DEFAULT_ALPHA,
        }
        response = requests.get(f"{MID_API_URL}/zoom/calculate", params=params)

        res = json.loads(response.content)

        assert response.status_code == HTTPStatus.OK
        expected = {
            "calculate": [
                {
                    "freq_centre": {"value": 797500000.0, "unit": "Hz"},
                    "spectropolarimetry_results": {
                        "fwhm_of_the_rmsf": {
                            "value": 3128.146807579617,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth_extent": {
                            "value": 22.31874920746199,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth": {
                            "value": 23136997.673582196,
                            "unit": "rad / m2",
                        },
                    },
                    "spectral_sensitivity": {
                        "value": 0.006417244735836066,
                        "unit": "Jy",
                    },
                    "warnings": [],
                }
            ],
            "weighting": {
                "spectral_weighting": [
                    {
                        "weighting_factor": 6.8908003470875,
                        "sbs_conv_factor": 7260577.300208139,
                        "confusion_noise": {"value": 0, "limit_type": "value"},
                        "beam_size": {
                            "beam_maj_scaled": 0.00014436813190063763,
                            "beam_min_scaled": 0.00014145069909181785,
                            "beam_pa": 117.95508831639842,
                        },
                        "freq_centre": {"value": 797500000.0, "unit": "Hz"},
                    }
                ]
            },
            "weighted_result": [
                {
                    "weighted_spectral_sensitivity": {
                        "value": 0.04421995225304459,
                        "unit": "Jy",
                    },
                    "spectral_confusion_noise": {"value": 0.0, "unit": "Jy"},
                    "total_spectral_sensitivity": {
                        "value": 0.04421995225304459,
                        "unit": "Jy",
                    },
                    "spectral_synthesized_beam_size": {
                        "beam_maj": {"value": 0.5197252748422955, "unit": "arcsec"},
                        "beam_min": {"value": 0.5092225167305443, "unit": "arcsec"},
                    },
                    "spectral_surface_brightness_sensitivity": {
                        "value": 321062.3815447433,
                        "unit": "K",
                    },
                    "warnings": [],
                }
            ],
        }

        assert expected == res

    def test_integration_time_non_gaussian(self):
        params = {
            "rx_band": "Band 1",
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_ALL.value,
            "freq_centres_hz": 797500000,
            "pointing_centre": "10:00:00.0 -30:00:00.0",
            "pwv": DEFAULT_PWV,
            "el": DEFAULT_EL.value,
            "spectral_averaging_factor": 1,
            "spectral_resolutions_hz": 210,
            "total_bandwidths_hz": 3125000,
            "sensitivity_unit": "Jy/beam",
            "integration_time_s": 600,
            "weighting_mode": "robust",
            "robustness": 2,
            "alpha": DEFAULT_ALPHA,
        }
        response = requests.get(f"{MID_API_URL}/zoom/calculate", params=params)

        res = json.loads(response.content)

        assert response.status_code == HTTPStatus.OK
        expected = {
            "calculate": [
                {
                    "freq_centre": {"value": 797500000.0, "unit": "Hz"},
                    "spectropolarimetry_results": {
                        "fwhm_of_the_rmsf": {
                            "value": 3128.146807579617,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth_extent": {
                            "value": 22.31874920746199,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth": {
                            "value": 23136997.673582196,
                            "unit": "rad / m2",
                        },
                    },
                    "spectral_sensitivity": {
                        "value": 0.006417244735836066,
                        "unit": "Jy",
                    },
                    "warnings": [],
                }
            ],
            "weighting": {
                "spectral_weighting": [
                    {
                        "weighting_factor": 1.0000072483979447,
                        "sbs_conv_factor": 498249.1449202928,
                        "confusion_noise": {
                            "value": 7.248917149009592e-08,
                            "limit_type": "value",
                        },
                        "beam_size": {
                            "beam_maj_scaled": 0.0005495966130911797,
                            "beam_min_scaled": 0.0005414482804413697,
                            "beam_pa": 493.4287550848289,
                        },
                        "freq_centre": {"value": 797500000.0, "unit": "Hz"},
                    }
                ]
            },
            "weighted_result": [
                {
                    "weighted_spectral_sensitivity": {
                        "value": 0.00641729125057962,
                        "unit": "Jy",
                    },
                    "warnings": [],
                }
            ],
        }

        assert expected == res

    def test_sensitivity_limit_reached_value_error(self):
        params = {
            "rx_band": "Band 1",
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_ALL.value,
            "freq_centres_hz": 797500000,
            "pointing_centre": "10:00:00.0 -30:00:00.0",
            "pwv": DEFAULT_PWV,
            "el": DEFAULT_EL.value,
            "spectral_averaging_factor": 1,
            "spectral_resolutions_hz": 210,
            "total_bandwidths_hz": 3125000,
            "supplied_sensitivities": [1],
            "sensitivity_unit": "nJy/beam",
            "weighting_mode": "uniform",
            "robustness": -2,
            "alpha": DEFAULT_ALPHA,
            "taper": 1024,
        }
        response = requests.get(f"{MID_API_URL}/zoom/calculate", params=params)

        res = json.loads(response.content)

        assert response.status_code == HTTPStatus.BAD_REQUEST
        expected = {
            "title": "Validation Error",
            "detail": "Sensitivity (1e-18 Jy / beam) must be greater than or equal to confusion noise (0.050943480600715",
        }
        self.assertEqual(expected["title"], res["title"])
        self.assertIn(expected["detail"], res["detail"])

    def test_sensitivity_limit_reached(self):
        params = {
            "rx_band": "Band 1",
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_ALL.value,
            "freq_centres_hz": 797500000,
            "pointing_centre": "10:00:00.0 -30:00:00.0",
            "pwv": DEFAULT_PWV,
            "el": DEFAULT_EL.value,
            "spectral_averaging_factor": 1,
            "spectral_resolutions_hz": 210,
            "total_bandwidths_hz": 3125000,
            "supplied_sensitivities": [10],
            "weighting_mode": "uniform",
            "alpha": DEFAULT_ALPHA,
        }
        response = requests.get(f"{MID_API_URL}/zoom/calculate", params=params)

        res = json.loads(response.content)

        assert response.status_code == HTTPStatus.OK
        expected = {
            "calculate": [
                {
                    "freq_centre": {"value": 797500000.0, "unit": "Hz"},
                    "spectropolarimetry_results": {
                        "fwhm_of_the_rmsf": {
                            "value": 3128.146807579617,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth_extent": {
                            "value": 22.31874920746199,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth": {
                            "value": 23136997.673582196,
                            "unit": "rad / m2",
                        },
                    },
                    "spectral_integration_time": {
                        "value": 0.011732425063569262,
                        "unit": "s",
                    },
                    "warnings": [],
                }
            ],
            "weighting": {
                "spectral_weighting": [
                    {
                        "weighting_factor": 6.8908003470875,
                        "sbs_conv_factor": 7260577.300208139,
                        "confusion_noise": {"value": 0, "limit_type": "value"},
                        "beam_size": {
                            "beam_maj_scaled": 0.00014436813190063763,
                            "beam_min_scaled": 0.00014145069909181785,
                            "beam_pa": 117.95508831639842,
                        },
                        "freq_centre": {"value": 797500000.0, "unit": "Hz"},
                    }
                ]
            },
            "weighted_result": [
                {
                    "spectral_confusion_noise": {"value": 0.0, "unit": "Jy"},
                    "spectral_synthesized_beam_size": {
                        "beam_maj": {"value": 0.5197252748422955, "unit": "arcsec"},
                        "beam_min": {"value": 0.5092225167305443, "unit": "arcsec"},
                    },
                    "spectral_integration_time": {
                        "value": 0.011732425063569262,
                        "unit": "s",
                    },
                    "warnings": [],
                }
            ],
        }
        assert res == expected

    def test_sensitivity_limit_reached_non_gaussian(self):
        params = {
            "rx_band": "Band 1",
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_ALL.value,
            "freq_centres_hz": 797500000,
            "pointing_centre": "10:00:00.0 -30:00:00.0",
            "pwv": DEFAULT_PWV,
            "el": DEFAULT_EL.value,
            "spectral_averaging_factor": 1,
            "spectral_resolutions_hz": 210,
            "total_bandwidths_hz": 3125000,
            "supplied_sensitivities": [10],
            "weighting_mode": "natural",
            "alpha": DEFAULT_ALPHA,
        }
        response = requests.get(f"{MID_API_URL}/zoom/calculate", params=params)

        res = json.loads(response.content)

        assert response.status_code == HTTPStatus.OK
        expected = {
            "calculate": [
                {
                    "freq_centre": {"value": 797500000.0, "unit": "Hz"},
                    "spectropolarimetry_results": {
                        "fwhm_of_the_rmsf": {
                            "value": 3128.146807579617,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth_extent": {
                            "value": 22.31874920746199,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth": {
                            "value": 23136997.673582196,
                            "unit": "rad / m2",
                        },
                    },
                    "spectral_integration_time": {
                        "value": 0.00024708617999769425,
                        "unit": "s",
                    },
                    "warnings": [],
                }
            ],
            "weighting": {
                "spectral_weighting": [
                    {
                        "weighting_factor": 1.0,
                        "sbs_conv_factor": 496812.2110765234,
                        "confusion_noise": {
                            "value": 7.28513591638922e-08,
                            "limit_type": "value",
                        },
                        "beam_size": {
                            "beam_maj_scaled": 0.0005503898808793186,
                            "beam_min_scaled": 0.0005422316780292814,
                            "beam_pa": 493.43079622015034,
                        },
                        "freq_centre": {"value": 797500000.0, "unit": "Hz"},
                    }
                ]
            },
            "weighted_result": [
                {
                    "spectral_integration_time": {
                        "value": 0.00024708617999769425,
                        "unit": "s",
                    },
                    "warnings": [],
                }
            ],
        }
        assert res == expected


class TestSubarrays:
    def test_subarrays_list(self):
        """Test the subarrays entry point."""
        response = requests.get(f"{MID_API_URL}/subarrays")
        response_body = json.loads(response.content)
        # Check that the placeholder subarray entered in AT2-606 is removed when necessary
        assert {
            "name": "MID_AA4_all",
            "label": "AA4",
            "n_ska": 133,
            "n_meer": 64,
        } in response_body
        assert {
            "name": "MID_AA4_MeerKAT_only",
            "label": "AA*/AA4 (13.5-m antennas only)",
            "n_ska": 0,
            "n_meer": 64,
        } in response_body
        assert {
            "name": "MID_AA05_all",
            "label": "AA0.5",
            "n_ska": 4,
            "n_meer": 0,
        } in response_body


class TestContinuumCalculatorCases:
    def test_calculate_sensitivity_negative_input(self):
        cal_params = {
            "rx_band": "Band 1",
            "freq_centre_hz": (500 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (0.2 * u.GHz).to(u.Hz).value,
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_SKA_ONLY.value,
            "pwv": 20,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "t_gal_ska": TGAL_500MHz.value,
            "integration_time_s": -1,
        }

        combined_params = cal_params | WEIGHTING_INPUT_FOR_COMBINE
        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        res = json.loads(response.content)

        expected = {
            "type": "about:blank",
            "title": "Bad Request",
            "detail": "-1.0 is less than or equal to the minimum of 0\n\nFailed validating 'minimum' in schema:\n    {'exclusiveMinimum': True, 'minimum': 0, 'type': 'number'}\n\nOn instance:\n    -1.0",
            "status": 400,
        }
        assert expected == res

    def test_calculate_sensitivity_band1(self):
        """
        Converted from test_calculator_case -> test_calculate_sensitivity -> TESTCASE1
        """
        cal_params = {
            "rx_band": "Band 1",
            "freq_centre_hz": (0.5 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (0.2 * u.GHz).to(u.Hz).value,
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_SKA_ONLY.value,
            "pwv": 20,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "t_gal_meer": TGAL_500MHz.value,
            "t_gal_ska": TGAL_500MHz.value,
            "integration_time_s": 3600,
        }

        combined_params = cal_params | WEIGHTING_INPUT_FOR_COMBINE

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        res = json.loads(response.content)

        expected = {
            "continuum_sensitivity": {
                "value": pytest.approx(8.38114517796831e-06, abs=1e-9),
                "unit": "Jy",
            },
            "spectral_sensitivity": {"value": 0.00102239502382144, "unit": "Jy"},
            "spectropolarimetry_results": {
                "fwhm_of_the_rmsf": {"value": 11.101348958205701, "unit": "rad / m2"},
                "max_faraday_depth_extent": {
                    "value": 12.583493796615013,
                    "unit": "rad / m2",
                },
                "max_faraday_depth": {"value": 45887.22756812186, "unit": "rad / m2"},
            },
        }

        keys_to_test = [
            "continuum_sensitivity",
            "spectral_sensitivity",
            "spectropolarimetry_results",
        ]
        filtered_res = _filter_dict_by_key(
            dict_to_filter=res, keys_to_filter=keys_to_test
        )
        assert expected == filtered_res

    def test_calculate_sensitivity_band2(self):
        """
        Converted from test_calculator_case -> test_calculate_sensitivity -> TESTCASE2
        """
        cal_params = {
            "rx_band": "Band 2",
            "freq_centre_hz": (1.3 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (0.4 * u.GHz).to(u.Hz).value,
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_MEERKAT_ONLY.value,
            "pwv": 5,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "integration_time_s": 14400,
        }

        combined_params = cal_params | WEIGHTING_INPUT_FOR_COMBINE
        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        res = json.loads(response.content)

        expected = {
            "continuum_sensitivity": {"value": 7.510094417715472e-06, "unit": "Jy"},
            "spectral_sensitivity": {"value": 0.0012956143762686795, "unit": "Jy"},
            "spectropolarimetry_results": {
                "fwhm_of_the_rmsf": {"value": 100.90199982730262, "unit": "rad / m2"},
                "max_faraday_depth_extent": {
                    "value": 78.64789325771308,
                    "unit": "rad / m2",
                },
                "max_faraday_depth": {"value": 954280.3297852346, "unit": "rad / m2"},
            },
        }
        keys_to_test = [
            "continuum_sensitivity",
            "spectral_sensitivity",
            "spectropolarimetry_results",
        ]
        filtered_res = _filter_dict_by_key(
            dict_to_filter=res, keys_to_filter=keys_to_test
        )
        assert expected == filtered_res

    def test_calculate_sensitivity_band3_failed(self):
        """
        Converted from test_calculator_case -> test_calculate_sensitivity -> TESTCASE3
        """
        cal_params = {
            "rx_band": "Band 3",
            "freq_centre_hz": (2 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (0.8 * u.GHz).to(u.Hz).value,
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_ALL.value,
            "pwv": 10,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "t_gal_meer": TGAL_2GHz.value,
            "t_gal_ska": TGAL_2GHz.value,
            "integration_time_s": 3600,
        }

        combined_params = cal_params | WEIGHTING_INPUT_FOR_COMBINE
        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        assert response.status_code == 400
        res = json.loads(response.content)

        expected = {
            "title": "Validation Error",
            "detail": "Spectral window defined by central frequency and bandwidth does not lie within the band range.",
        }
        assert expected == res

    def test_calculate_sensitivity_band3(self):
        """
        Converted from test_calculator_case -> test_calculate_sensitivity -> TESTCASE3
        """
        cal_params = {
            "rx_band": "Band 3",
            "freq_centre_hz": (2 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (0.4 * u.GHz)
            .to(u.Hz)
            .value,  # originally 0.8, but this fails validation
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_ALL.value,
            "pwv": 10,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "t_gal_meer": TGAL_2GHz.value,
            "t_gal_ska": TGAL_2GHz.value,
            "integration_time_s": 3600,
        }

        combined_params = cal_params | WEIGHTING_INPUT_FOR_COMBINE
        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        res = json.loads(response.content)

        expected = {
            "continuum_sensitivity": {"value": 9.876000991811258e-07, "unit": "Jy"},
            "spectral_sensitivity": {"value": 0.00017037720371199704, "unit": "Jy"},
            "spectropolarimetry_results": {
                "fwhm_of_the_rmsf": {"value": 377.77636936157216, "unit": "rad / m2"},
                "max_faraday_depth_extent": {
                    "value": 169.18083937246973,
                    "unit": "rad / m2",
                },
                "max_faraday_depth": {"value": 4181309.70551857, "unit": "rad / m2"},
            },
        }

        keys_to_test = [
            "continuum_sensitivity",
            "spectral_sensitivity",
            "spectropolarimetry_results",
        ]
        filtered_res = _filter_dict_by_key(
            dict_to_filter=res, keys_to_filter=keys_to_test
        )

        assert expected == filtered_res

    def test_calculate_sensitivity_band4_failed(self):
        """
        Converted from test_calculator_case -> test_calculate_sensitivity -> TESTCASE4
        """
        cal_params = {
            "rx_band": "Band 4",
            "freq_centre_hz": (4 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (0.4 * u.GHz).to(u.Hz).value,
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_MEERKAT_ONLY.value,
            "pwv": 5,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "integration_time_s": 14400,
        }

        combined_params = cal_params | WEIGHTING_INPUT_FOR_COMBINE

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        assert response.status_code == 400
        res = json.loads(response.content)

        expected = {
            "title": "Validation Error",
            "detail": "Subarray configuration not allowed for given observing band.",
        }
        assert expected == res

    def test_calculate_sensitivity_band4(self):
        """
        Converted from test_calculator_case -> test_calculate_sensitivity -> TESTCASE4
        """
        cal_params = {
            "rx_band": "Band 4",
            "freq_centre_hz": (4 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (0.4 * u.GHz).to(u.Hz).value,
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_SKA_ONLY.value,
            "pwv": 5,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "integration_time_s": 14400,
        }

        combined_params = cal_params | WEIGHTING_INPUT_FOR_COMBINE

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        res = json.loads(response.content)

        expected = {
            "continuum_sensitivity": {"value": 8.294763750350039e-07, "unit": "Jy"},
            "spectral_sensitivity": {"value": 0.00014309826967494956, "unit": "Jy"},
            "spectropolarimetry_results": {
                "fwhm_of_the_rmsf": {"value": 3068.1723492742303, "unit": "rad / m2"},
                "max_faraday_depth_extent": {
                    "value": 616.6030347745191,
                    "unit": "rad / m2",
                },
                "max_faraday_depth": {"value": 39340787.672182806, "unit": "rad / m2"},
            },
        }

        keys_to_test = [
            "continuum_sensitivity",
            "spectral_sensitivity",
            "spectropolarimetry_results",
        ]
        filtered_res = _filter_dict_by_key(
            dict_to_filter=res, keys_to_filter=keys_to_test
        )

        assert expected == filtered_res

    def test_calculate_sensitivity_band5a_failed(self):
        """
        Converted from test_calculator_case -> test_calculate_sensitivity -> TESTCASE5
        """
        cal_params = {
            "rx_band": "Band 5a",
            "freq_centre_hz": (6.0 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (0.4 * u.GHz).to(u.Hz).value,
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_MEERKAT_ONLY.value,
            "pwv": 5,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "t_gal_meer": TGAL_3GHz.value,
            "t_gal_ska": TGAL_3GHz.value,
            "integration_time_s": 3600,
        }

        combined_params = cal_params | WEIGHTING_INPUT_FOR_COMBINE
        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        assert response.status_code == 400
        res = json.loads(response.content)

        expected = {
            "title": "Validation Error",
            "detail": "Subarray configuration not allowed for given observing band.",
        }
        assert expected == res

    def test_calculate_sensitivity_band5a(self):
        """
        Converted from test_calculator_case -> test_calculate_sensitivity -> TESTCASE5
        """
        cal_params = {
            "rx_band": "Band 5a",
            "freq_centre_hz": (6.0 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (0.4 * u.GHz).to(u.Hz).value,
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_SKA_ONLY.value,
            "pwv": 5,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "t_gal_meer": TGAL_3GHz.value,
            "t_gal_ska": TGAL_3GHz.value,
            "integration_time_s": 3600,
        }

        combined_params = cal_params | WEIGHTING_INPUT_FOR_COMBINE

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        res = json.loads(response.content)

        expected = {
            "continuum_sensitivity": {"value": 1.4826891329743875e-06, "unit": "Jy"},
            "spectral_sensitivity": {"value": 0.00025578817646920016, "unit": "Jy"},
            "spectropolarimetry_results": {
                "fwhm_of_the_rmsf": {"value": 10383.9360151008, "unit": "rad / m2"},
                "max_faraday_depth_extent": {
                    "value": 1343.664689545714,
                    "unit": "rad / m2",
                },
                "max_faraday_depth": {"value": 139886385.08036003, "unit": "rad / m2"},
            },
        }

        keys_to_test = [
            "continuum_sensitivity",
            "spectral_sensitivity",
            "spectropolarimetry_results",
        ]
        filtered_res = _filter_dict_by_key(
            dict_to_filter=res, keys_to_filter=keys_to_test
        )

        assert expected == filtered_res

    def test_calculate_sensitivity_band5b_failed(self):
        """
        Converted from test_calculator_case -> test_calculate_sensitivity -> TESTCASE6
        """
        cal_params = {
            "rx_band": "Band 5b",
            "freq_centre_hz": (13.0 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (1.0 * u.GHz).to(u.Hz).value,
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_ALL.value,
            "pwv": 10,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "integration_time_s": 14400,
        }

        combined_params = cal_params | WEIGHTING_INPUT_FOR_COMBINE

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        assert response.status_code == 400
        res = json.loads(response.content)

        expected = {
            "title": "Validation Error",
            "detail": "Subarray configuration not allowed for given observing band.",
        }
        assert expected == res

    def test_calculate_sensitivity_band5b(self):
        """
        Converted from test_calculator_case -> test_calculate_sensitivity -> TESTCASE6
        """
        cal_params = {
            "rx_band": "Band 5b",
            "freq_centre_hz": (13.0 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (1.0 * u.GHz).to(u.Hz).value,
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_SKA_ONLY.value,
            "pwv": 10,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "integration_time_s": 14400,
        }

        combined_params = cal_params | WEIGHTING_INPUT_FOR_COMBINE

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        res = json.loads(response.content)

        expected = {
            "continuum_sensitivity": {"value": 6.952930925459463e-07, "unit": "Jy"},
            "spectral_sensitivity": {"value": 0.0001896567396887937, "unit": "Jy"},
            "spectropolarimetry_results": {
                "fwhm_of_the_rmsf": {"value": 42215.243995423785, "unit": "rad / m2"},
                "max_faraday_depth_extent": {
                    "value": 6370.530091531987,
                    "unit": "rad / m2",
                },
                "max_faraday_depth": {"value": 1400298735.3107262, "unit": "rad / m2"},
            },
        }

        keys_to_test = [
            "continuum_sensitivity",
            "spectral_sensitivity",
            "spectropolarimetry_results",
        ]
        filtered_res = _filter_dict_by_key(
            dict_to_filter=res, keys_to_filter=keys_to_test
        )

        assert expected == filtered_res

    def test_calculate_sensitivity_band1_custom_failed(self):
        """
        Tests that the custom mode response for Mid Continuum supplied integration time returns the expected validation error.
        """
        params = {
            "rx_band": MidBand.BAND_1.value,
            "freq_centre_hz": (0.7 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_2,
            "bandwidth_hz": (0.3 * u.GHz).to(u.Hz).value,
            "pwv": 5,
            "el": 90,
            "eta_pointing": 0.5,
            "eta_coherence": 0.5,
            "eta_digitisation": 0.5,
            "eta_correlation": 0.5,
            "eta_bandpass": 0.5,
            "n_ska": 100,
            "eta_ska": 0.7,
            "t_spl_ska": 10.0,
            "t_rx_ska": 20.0,
            "n_meer": 30,
            "eta_meer": 0.8,
            "t_spl_meer": 15.0,
            "t_rx_meer": 25.0,
            "t_sky_ska": 15.0,
            "t_sky_meer": 15.0,
            "t_gal_ska": TGAL_4GHz.value,
            "t_gal_meer": TGAL_4GHz.value,
            "integration_time_s": 10,
            "weighting_mode": Weighting.UNIFORM.value,
        }
        response = requests.get(f"{MID_API_URL}/continuum/calculate", params=params)

        assert response.status_code == 400
        res = json.loads(response.content)

        expected = {
            "title": "Validation Error",
            "detail": "Spectral window defined by central frequency and bandwidth does not lie within the band range.",
        }
        assert expected == res

    def test_calculate_sensitivity_band1_custom(self):
        """
        Checks that the custom mode for Mid Continuum supplied integration time returns the expected results.
        """
        params = {
            "rx_band": MidBand.BAND_1.value,
            "freq_centre_hz": (0.7 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_2,
            "bandwidth_hz": (0.2 * u.GHz).to(u.Hz).value,
            "pwv": 5,
            "el": 90,
            "eta_pointing": 0.5,
            "eta_coherence": 0.5,
            "eta_digitisation": 0.5,
            "eta_correlation": 0.5,
            "eta_bandpass": 0.5,
            "n_ska": 100,
            "eta_ska": 0.7,
            "t_spl_ska": 10.0,
            "t_rx_ska": 20.0,
            "n_meer": 30,
            "eta_meer": 0.8,
            "t_spl_meer": 15.0,
            "t_rx_meer": 25.0,
            "t_sky_ska": 15.0,
            "t_sky_meer": 15.0,
            "t_gal_ska": TGAL_4GHz.value,
            "t_gal_meer": TGAL_4GHz.value,
            "integration_time_s": 10,
            "weighting_mode": Weighting.UNIFORM.value,
        }
        response = requests.get(f"{MID_API_URL}/continuum/calculate", params=params)

        res = json.loads(response.content)

        expected = {
            "continuum_sensitivity": {"value": 0.004007762047863952, "unit": "Jy"},
            "spectral_sensitivity": {"value": 0.4888969093588493, "unit": "Jy"},
            "spectropolarimetry_results": {
                "fwhm_of_the_rmsf": {"value": 31.717962266215736, "unit": "rad / m2"},
                "max_faraday_depth_extent": {
                    "value": 22.370780915676757,
                    "unit": "rad / m2",
                },
                "max_faraday_depth": {"value": 154866.7913434515, "unit": "rad / m2"},
            },
            "continuum_weighting": None,
            "spectral_weighting": None,
            "weighted_result": {
                "weighted_continuum_sensitivity": {
                    "value": [0.004007762047863952],
                    "unit": "Jy",
                },
                "continuum_confusion_noise": None,
                "total_continuum_sensitivity": None,
                "continuum_synthesized_beam_size": None,
                "continuum_surface_brightness_sensitivity": None,
                "weighted_spectral_sensitivity": {
                    "value": [0.4888969093588493],
                    "unit": "Jy",
                },
                "spectral_confusion_noise": None,
                "total_spectral_sensitivity": None,
                "spectral_synthesized_beam_size": None,
                "spectral_surface_brightness_sensitivity": None,
                "continuum_integration_time": None,
                "spectral_integration_time": None,
                "confusion_noise_per_subband": None,
                "synthesized_beam_size_per_subband": None,
                "integration_time_per_subband": None,
                "warnings": [],
            },
        }
        assert expected == res

    @pytest.mark.skip(reason="refactor test after zoom/custom mode is reworked")
    def test_calculate_sensitivity_band1_zoom(self):
        """
        Converted from test_calculator_case -> test_calculate_sensitivity -> TESTCASE8
        """
        params = {
            "rx_band": "Band 1",
            "freq_centres_hz": (0.7 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_2,
            "bandwidth_hz": (0.21 * u.kHz).to(u.Hz).value,
            "pwv": 5,
            "el": 90,
            "eta_pointing": 0.5,
            "eta_coherence": 0.5,
            "eta_digitisation": 0.5,
            "eta_correlation": 0.5,
            "eta_bandpass": 0.5,
            "n_ska": 100,
            "eta_ska": 0.7,
            "t_spl_ska": 10.0,
            "t_rx_ska": 20.0,
            "n_meer": 30,
            "eta_meer": 0.8,
            "t_spl_meer": 15.0,
            "t_rx_meer": 25.0,
            "t_sky_ska": 15.0,
            "t_sky_meer": 15.0,
            "t_gal_ska": TGAL_4GHz.value,
            "t_gal_meer": TGAL_4GHz.value,
            "integration_time_s": 10,
            "spectral_resolutions_hz": 210,
            "total_bandwidths_hz": 3125000,
        }
        response = requests.get(f"{MID_API_URL}/zoom/calculate", params=params)

        res = json.loads(response.content)

        expected = [
            {
                "freq_centre": {"value": 700000000.0, "unit": "Hz"},
                "spectral_sensitivity": {"value": 3.911175274870794, "unit": "Jy"},
                "spectropolarimetry_results": {
                    "fwhm_of_the_rmsf": {
                        "value": 2115.3789494825605,
                        "unit": "rad / m2",
                    },
                    "max_faraday_depth_extent": {
                        "value": 17.204460990377626,
                        "unit": "rad / m2",
                    },
                    "max_faraday_depth": {
                        "value": 15633376.041209446,
                        "unit": "rad / m2",
                    },
                },
            }
        ]
        assert expected == res

    def test_calculate_integration_time_band1(self):
        """
        Converted from test_calculator_case -> calculate_integration_time -> TESTCASE1
        """
        cal = {
            "rx_band": "Band 1",
            "freq_centre_hz": (0.5 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (0.2 * u.GHz).to(u.Hz).value,
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_SKA_ONLY.value,
            "pwv": 20,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "t_gal_meer": TGAL_500MHz.value,
            "t_gal_ska": TGAL_500MHz.value,
            "supplied_sensitivity": (1 * u.uJy).to(u.Jy).value,
            "sensitivity_unit": "Jy/beam",
        }

        combined_params = cal | WEIGHTING_INPUT_FOR_COMBINE

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        res = json.loads(response.content)

        expected = {
            "continuum_integration_time": {"value": 23697228.23557253, "unit": "s"},
            "spectral_integration_time": {"value": 97381767738.2413, "unit": "s"},
            "spectropolarimetry_results": {
                "fwhm_of_the_rmsf": {"value": 11.101348958205701, "unit": "rad / m2"},
                "max_faraday_depth_extent": {
                    "value": 12.583493796615013,
                    "unit": "rad / m2",
                },
                "max_faraday_depth": {"value": 45887.22756812186, "unit": "rad / m2"},
            },
            "continuum_weighting": {
                "weighting_factor": 9.680419141845972,
                "sbs_conv_factor": 20260176.64497496,
                "confusion_noise": {"value": 0, "limit_type": "value"},
                "beam_size": {
                    "beam_maj_scaled": 0.0001388998173138762,
                    "beam_min_scaled": 0.0001340368031785243,
                    "beam_pa": 290.434472103542,
                },
                "subbands": [],
            },
            "spectral_weighting": {
                "weighting_factor": 5.087083304089957,
                "sbs_conv_factor": 7325063.04873158,
                "confusion_noise": {"value": 0, "limit_type": "value"},
                "beam_size": {
                    "beam_maj_scaled": 0.000230223972709511,
                    "beam_min_scaled": 0.00022366964705608763,
                    "beam_pa": 458.932764373367,
                },
                "subbands": [],
            },
            "weighted_result": {
                "weighted_continuum_sensitivity": None,
                "continuum_confusion_noise": {"value": 0.0, "unit": "Jy"},
                "total_continuum_sensitivity": None,
                "continuum_synthesized_beam_size": {
                    "beam_maj": {"value": 0.5000393423299543, "unit": "arcsec"},
                    "beam_min": {"value": 0.48253249144268745, "unit": "arcsec"},
                },
                "continuum_surface_brightness_sensitivity": None,
                "weighted_spectral_sensitivity": None,
                "spectral_confusion_noise": {"value": 0.0, "unit": "Jy"},
                "total_spectral_sensitivity": None,
                "spectral_synthesized_beam_size": {
                    "beam_maj": {"value": 0.8288063017542396, "unit": "arcsec"},
                    "beam_min": {"value": 0.8052107294019154, "unit": "arcsec"},
                },
                "spectral_surface_brightness_sensitivity": None,
                "continuum_integration_time": {"value": 23697228.23557253, "unit": "s"},
                "spectral_integration_time": {"value": 97381767738.2413, "unit": "s"},
                "confusion_noise_per_subband": None,
                "synthesized_beam_size_per_subband": None,
                "integration_time_per_subband": None,
                "warnings": [],
            },
        }

        assert expected == res

    def test_calculate_integration_time_band2(self):
        """
        Converted from test_calculator_case -> test_calculate_integration_time -> TESTCASE2
        """
        params = {
            "rx_band": "Band 2",
            "freq_centre_hz": (1.3 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (0.4 * u.GHz).to(u.Hz).value,
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_MEERKAT_ONLY.value,
            "pwv": 5,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "supplied_sensitivity": (10 * u.uJy).to(u.Jy).value,
            "sensitivity_unit": "Jy/beam",
        }

        combined_params = params | WEIGHTING_INPUT_FOR_COMBINE

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        res = json.loads(response.content)

        expected = {
            "continuum_integration_time": {"value": 193122.88431925222, "unit": "s"},
            "spectral_integration_time": {"value": 1492362555.8147779, "unit": "s"},
            "spectropolarimetry_results": {
                "fwhm_of_the_rmsf": {"value": 100.90199982730262, "unit": "rad / m2"},
                "max_faraday_depth_extent": {
                    "value": 78.64789325771308,
                    "unit": "rad / m2",
                },
                "max_faraday_depth": {"value": 954280.3297852346, "unit": "rad / m2"},
            },
            "continuum_weighting": {
                "weighting_factor": 4.869430957713318,
                "sbs_conv_factor": 41981.48439779324,
                "confusion_noise": {
                    "value": 5.305148863794852e-07,
                    "limit_type": "value",
                },
                "beam_size": {
                    "beam_maj_scaled": 0.0013335319237084335,
                    "beam_min_scaled": 0.000996692201308555,
                    "beam_pa": -20.29233988400413,
                },
                "subbands": [],
            },
            "spectral_weighting": {
                "weighting_factor": 2.442757079512349,
                "sbs_conv_factor": 15802.562776306468,
                "confusion_noise": {
                    "value": 1.8303997466078673e-06,
                    "limit_type": "value",
                },
                "beam_size": {
                    "beam_maj_scaled": 0.002135878747382566,
                    "beam_min_scaled": 0.0016531724102286246,
                    "beam_pa": -12.973394312111632,
                },
                "subbands": [],
            },
            "weighted_result": {
                "weighted_continuum_sensitivity": None,
                "continuum_confusion_noise": {
                    "value": 5.305148863794852e-07,
                    "unit": "Jy",
                },
                "total_continuum_sensitivity": None,
                "continuum_synthesized_beam_size": {
                    "beam_maj": {"value": 4.800714925350361, "unit": "arcsec"},
                    "beam_min": {"value": 3.5880919247107985, "unit": "arcsec"},
                },
                "continuum_surface_brightness_sensitivity": None,
                "weighted_spectral_sensitivity": None,
                "spectral_confusion_noise": {
                    "value": 1.8303997466078673e-06,
                    "unit": "Jy",
                },
                "total_spectral_sensitivity": None,
                "spectral_synthesized_beam_size": {
                    "beam_maj": {"value": 7.689163490577237, "unit": "arcsec"},
                    "beam_min": {"value": 5.951420676823049, "unit": "arcsec"},
                },
                "spectral_surface_brightness_sensitivity": None,
                "continuum_integration_time": {
                    "value": 193122.88431925222,
                    "unit": "s",
                },
                "spectral_integration_time": {"value": 1492362555.8147779, "unit": "s"},
                "confusion_noise_per_subband": None,
                "synthesized_beam_size_per_subband": None,
                "integration_time_per_subband": None,
                "warnings": [],
            },
        }

        assert expected == res

    def test_calculate_integration_time_band3_failed(self):
        """
        Converted from test_calculator_case -> test_calculate_integration_time -> TESTCASE3
        """
        params = {
            "rx_band": "Band 3",
            "freq_centre_hz": (2 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (0.8 * u.GHz).to(u.Hz).value,
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_ALL.value,
            "pwv": 10,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "t_gal_meer": TGAL_2GHz.value,
            "t_gal_ska": TGAL_2GHz.value,
            "supplied_sensitivity": (1 * u.uJy).to(u.Jy).value,
            "sensitivity_unit": "Jy/beam",
        }

        combined_params = params | WEIGHTING_INPUT_FOR_COMBINE

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        assert response.status_code == 400
        res = json.loads(response.content)

        expected = {
            "title": "Validation Error",
            "detail": "Spectral window defined by central frequency and bandwidth does not lie within the band range.",
        }
        assert expected == res

    def test_calculate_integration_time_band3(self):
        """
        Converted from test_calculator_case -> test_calculate_integration_time -> TESTCASE3
        """
        params = {
            "rx_band": "Band 3",
            "freq_centre_hz": (2 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (0.4 * u.GHz).to(u.Hz).value,
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_ALL.value,
            "pwv": 10,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "t_gal_meer": TGAL_2GHz.value,
            "t_gal_ska": TGAL_2GHz.value,
            "supplied_sensitivity": (1 * u.uJy).to(u.Jy).value,
            "sensitivity_unit": "Jy/beam",
        }

        combined_params = params | WEIGHTING_INPUT_FOR_COMBINE

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate", params=combined_params
        )

        res = json.loads(response.content)

        expected = {
            "continuum_integration_time": {"value": 636378.3157669718, "unit": "s"},
            "spectral_integration_time": {"value": 4938763808.322036, "unit": "s"},
            "spectropolarimetry_results": {
                "fwhm_of_the_rmsf": {"value": 377.77636936157216, "unit": "rad / m2"},
                "max_faraday_depth_extent": {
                    "value": 169.18083937246973,
                    "unit": "rad / m2",
                },
                "max_faraday_depth": {"value": 4181309.70551857, "unit": "rad / m2"},
            },
            "continuum_weighting": {
                "weighting_factor": 13.4624874418437,
                "sbs_conv_factor": 20260008.911300637,
                "confusion_noise": {"value": 0, "limit_type": "value"},
                "beam_size": {
                    "beam_maj_scaled": 3.472511229797872e-05,
                    "beam_min_scaled": 3.350932577964241e-05,
                    "beam_pa": 290.4338280746219,
                },
                "subbands": [],
            },
            "spectral_weighting": {
                "weighting_factor": 6.874583508702582,
                "sbs_conv_factor": 7197174.102241507,
                "confusion_noise": {"value": 0, "limit_type": "value"},
                "beam_size": {
                    "beam_maj_scaled": 5.796339325867067e-05,
                    "beam_min_scaled": 5.651102314553003e-05,
                    "beam_pa": 116.78285101018233,
                },
                "subbands": [],
            },
            "weighted_result": {
                "weighted_continuum_sensitivity": None,
                "continuum_confusion_noise": {"value": 0.0, "unit": "Jy"},
                "total_continuum_sensitivity": None,
                "continuum_synthesized_beam_size": {
                    "beam_maj": {"value": 0.1250104042727234, "unit": "arcsec"},
                    "beam_min": {"value": 0.12063357280671269, "unit": "arcsec"},
                },
                "continuum_surface_brightness_sensitivity": None,
                "weighted_spectral_sensitivity": None,
                "spectral_confusion_noise": {"value": 0.0, "unit": "Jy"},
                "total_spectral_sensitivity": None,
                "spectral_synthesized_beam_size": {
                    "beam_maj": {"value": 0.2086682157312144, "unit": "arcsec"},
                    "beam_min": {"value": 0.2034396833239081, "unit": "arcsec"},
                },
                "spectral_surface_brightness_sensitivity": None,
                "continuum_integration_time": {"value": 636378.3157669718, "unit": "s"},
                "spectral_integration_time": {"value": 4938763808.322036, "unit": "s"},
                "confusion_noise_per_subband": None,
                "synthesized_beam_size_per_subband": None,
                "integration_time_per_subband": None,
                "warnings": [],
            },
        }

        assert expected == res

    def test_calculate_integration_time_band4_failed(self):
        """
        Converted from test_calculator_case -> test_calculate_integration_time -> TESTCASE4
        """
        params = {
            "rx_band": "Band 4",
            "freq_centre_hz": (4 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (0.4 * u.GHz).to(u.Hz).value,
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_MEERKAT_ONLY.value,
            "pwv": 5,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "supplied_sensitivity": (0.1 * u.uJy).to(u.Jy).value,
            "sensitivity_unit": "Jy/beam",
        }

        combined_params = params | WEIGHTING_INPUT_FOR_COMBINE

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        assert response.status_code == 400
        res = json.loads(response.content)

        expected = {
            "title": "Validation Error",
            "detail": "Subarray configuration not allowed for given observing band.",
        }
        assert expected == res

    def test_calculate_integration_time_band4(self):
        """
        Converted from test_calculator_case -> test_calculate_integration_time -> TESTCASE4
        """
        params = {
            "rx_band": "Band 4",
            "freq_centre_hz": (4 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (0.4 * u.GHz).to(u.Hz).value,
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_SKA_ONLY.value,
            "pwv": 5,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "supplied_sensitivity": (0.1 * u.uJy).to(u.Jy).value,
            "sensitivity_unit": "Jy/beam",
        }

        combined_params = params | WEIGHTING_INPUT_FOR_COMBINE

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate", params=combined_params
        )

        res = json.loads(response.content)

        expected = {
            "continuum_integration_time": {"value": 92845072.07904439, "unit": "s"},
            "spectral_integration_time": {"value": 763078040601.345, "unit": "s"},
            "spectropolarimetry_results": {
                "fwhm_of_the_rmsf": {"value": 3068.1723492742303, "unit": "rad / m2"},
                "max_faraday_depth_extent": {
                    "value": 616.6030347745191,
                    "unit": "rad / m2",
                },
                "max_faraday_depth": {"value": 39340787.672182806, "unit": "rad / m2"},
            },
            "continuum_weighting": {
                "weighting_factor": 9.680419141845972,
                "sbs_conv_factor": 20260176.64497496,
                "confusion_noise": {"value": 0, "limit_type": "value"},
                "beam_size": {
                    "beam_maj_scaled": 1.7362477164234524e-05,
                    "beam_min_scaled": 1.6754600397315536e-05,
                    "beam_pa": 290.434472103542,
                },
                "subbands": [],
            },
            "spectral_weighting": {
                "weighting_factor": 5.087083304089957,
                "sbs_conv_factor": 7325063.04873158,
                "confusion_noise": {"value": 0, "limit_type": "value"},
                "beam_size": {
                    "beam_maj_scaled": 2.8777996588688874e-05,
                    "beam_min_scaled": 2.7958705882010953e-05,
                    "beam_pa": 458.932764373367,
                },
                "subbands": [],
            },
            "weighted_result": {
                "weighted_continuum_sensitivity": None,
                "continuum_confusion_noise": {"value": 0.0, "unit": "Jy"},
                "total_continuum_sensitivity": None,
                "continuum_synthesized_beam_size": {
                    "beam_maj": {"value": 0.06250491779124429, "unit": "arcsec"},
                    "beam_min": {"value": 0.06031656143033593, "unit": "arcsec"},
                },
                "continuum_surface_brightness_sensitivity": None,
                "weighted_spectral_sensitivity": None,
                "spectral_confusion_noise": {"value": 0.0, "unit": "Jy"},
                "total_spectral_sensitivity": None,
                "spectral_synthesized_beam_size": {
                    "beam_maj": {"value": 0.10360078771927995, "unit": "arcsec"},
                    "beam_min": {"value": 0.10065134117523943, "unit": "arcsec"},
                },
                "spectral_surface_brightness_sensitivity": None,
                "continuum_integration_time": {"value": 92845072.07904439, "unit": "s"},
                "spectral_integration_time": {"value": 763078040601.345, "unit": "s"},
                "confusion_noise_per_subband": None,
                "synthesized_beam_size_per_subband": None,
                "integration_time_per_subband": None,
                "warnings": [],
            },
        }

        assert expected == res

    def test_calculate_integration_time_band5a_failed(self):
        """
        Converted from test_calculator_case -> test_calculate_integration_time -> TESTCASE5
        """
        params = {
            "rx_band": "Band 5a",
            "freq_centre_hz": (6.0 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (0.4 * u.GHz).to(u.Hz).value,
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_MEERKAT_ONLY.value,
            "pwv": 5,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "t_gal_meer": TGAL_3GHz.value,
            "t_gal_ska": TGAL_3GHz.value,
            "supplied_sensitivity": (0.1 * u.uJy).to(u.Jy).value,
            "sensitivity_unit": "Jy/beam",
        }

        combined_params = params | WEIGHTING_INPUT_FOR_COMBINE

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        assert response.status_code == 400
        res = json.loads(response.content)

        expected = {
            "title": "Validation Error",
            "detail": "Subarray configuration not allowed for given observing band.",
        }
        assert expected == res

    def test_calculate_integration_time_band5a(self):
        """
        Converted from test_calculator_case -> test_calculate_integration_time -> TESTCASE5
        """
        params = {
            "rx_band": "Band 5a",
            "freq_centre_hz": (6.0 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (0.4 * u.GHz).to(u.Hz).value,
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_SKA_ONLY.value,
            "pwv": 5,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "t_gal_meer": TGAL_3GHz.value,
            "t_gal_ska": TGAL_3GHz.value,
            "supplied_sensitivity": (1 * u.uJy).to(u.Jy).value,
            "sensitivity_unit": "Jy/beam",
        }
        combined_params = params | WEIGHTING_INPUT_FOR_COMBINE

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate", params=combined_params
        )

        res = json.loads(response.content)

        expected = {
            "continuum_integration_time": {"value": 741636.3934812859, "unit": "s"},
            "spectral_integration_time": {"value": 6095384852.461979, "unit": "s"},
            "spectropolarimetry_results": {
                "fwhm_of_the_rmsf": {"value": 10383.9360151008, "unit": "rad / m2"},
                "max_faraday_depth_extent": {
                    "value": 1343.664689545714,
                    "unit": "rad / m2",
                },
                "max_faraday_depth": {"value": 139886385.08036003, "unit": "rad / m2"},
            },
            "continuum_weighting": {
                "weighting_factor": 9.680419141845972,
                "sbs_conv_factor": 20260176.644974954,
                "confusion_noise": {"value": 0, "limit_type": "value"},
                "beam_size": {
                    "beam_maj_scaled": 1.1574984776156349e-05,
                    "beam_min_scaled": 1.1169733598210357e-05,
                    "beam_pa": 290.434472103542,
                },
                "subbands": [],
            },
            "spectral_weighting": {
                "weighting_factor": 5.087083304089957,
                "sbs_conv_factor": 7325063.048731581,
                "confusion_noise": {"value": 0, "limit_type": "value"},
                "beam_size": {
                    "beam_maj_scaled": 1.9185331059125917e-05,
                    "beam_min_scaled": 1.8639137254673968e-05,
                    "beam_pa": 458.932764373367,
                },
                "subbands": [],
            },
            "weighted_result": {
                "weighted_continuum_sensitivity": None,
                "continuum_confusion_noise": {"value": 0.0, "unit": "Jy"},
                "total_continuum_sensitivity": None,
                "continuum_synthesized_beam_size": {
                    "beam_maj": {"value": 0.04166994519416285, "unit": "arcsec"},
                    "beam_min": {"value": 0.040211040953557285, "unit": "arcsec"},
                },
                "continuum_surface_brightness_sensitivity": None,
                "weighted_spectral_sensitivity": None,
                "spectral_confusion_noise": {"value": 0.0, "unit": "Jy"},
                "total_spectral_sensitivity": None,
                "spectral_synthesized_beam_size": {
                    "beam_maj": {"value": 0.0690671918128533, "unit": "arcsec"},
                    "beam_min": {"value": 0.06710089411682628, "unit": "arcsec"},
                },
                "spectral_surface_brightness_sensitivity": None,
                "continuum_integration_time": {"value": 741636.3934812859, "unit": "s"},
                "spectral_integration_time": {"value": 6095384852.461979, "unit": "s"},
                "confusion_noise_per_subband": None,
                "synthesized_beam_size_per_subband": None,
                "integration_time_per_subband": None,
                "warnings": [],
            },
        }

        assert expected == res

    def test_calculate_integration_time_band5b_failed(self):
        """
        Converted from test_calculator_case -> test_calculate_integration_time -> TESTCASE6
        """
        params = {
            "rx_band": "Band 5b",
            "freq_centre_hz": (13.0 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (1.0 * u.GHz).to(u.Hz).value,
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_ALL.value,
            "pwv": 10,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "supplied_sensitivity": (0.1 * u.uJy).to(u.Jy).value,
            "sensitivity_unit": "Jy/beam",
        }

        combined_params = params | WEIGHTING_INPUT_FOR_COMBINE

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        assert response.status_code == 400
        res = json.loads(response.content)

        expected = {
            "title": "Validation Error",
            "detail": "Subarray configuration not allowed for given observing band.",
        }
        assert expected == res

    def test_calculate_integration_time_band5b(self):
        """
        Converted from test_calculator_case -> test_calculate_integration_time -> TESTCASE6
        """
        params = {
            "rx_band": "Band 5b",
            "freq_centre_hz": (13.0 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (1.0 * u.GHz).to(u.Hz).value,
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_SKA_ONLY.value,
            "pwv": 10,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "supplied_sensitivity": (0.1 * u.uJy).to(u.Jy).value,
            "sensitivity_unit": "Jy/beam",
        }

        combined_params = params | WEIGHTING_INPUT_FOR_COMBINE

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate", params=combined_params
        )

        res = json.loads(response.content)

        expected = {
            "continuum_integration_time": {"value": 65235898.04979646, "unit": "s"},
            "spectral_integration_time": {"value": 1340407200565.4517, "unit": "s"},
            "spectropolarimetry_results": {
                "fwhm_of_the_rmsf": {"value": 42215.243995423785, "unit": "rad / m2"},
                "max_faraday_depth_extent": {
                    "value": 6370.530091531987,
                    "unit": "rad / m2",
                },
                "max_faraday_depth": {"value": 1400298735.3107262, "unit": "rad / m2"},
            },
            "continuum_weighting": {
                "weighting_factor": 9.680419141845972,
                "sbs_conv_factor": 20260176.644974954,
                "confusion_noise": {"value": 0, "limit_type": "value"},
                "beam_size": {
                    "beam_maj_scaled": 5.342300665918315e-06,
                    "beam_min_scaled": 5.155261660712472e-06,
                    "beam_pa": 290.434472103542,
                },
                "subbands": [],
            },
            "spectral_weighting": {
                "weighting_factor": 5.087083304089957,
                "sbs_conv_factor": 7325063.04873158,
                "confusion_noise": {"value": 0, "limit_type": "value"},
                "beam_size": {
                    "beam_maj_scaled": 8.854768181135039e-06,
                    "beam_min_scaled": 8.602678732926447e-06,
                    "beam_pa": 458.932764373367,
                },
                "subbands": [],
            },
            "weighted_result": {
                "weighted_continuum_sensitivity": None,
                "continuum_confusion_noise": {"value": 0.0, "unit": "Jy"},
                "total_continuum_sensitivity": None,
                "continuum_synthesized_beam_size": {
                    "beam_maj": {"value": 0.019232282397305935, "unit": "arcsec"},
                    "beam_min": {"value": 0.0185589419785649, "unit": "arcsec"},
                },
                "continuum_surface_brightness_sensitivity": None,
                "weighted_spectral_sensitivity": None,
                "spectral_confusion_noise": {"value": 0.0, "unit": "Jy"},
                "total_spectral_sensitivity": None,
                "spectral_synthesized_beam_size": {
                    "beam_maj": {"value": 0.03187716545208614, "unit": "arcsec"},
                    "beam_min": {"value": 0.03096964343853521, "unit": "arcsec"},
                },
                "spectral_surface_brightness_sensitivity": None,
                "continuum_integration_time": {"value": 65235898.04979646, "unit": "s"},
                "spectral_integration_time": {"value": 1340407200565.4517, "unit": "s"},
                "confusion_noise_per_subband": None,
                "synthesized_beam_size_per_subband": None,
                "integration_time_per_subband": None,
                "warnings": [],
            },
        }

        assert expected == res

    def test_calculate_integration_time_band1_negative_input(self):
        params = {
            "rx_band": "Band 1",
            "freq_centre_hz": (500 * u.GHz).to(u.Hz).value,
            "pointing_centre": POINTING_CENTRE_1,
            "bandwidth_hz": (0.2 * u.GHz).to(u.Hz).value,
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_SKA_ONLY.value,
            "pwv": 20,
            "el": 90,
            "eta_system": ETA_SYSTEM,
            "t_gal_meer": TGAL_500MHz.value,
            "t_gal_ska": TGAL_500MHz.value,
            "supplied_sensitivity": (-1.0 * u.uJy).to(u.Jy).value,
            "sensitivity_unit": "Jy/beam",
        }

        combined_params = params | WEIGHTING_INPUT_FOR_COMBINE

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        res = json.loads(response.content)

        expected = {
            "type": "about:blank",
            "title": "Bad Request",
            "detail": "-1e-06 is less than or equal to the minimum of 0\n\nFailed validating 'minimum' in schema:\n    {'example': 0.001,\n     'exclusiveMinimum': True,\n     'minimum': 0,\n     'type': 'number'}\n\nOn instance:\n    -1e-06",
            "status": 400,
        }
        assert expected == res


class TestContinuum:  # TODO: there is no subarray_configuration but after combined it is required
    @pytest.mark.skip(
        reason="refactor test after getting required subarray_configuration after combined"
    )
    def test_calculate_returns_sensitivity(self):
        """
        Test /continuum/calculate returns the correct sensitivities when given an integration time.
        """
        cal = CONTINUUM_CALCULATE_PARAMS | {"integration_time_s": 1000}

        combined_params = cal | WEIGHTING_INPUT_FOR_COMBINE
        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        response_body = json.loads(response.content)

        assert response.status_code == HTTPStatus.OK
        assert numpy.isclose(
            response_body["continuum_sensitivity"]["value"], 7.5415356594694794e-06
        )
        assert response_body["continuum_sensitivity"]["unit"] == "Jy"
        assert numpy.isclose(
            response_body["spectral_sensitivity"]["value"],
            0.0013010385057880293,
        )
        assert response_body["spectral_sensitivity"]["unit"] == "Jy"

    @pytest.mark.skip(
        reason="supplied sensitivity is updated but skipped for now since we added sensitivity_unit for supplied_sensitivity but not able to rework the param"
    )
    def test_calculate_returns_integration_time(self):
        """
        Test /continuum/calculate returns the correct integration time when given a sensitivity
        """
        params = CONTINUUM_CALCULATE_PARAMS | {"supplied_sensitivity": 1.5}
        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=params,
        )

        response_body = json.loads(response.content)

        assert response.status_code == HTTPStatus.OK
        assert numpy.isclose(
            response_body["continuum_integration_time"]["value"], 2.5277671156911018e-08
        )
        assert response_body["continuum_integration_time"]["unit"] == "s"
        assert numpy.isclose(
            response_body["spectral_integration_time"]["value"], 0.0007523116415747327
        )
        assert response_body["spectral_integration_time"]["unit"] == "s"

    @pytest.mark.skip(
        reason="refactor test after getting required subarray_configuration after combined"
    )
    def test_calculate_returns_subband_sensitivities(self):
        """
        Test /continuum/calculate returns the correct subband sensitivities when given an integration times and n_subbands
        """
        cal = CONTINUUM_CALCULATE_PARAMS | {
            "integration_time_s": 100,
            "n_subbands": 2,
        }

        combined_params = cal | WEIGHTING_INPUT_FOR_COMBINE

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        response_body = json.loads(response.content)

        assert response.status_code == HTTPStatus.OK
        sensitivies = [
            subband_result["sensitivity"]
            for subband_result in response_body["continuum_subband_sensitivities"]
        ]
        assert sensitivies == [
            {"value": 4.086472201334516e-05, "unit": "Jy"},
            {"value": pytest.approx(2.916167550695785e-05), "unit": "Jy"},
        ]

    @pytest.mark.skip(
        reason="supplied sensitivity is updated but skipped for now since we added sensitivity_unit for supplied_sensitivity but not able to rework the param"
    )
    def test_calculate_returns_subband_integration_times(self):
        """
        Test /continuum/calculate returns the correct subband integration times when given a sensitivity and n_subbands and subband_sensitivities
        """
        params = CONTINUUM_CALCULATE_PARAMS | {
            "supplied_sensitivity": 1.5,
            "sensitivity_unit": "Jy/beam",
            "n_subbands": 2,
            "subband_supplied_sensitivities": [1, 2],
            "subband_supplied_sensitivities_unit": "Jy/beam",
        }

        combined_params = params | WEIGHTING_INPUT_FOR_COMBINE

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        response_body = json.loads(response.content)

        assert response.status_code == HTTPStatus.OK
        integration_times = [
            subband_result["integration_time"]
            for subband_result in response_body["continuum_subband_integration_times"]
        ]
        assert numpy.allclose(
            [integration_times[0]["value"], integration_times[1]["value"]],
            [1.6699255052279768e-07, 2.1260082959327638e-08],
        )
        assert integration_times[0]["unit"] == integration_times[1]["unit"] == "s"

    @pytest.mark.skip(
        reason="refactor test after getting required subarray_configuration after combined"
    )
    def test_calculate_spectropolarimetry_values(self):
        cal = CONTINUUM_CALCULATE_PARAMS | {"integration_time_s": 1000}

        combined_params = cal | WEIGHTING_INPUT_FOR_COMBINE
        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        response_body = json.loads(response.content)

        assert response.status_code == HTTPStatus.OK

        assert response_body["spectropolarimetry_results"] == {
            "fwhm_of_the_rmsf": {"unit": "rad / m2", "value": 20.807750946498167},
            "max_faraday_depth": {"unit": "rad / m2", "value": 147251.87431630664},
            "max_faraday_depth_extent": {
                "unit": "rad / m2",
                "value": 34.25886417119213,
            },
        }

    def test_calculate_exception(self):
        """
        Test /calculate returns an error response as expected
        """
        error_params = CONTINUUM_CALCULATE_PARAMS | {
            "supplied_sensitivity": 1.5,
            "n_ska": 230,
        }  # Should not be more than 133

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate", params=error_params
        )
        response_body = json.loads(response.content)

        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert "230 is greater than the maximum of 133" in response_body["detail"]

    def test_mid_calculate_continuum_sensitivies_custom_array_results(self):
        """
        Test /continuum/calculate supplied integration time custom subarray returns the correct results
        """
        params = {
            "integration_time_s": 1,
            "rx_band": MidBand.BAND_1.value,
            "n_ska": 45,
            "n_meer": 0,
            "freq_centre_hz": 797500000,
            "bandwidth_hz": 435000000,
            "pointing_centre": "00:00:00.0 00:00:00.0",
            "pwv": DEFAULT_PWV,
            "el": DEFAULT_EL.value,
            "n_subbands": 1,
            "weighting_mode": Weighting.UNIFORM.value,
            "alpha": DEFAULT_ALPHA,
        }

        expected = {
            "continuum_sensitivity": {"value": 0.00046242226891483895, "unit": "Jy"},
            "spectral_sensitivity": {"value": 0.0831924155254681, "unit": "Jy"},
            "spectropolarimetry_results": {
                "fwhm_of_the_rmsf": {"value": 19.25329547027156, "unit": "rad / m2"},
                "max_faraday_depth_extent": {
                    "value": 36.010968413944646,
                    "unit": "rad / m2",
                },
                "max_faraday_depth": {"value": 139890.7611157587, "unit": "rad / m2"},
            },
            "continuum_weighting": None,
            "spectral_weighting": None,
            "weighted_result": {
                "weighted_continuum_sensitivity": {
                    "value": [0.00046242226891483895],
                    "unit": "Jy",
                },
                "continuum_confusion_noise": None,
                "total_continuum_sensitivity": None,
                "continuum_synthesized_beam_size": None,
                "continuum_surface_brightness_sensitivity": None,
                "weighted_spectral_sensitivity": {
                    "value": [0.0831924155254681],
                    "unit": "Jy",
                },
                "spectral_confusion_noise": None,
                "total_spectral_sensitivity": None,
                "spectral_synthesized_beam_size": None,
                "spectral_surface_brightness_sensitivity": None,
                "continuum_integration_time": None,
                "spectral_integration_time": None,
                "confusion_noise_per_subband": None,
                "synthesized_beam_size_per_subband": None,
                "integration_time_per_subband": None,
                "warnings": [],
            },
        }

        response = requests.get(f"{MID_API_URL}/continuum/calculate", params=params)
        result = json.loads(response.content)

        assert response.status_code == HTTPStatus.OK, response.content
        assert expected == result

    def test_mid_calculate_continuum_integration_time_custom_array_results(self):
        """
        Test /continuum/calculate supplied sensitivity custom subarray returns the correct results
        """
        params = {
            "supplied_sensitivity": 0.001,
            "rx_band": MidBand.BAND_1.value,
            "n_ska": 45,
            "n_meer": 0,
            "freq_centre_hz": 797500000,
            "bandwidth_hz": 435000000,
            "pointing_centre": "00:00:00.0 00:00:00.0",
            "pwv": DEFAULT_PWV,
            "el": DEFAULT_EL.value,
            "n_subbands": 1,
            "weighting_mode": Weighting.UNIFORM.value,
            "alpha": DEFAULT_ALPHA,
        }

        expected = {
            "continuum_integration_time": {"value": 0.21383435478834772, "unit": "s"},
            "spectral_integration_time": {"value": 6920.978000962147, "unit": "s"},
            "spectropolarimetry_results": {
                "fwhm_of_the_rmsf": {"value": 19.25329547027156, "unit": "rad / m2"},
                "max_faraday_depth_extent": {
                    "value": 36.010968413944646,
                    "unit": "rad / m2",
                },
                "max_faraday_depth": {"value": 139890.7611157587, "unit": "rad / m2"},
            },
            "continuum_weighting": None,
            "spectral_weighting": None,
            "weighted_result": {
                "continuum_confusion_noise": None,
                "continuum_synthesized_beam_size": None,
                "continuum_integration_time": {
                    "value": [0.21383435478834772],
                    "unit": "s",
                },
                "spectral_confusion_noise": None,
                "spectral_synthesized_beam_size": None,
                "spectral_integration_time": {
                    "value": [6920.978000962147],
                    "unit": "s",
                },
                "warnings": [],
            },
        }

        response = requests.get(f"{MID_API_URL}/continuum/calculate", params=params)
        result = json.loads(response.content)

        assert response.status_code == HTTPStatus.OK, response.content
        assert expected == result


class TestWeighting:  # will use new calculate endpoint
    def test_weighting_returns_expected_value(self):
        """
        Test /calculate returns the correct response
        """
        combined_params = {
            **WEIGHTING_INPUT_PARAMS,
            **(CONTINUUM_CALCULATE_INPUT_FOR_COMBINE | {"integration_time_s": 1}),
        }

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        response_body = json.loads(response.content)

        assert response.status_code == HTTPStatus.OK
        assert response_body["spectral_weighting"] == {
            "weighting_factor": 3.574555564224668,
            "sbs_conv_factor": 24523.66058493909,
            "subbands": [],
            "confusion_noise": {
                "value": 4.745589385453077e-06,
                "limit_type": "value",
            },
            "beam_size": {
                "beam_maj_scaled": 0.0025466146922955555,
                "beam_min_scaled": 0.002419392067356811,
                "beam_pa": -402.9659297877625,
            },
        }

    def test_weighting_returns_subband_values(self):
        """
        Test /calculate weighting returns the correct subband weightings
        """
        combined_params = {
            **(
                WEIGHTING_INPUT_PARAMS
                | {"subband_freq_centres_hz": [1e6, 2e6], "n_subbands": 2}
            ),
            **(CONTINUUM_CALCULATE_INPUT_FOR_COMBINE | {"integration_time_s": 1}),
        }
        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_params,
        )

        response_body = json.loads(response.content)

        assert response.status_code == HTTPStatus.OK
        assert response_body["spectral_weighting"]["subbands"] == [
            {
                "beam_size": {
                    "beam_maj_scaled": 2.0118256069134888,
                    "beam_min_scaled": 1.9113197332118808,
                    "beam_pa": -402.9659297877625,
                },
                "confusion_noise": {
                    "limit_type": "lower limit",
                    "value": 91.28038472887631,
                },
                "sbs_conv_factor": 24523.660584939087,
                "subband_freq_centre": {"unit": "Hz", "value": 1000000.0},
                "weighting_factor": 3.574555564224668,
            },
            {
                "beam_size": {
                    "beam_maj_scaled": 1.0059128034567444,
                    "beam_min_scaled": 0.9556598666059404,
                    "beam_pa": -402.9659297877625,
                },
                "confusion_noise": {"limit_type": "value", "value": 30.817953498548224},
                "sbs_conv_factor": 24523.660584939087,
                "subband_freq_centre": {"unit": "Hz", "value": 2000000.0},
                "weighting_factor": 3.574555564224668,
            },
        ]

    def test_weighting_exception(self):
        """
        Test /calculate weighting returns an error response as expected
        """
        combined_error_params = {
            **(WEIGHTING_INPUT_PARAMS | {"weighting_mode": "foo"}),
            **(CONTINUUM_CALCULATE_INPUT_FOR_COMBINE | {"integration_time_s": 1}),
        }

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_error_params,
        )
        response_body = json.loads(response.content)

        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert (
            "'foo' is not one of ['natural', 'robust', 'uniform']"
            in response_body["detail"]
        )

    def test_taper_exception(self):
        """
        Test /calculate weighting when the taper is not an allowed value - should fail the validation against the OpenAPI spec.
        """
        combined_error_params = {
            **(WEIGHTING_INPUT_PARAMS | {"taper": "3.14"}),
            **(CONTINUUM_CALCULATE_INPUT_FOR_COMBINE | {"integration_time_s": 1}),
        }

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=combined_error_params,
        )
        response_body = json.loads(response.content)

        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert (
            "3.14 is not one of "
            "[0, 0.25, 1, 4, 16, 64, 256, 1024]" in response_body["detail"]
        )

    def test_weighted_result_using_default_ui_params(self):
        """
        Test /calculate calculated result using ui params
        """
        params = {
            "n_subbands": 1,
            "supplied_sensitivity": 1.0,
            "sensitivity_unit": "Jy/beam",
            "rx_band": "Band 1",
            "subarray_configuration": "AA4",
            "freq_centre_hz": 797500000.0,
            "bandwidth_hz": 435000000.0,
            "spectral_averaging_factor": 1.0,
            "pointing_centre": "00:00:00.0 00:00:00.0",
            "pwv": 10.0,
            "el": 45.0,
            "subband_supplied_sensitivities_unit": "Jy/beam",
            "weighting_mode": "uniform",
            "robustness": 0.0,
            "alpha": 2.75,
        }

        expected_weighted_result = {
            "weighted_continuum_sensitivity": None,
            "continuum_confusion_noise": {"value": 0.0, "unit": "Jy"},
            "total_continuum_sensitivity": None,
            "continuum_synthesized_beam_size": {
                "beam_maj": {"value": 0.3841663681575149, "unit": "arcsec"},
                "beam_min": {"value": 0.3434616616339319, "unit": "arcsec"},
            },
            "continuum_surface_brightness_sensitivity": None,
            "weighted_spectral_sensitivity": None,
            "spectral_confusion_noise": {"value": 0.0, "unit": "Jy"},
            "total_spectral_sensitivity": None,
            "spectral_synthesized_beam_size": {
                "beam_maj": {"value": 0.620656187913142, "unit": "arcsec"},
                "beam_min": {"value": 0.5502920077563291, "unit": "arcsec"},
            },
            "spectral_surface_brightness_sensitivity": None,
            "continuum_integration_time": {
                "value": 1.8506663149092246e-06,
                "unit": "s",
            },
            "spectral_integration_time": {"value": 0.016187084064089984, "unit": "s"},
            "confusion_noise_per_subband": None,
            "synthesized_beam_size_per_subband": None,
            "integration_time_per_subband": None,
            "warnings": [],
        }

        response = requests.get(
            f"{MID_API_URL}/continuum/calculate",
            params=params,
        )
        response_body = json.loads(response.content)

        assert response.status_code == HTTPStatus.OK
        assert response_body["weighted_result"] == expected_weighted_result

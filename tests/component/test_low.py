import json
from http import HTTPStatus
from json import loads

import pytest
import requests

from ska_ost_senscalc.common.service import SENS_LIMIT_WARNING
from tests import LowErrorMessages as err_mess

from . import LOW_API_URL


class TestPSS:
    PSS_CALCULATE_URL = f"{LOW_API_URL}/pss/calculate"

    def test_low_calculate_pss_without_params(self):
        response = requests.get(
            f"{self.PSS_CALCULATE_URL}?num_stations=512&pulsar_mode=folded_pulse"
        )
        result = loads(response.content)

        assert response.status_code == HTTPStatus.OK
        assert (
            pytest.approx(result["folded_pulse_sensitivity"]["value"])
            == 0.07905716305673122
        )
        assert result["folded_pulse_sensitivity"]["unit"] == "uJy"

    @pytest.mark.parametrize(
        "params, sensitivity_result, sensitivity_units",
        [
            (
                {
                    "num_stations": 100,
                    "freq_centre_mhz": 150,
                    "pointing_centre": "08:00:00 20:00:50",
                    "integration_time_h": 0.5,
                    "elevation_limit": 20,
                    "dm": 0.0,
                    "intrinsic_pulse_width": 0.004,
                    "pulse_period": 33,
                    "pulsar_mode": "folded_pulse",
                },
                1.5633063325914773,
                "uJy",
            ),
        ],
    )
    def test_low_calculate_pss_with_params_folded(
        self, params, sensitivity_result, sensitivity_units
    ):
        response = requests.get(f"{LOW_API_URL}/pss/calculate", params=params)
        result = loads(response.content)

        assert response.status_code == HTTPStatus.OK
        assert (
            pytest.approx(result["folded_pulse_sensitivity"]["value"])
            == sensitivity_result
        )
        assert result["folded_pulse_sensitivity"]["unit"] == sensitivity_units

    @pytest.mark.parametrize(
        "params, sensitivity_result, sensitivity_units",
        [
            (
                {
                    "num_stations": 100,
                    "freq_centre_mhz": 150,
                    "pointing_centre": "08:00:00 20:00:50",
                    "integration_time_h": 0.5,
                    "elevation_limit": 20,
                    "dm": 0.0,
                    "intrinsic_pulse_width": 0.004,
                    "bandwidth_mhz": 100,
                    "pulsar_mode": "single_pulse",
                },
                25067.487613532267,
                "uJy",
            ),
            (
                {
                    "num_stations": 100,
                    "freq_centre_mhz": 150,
                    "pointing_centre": "08:00:00 20:00:50",
                    "integration_time_h": 0.5,
                    "elevation_limit": 20,
                    "dm": 0.0,
                    "intrinsic_pulse_width": 0.004,
                    "bandwidth_mhz": 1,
                    "pulsar_mode": "single_pulse",
                },
                302167.28229736094,
                "uJy",
            ),
        ],
    )
    def test_low_calculate_pss_with_params_single(
        self, params, sensitivity_result, sensitivity_units
    ):
        response = requests.get(f"{LOW_API_URL}/pss/calculate", params=params)
        result = loads(response.content)

        assert response.status_code == HTTPStatus.OK
        assert (
            pytest.approx(result["folded_pulse_sensitivity"]["value"])
            == sensitivity_result
        )
        assert result["folded_pulse_sensitivity"]["unit"] == sensitivity_units

    def test_low_calculate_pss_validation_error(self):
        params = {
            "num_stations": 512,
            "freq_centre_mhz": 200,
            "pointing_centre": "13:25:00 -63:01:09",
            "integration_time_h": 5,
            "elevation_limit": 20,
            "dm": 10000.0,
            "pulse_period": 7.2,
            "intrinsic_pulse_width": 1,
            "pulsar_mode": "folded_pulse",
        }
        response = requests.get(f"{self.PSS_CALCULATE_URL}", params=params)
        result = loads(response.content)

        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert result["title"] == "Validation Error"


class TestContinuum:
    CONTINUUM_CALCULATE_URL = f"{LOW_API_URL}/continuum/calculate"

    def test_low_calculate_continuum_with_required_params(self):
        params = {
            "subarray_configuration": "LOW_AA4_all",
            "integration_time_h": 1,
            "pointing_centre": "10:00:00.0 -30:00:00.0",
            "elevation_limit": 20,
            "freq_centre_mhz": 200,
            "spectral_averaging_factor": 1,
            "bandwidth_mhz": 300,
            "weighting_mode": "uniform",
            "robustness": 0,
        }
        response = requests.get(f"{self.CONTINUUM_CALCULATE_URL}?", params=params)
        result = loads(response.content)

        expected = {"value": 4.77839424123094, "unit": "uJy / beam"}

        assert HTTPStatus.OK == response.status_code
        assert expected["value"] == pytest.approx(
            result["calculate"]["continuum_sensitivity"]["value"]
        )
        assert expected["unit"] == result["calculate"]["continuum_sensitivity"]["unit"]

    @pytest.mark.parametrize(
        "params, expected_sensitivity_result",
        [
            (
                {
                    "subarray_configuration": "LOW_AA4_all",
                    "freq_centre_mhz": 100,
                    "bandwidth_mhz": 50,
                    "pointing_centre": "08:00:00 20:00:50",
                    "integration_time_h": 0.5,
                    "elevation_limit": 20,
                    "spectral_averaging_factor": 1,
                    "weighting_mode": "uniform",
                    "robustness": 0,
                },
                26.036571389568184,
            ),
            (
                {
                    "subarray_configuration": "LOW_AAstar_all",
                    "freq_centre_mhz": 330,
                    "bandwidth_mhz": 20,
                    "pointing_centre": "00:00:00 00:00:00",
                    "integration_time_h": 0.3,
                    "elevation_limit": 20,
                    "spectral_averaging_factor": 1,
                    "weighting_mode": "uniform",
                    "robustness": 0,
                },
                60.76962819180243,
            ),
            (
                {
                    "subarray_configuration": "LOW_AA4_all",
                    "freq_centre_mhz": 200,
                    "bandwidth_mhz": 300,
                    "pointing_centre": "10:00:00 -30:00:00",
                    "integration_time_h": 0.05,
                    "elevation_limit": 20,
                    "spectral_averaging_factor": 1,
                    "weighting_mode": "uniform",
                    "robustness": 0,
                },
                21.880681504687406,
            ),
            (
                {
                    "subarray_configuration": "LOW_AA4_all",
                    "freq_centre_mhz": 200,
                    "bandwidth_mhz": 300,
                    "pointing_centre": "00:00:00 -29:00:00",
                    "integration_time_h": 0.05,
                    "elevation_limit": 20,
                    "spectral_averaging_factor": 1,
                    "weighting_mode": "uniform",
                    "robustness": 0,
                },
                19.274829738369085,
            ),
            (
                {
                    "subarray_configuration": "LOW_AA4_all",
                    "freq_centre_mhz": 200,
                    "bandwidth_mhz": 1,
                    "pointing_centre": "13:25:00 -63:01:09",
                    "integration_time_h": 5,
                    "elevation_limit": 20,
                    "spectral_averaging_factor": 1,
                    "weighting_mode": "uniform",
                    "robustness": 0,
                },
                78.67284671786479,
            ),
        ],
    )
    def test_low_calculate_continuum_with_params(
        self, params, expected_sensitivity_result
    ):
        response = requests.get(f"{LOW_API_URL}/continuum/calculate", params=params)
        result = loads(response.content)

        expected_unit = "uJy / beam"

        assert HTTPStatus.OK == response.status_code
        assert expected_sensitivity_result == pytest.approx(
            result["calculate"]["continuum_sensitivity"]["value"]
        )
        assert expected_unit == result["calculate"]["continuum_sensitivity"]["unit"]

    def test_low_calculate_continuum_validation_error(self):
        params = {
            "subarray_configuration": "LOW_AA4_all",
            "freq_centre_mhz": 60,
            "bandwidth_mhz": 50,
            "pointing_centre": "13:25:00 -63:01:09",
            "integration_time_h": 5,
            "spectral_averaging_factor": 1,
            "weighting_mode": "uniform",
            "robustness": 0,
        }
        response = requests.get(self.CONTINUUM_CALCULATE_URL, params=params)
        result = loads(response.content)

        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert result["detail"] == err_mess.ZOOM_SPECTRAL_WINDOW

    def test_low_calculate_returns_subband_sensitivities_and_calculated_results_subband_values(
        self,
    ):
        """
        Test /calculate returns the correct subband sensitivities when given an integration times and n_subbands
        """
        params = {
            "subarray_configuration": "LOW_AA4_all",
            "freq_centre_mhz": 200,
            "bandwidth_mhz": 1,
            "pointing_centre": "13:25:00 -63:01:09",
            "integration_time_h": 0.1,
            "elevation_limit": 20,
            "n_subbands": 2,
            "spectral_averaging_factor": 1,
            "weighting_mode": "uniform",
            "robustness": 0,
        }
        response = requests.get(self.CONTINUUM_CALCULATE_URL, params=params)
        result = loads(response.content)

        assert HTTPStatus.OK == response.status_code
        sensitivies = [
            subband_result["sensitivity"]["value"]
            for subband_result in result["calculate"]["continuum_subband_sensitivities"]
        ]

        expected_sensitivities = [772.7909093188443, 774.1145468772285]

        assert expected_sensitivities == pytest.approx(sensitivies)

        expected_weighted_sensitivity = {
            "min_value": {"value": 0.008140626444916099, "unit": "Jy / beam"},
            "max_value": {"value": 0.00812670701530125, "unit": "Jy / beam"},
        }

        assert (
            expected_weighted_sensitivity
            == result["weighted_result"]["weighted_sensitivity_per_subband"]
        )

        expected_confusion_noise = {
            "min_value": {"value": 8.050281859332627e-07, "unit": "Jy"},
            "max_value": {"value": 8.123502189952575e-07, "unit": "Jy"},
        }

        assert (
            expected_confusion_noise
            == result["weighted_result"]["confusion_noise_per_subband"]
        )

        expected_total_sensitivity = {
            "min_value": {"value": 0.008140626484720798, "unit": "Jy / beam"},
            "max_value": {"value": 0.008126707055902743, "unit": "Jy / beam"},
        }

        assert (
            expected_total_sensitivity
            == result["weighted_result"]["total_sensitivity_per_subband"]
        )

        expected_beam_size = {
            "min_value": {
                "beam_maj": {"unit": "arcsec", "value": 4.29934935552931},
                "beam_min": {"unit": "arcsec", "value": 2.3026956661403806},
            },
            "max_value": {
                "beam_maj": {"unit": "arcsec", "value": 4.310111181200223},
                "beam_min": {"unit": "arcsec", "value": 2.308459610235851},
            },
        }

        assert (
            expected_beam_size
            == result["weighted_result"]["synthesized_beam_size_per_subband"]
        )

        expected_surface_brightness = {
            "min_value": {"value": 25060.36964447636, "unit": "K"},
            "max_value": {"value": 25017.519621560423, "unit": "K"},
        }

        assert (
            expected_surface_brightness
            == result["weighted_result"]["surface_brightness_sensitivity_per_subband"]
        )

    def test_low_spectropolarimetry_values(self):
        params = {
            "subarray_configuration": "LOW_AA4_all",
            "freq_centre_mhz": 200,
            "bandwidth_mhz": 50,
            "pointing_centre": "13:25:00 -63:01:09",
            "integration_time_h": 5,
            "elevation_limit": 20,
            "spectral_averaging_factor": 1,
            "weighting_mode": "uniform",
            "robustness": 0,
        }
        response = requests.get(self.CONTINUUM_CALCULATE_URL, params=params)
        result = loads(response.content)

        assert HTTPStatus.OK == response.status_code

        expected = {
            "fwhm_of_the_rmsf": {
                "unit": "rad / m2",
                "value": 2.9882055873208273,
            },
            "max_faraday_depth": {
                "unit": "rad / m2",
                "value": 9519.10307014195,
            },
            "max_faraday_depth_extent": {
                "unit": "rad / m2",
                "value": 1.7695507844823528,
            },
        }
        assert expected == result["calculate"]["spectropolarimetry_results"]

    def test_low_weighting_results(self):
        params = {
            "freq_centre_mhz": 200,
            "pointing_centre": "08:00:00 20:00:50",
            "weighting_mode": "uniform",
            "subarray_configuration": "LOW_AA4_all",
        }
        response = requests.get(f"{LOW_API_URL}/continuum/calculate", params=params)
        result = loads(response.content)

        assert response.status_code == HTTPStatus.OK, response.content

        expected_spectral_weighting = {
            "beam_size": {
                "beam_maj_scaled": 0.0021447016023778314,
                "beam_min_scaled": 0.0012360198298229088,
                "beam_pa": 4.103128002184216,
            },
            "confusion_noise": {
                "limit_type": "value",
                "value": 4.283742665039918e-06,
            },
            "sbs_conv_factor": 889317.147072434,
            "subbands": [],
            "weighting_factor": 13.593045402531596,
        }
        assert expected_spectral_weighting == result["weighting"]["spectral_weighting"]

        expected_continuum_weighting = {
            "beam_size": {
                "beam_maj_scaled": 0.0015492102611653967,
                "beam_min_scaled": 0.0007646406532278215,
                "beam_pa": 1.7131501948139467,
            },
            "confusion_noise": {
                "limit_type": "value",
                "value": pytest.approx(1.5074043995002468e-06),
            },
            "sbs_conv_factor": 1990129.0154467784,
            "subbands": [],
            "weighting_factor": 15.883795921989435,
        }
        assert (
            expected_continuum_weighting == result["weighting"]["continuum_weighting"]
        )

    @pytest.mark.parametrize(
        "params, weighting_result",
        [
            (
                {
                    "freq_centre_mhz": 200,
                    "pointing_centre": "08:00:00 20:00:50",
                    "weighting_mode": "natural",
                    "subarray_configuration": "LOW_AA4_all",
                    "subband_freq_centres_mhz": [100, 200],
                    "n_subbands": 2,
                },
                {
                    "weighting_factor": 1,
                    "sbs_conv_factor": 148559.2068694863,
                    "confusion_noise": {
                        "limit_type": "value",
                        "value": 0.00003062179392287691,
                    },
                    "beam_size": [
                        {
                            "beam_maj_scaled": 0.06664917462264908,
                            "beam_min_scaled": 0.017279103686617562,
                            "beam_pa": 101.39284464459573,
                        }
                    ],
                    "subbands": [
                        {
                            "subband_freq_centre": {"unit": "Hz", "value": 100000000},
                            "beam_size": {
                                "beam_maj_scaled": 0.010196861159310674,
                                "beam_min_scaled": 0.00622505071452974,
                                "beam_pa": 1.563817893215893,
                            },
                            "confusion_noise": {
                                "limit_type": "value",
                                "value": 0.00017524228556527598,
                            },
                            "sbs_conv_factor": 148559.2068694863,
                            "weighting_factor": 1,
                        },
                        {
                            "subband_freq_centre": {"unit": "Hz", "value": 200000000},
                            "beam_size": {
                                "beam_maj_scaled": 0.005098430579655337,
                                "beam_min_scaled": 0.00311252535726487,
                                "beam_pa": 1.563817893215893,
                            },
                            "confusion_noise": {
                                "limit_type": "value",
                                "value": 0.00003062179392287691,
                            },
                            "sbs_conv_factor": 148559.2068694863,
                            "weighting_factor": 1,
                        },
                    ],
                },
            )
        ],
    )
    def test_low_continuum_weighting_results_subbands_and_calculated_results_subband_values(
        self, params, weighting_result
    ):
        response = requests.get(f"{LOW_API_URL}/continuum/calculate", params=params)
        result = loads(response.content)

        assert response.status_code == HTTPStatus.OK, response.content
        assert (
            weighting_result["subbands"]
            == result["weighting"]["continuum_weighting"]["subbands"]
        )

        expected_weighted_sensitivity = {
            "min_value": {"value": 0.00001466310466249997, "unit": "Jy / beam"},
            "max_value": {"value": 0.000014234045212223832, "unit": "Jy / beam"},
        }
        assert (
            expected_weighted_sensitivity
            == result["weighted_result"]["weighted_sensitivity_per_subband"]
        )

        expected_confusion_noise = {
            "min_value": {"value": 0.00003062179392287691, "unit": "Jy"},
            "max_value": {"value": 0.00017524228556527598, "unit": "Jy"},
        }
        assert (
            expected_confusion_noise
            == result["weighted_result"]["confusion_noise_per_subband"]
        )

        expected_total_sensitivity = {
            "min_value": {
                "value": pytest.approx(3.395144917965313e-05, rel=1e-15),
                "unit": "Jy / beam",
            },
            "max_value": {"value": 0.00017581941500654973, "unit": "Jy / beam"},
        }
        assert (
            expected_total_sensitivity
            == result["weighted_result"]["total_sensitivity_per_subband"]
        )

        expected_beam_size = {
            "max_value": {
                "beam_maj": {"unit": "arcsec", "value": 36.70870017351842},
                "beam_min": {"unit": "arcsec", "value": 22.410182572307065},
            },
            "min_value": {
                "beam_maj": {"unit": "arcsec", "value": 18.35435008675921},
                "beam_min": {"unit": "arcsec", "value": 11.205091286153532},
            },
        }
        assert (
            expected_beam_size
            == result["weighted_result"]["synthesized_beam_size_per_subband"]
        )

    @pytest.mark.parametrize(
        "params, expected",
        [
            (
                {
                    "freq_centre_mhz": 200,
                    "pointing_centre": "08:00:00 20:00:50",
                    "weighting_mode": "natural",
                    "subarray_configuration": "LOW_AA4_all",
                    "n_subbands": 3,
                },
                {
                    "weighting_factor": 1,
                    "sbs_conv_factor": 148559.2068694863,
                    "confusion_noise": {
                        "value": 0.00003062179392287691,
                        "limit_type": "value",
                    },
                    "beam_size": {
                        "beam_maj_scaled": 0.005098430579655337,
                        "beam_min_scaled": 0.00311252535726487,
                        "beam_pa": 1.563817893215893,
                    },
                    "subbands": [
                        {
                            "subband_freq_centre": {"value": 100000000.0, "unit": "Hz"},
                            "weighting_factor": 1.0,
                            "sbs_conv_factor": 148559.2068694863,
                            "confusion_noise": {
                                "value": 0.00017524228556527598,
                                "limit_type": "value",
                            },
                            "beam_size": {
                                "beam_maj_scaled": 0.010196861159310674,
                                "beam_min_scaled": 0.00622505071452974,
                                "beam_pa": 1.563817893215893,
                            },
                        },
                        {
                            "subband_freq_centre": {"value": 200000000.0, "unit": "Hz"},
                            "weighting_factor": 1.0,
                            "sbs_conv_factor": 148559.2068694863,
                            "confusion_noise": {
                                "value": 3.062179392287691e-05,
                                "limit_type": "value",
                            },
                            "beam_size": {
                                "beam_maj_scaled": 0.005098430579655337,
                                "beam_min_scaled": 0.00311252535726487,
                                "beam_pa": 1.563817893215893,
                            },
                        },
                        {
                            "subband_freq_centre": {"value": 300000000.0, "unit": "Hz"},
                            "weighting_factor": 1.0,
                            "sbs_conv_factor": 148559.20686948637,
                            "confusion_noise": {
                                "value": 1.0192081455874415e-05,
                                "limit_type": "value",
                            },
                            "beam_size": {
                                "beam_maj_scaled": 0.0033989537197702245,
                                "beam_min_scaled": 0.0020750169048432466,
                                "beam_pa": 1.563817893215893,
                            },
                        },
                    ],
                },
            )
        ],
    )
    def test_generate_subband_frequencies_for_low_continuum_weighting_results_subbands(
        self, params, expected
    ):
        response = requests.get(f"{LOW_API_URL}/continuum/calculate", params=params)
        result = loads(response.content)
        subbands = result["weighting"]["continuum_weighting"]["subbands"]

        assert response.status_code == HTTPStatus.OK, response.content
        assert subbands == expected["subbands"]

    def test_no_sensitivity_warning_for_weighting_results_subbands(self):
        params = {
            "subarray_configuration": "LOW_AA4_all",
            "integration_time_h": 1,
            "pointing_centre": "10:00:00.0 -30:00:00.0",
            "elevation_limit": 15,
            "freq_centre_mhz": 200,
            "spectral_averaging_factor": 1,
            "bandwidth_mhz": 300,
            "n_subbands": 2,
            "weighting_mode": "uniform",
            "robustness": 0,
        }
        response = requests.get(f"{LOW_API_URL}/continuum/calculate", params=params)
        result = loads(response.content)

        assert response.status_code == HTTPStatus.OK, response.content
        assert result["weighted_result"]["warnings"] == []

    def test_return_sensitivity_warning_for_weighting_results_subbands(self):
        params = {
            "subarray_configuration": "LOW_AA4_all",
            "integration_time_h": 1,
            "pointing_centre": "00:00:00 00:00:00",
            "elevation_limit": 15,
            "freq_centre_mhz": 150,
            "spectral_averaging_factor": 1,
            "bandwidth_mhz": 100,
            "n_subbands": 2,
            "weighting_mode": "natural",
            "robustness": 0,
            "subband_freq_centres_mhz": [150, 50],
        }
        response = requests.get(f"{LOW_API_URL}/continuum/calculate", params=params)
        result = loads(response.content)

        assert response.status_code == HTTPStatus.OK, response.content
        expected = [
            {"continuum_sensitivity": SENS_LIMIT_WARNING},
            {"max_sensitivity": SENS_LIMIT_WARNING},
            {"min_sensitivity": SENS_LIMIT_WARNING},
        ]
        assert expected == result["weighted_result"]["warnings"]

    def test_low_continuum_custom_array_results(self):
        params = {
            "num_stations": "300",
            "integration_time_h": 1,
            "pointing_centre": "10:00:00.0 -30:00:00.0",
            "elevation_limit": 15,
            "freq_centre_mhz": 200,
            "spectral_averaging_factor": 1,
            "bandwidth_mhz": 300,
            "weighting_mode": "uniform",
            "robustness": 0,
        }

        expected = {
            "calculate": {
                "continuum_sensitivity": {
                    "value": 8.160770940086346,
                    "unit": "uJy / beam",
                },
                "continuum_subband_sensitivities": [],
                "spectral_sensitivity": {
                    "value": 1138.3846305170266,
                    "unit": "uJy / beam",
                },
                "warnings": [],
                "spectropolarimetry_results": {
                    "fwhm_of_the_rmsf": {
                        "value": 0.09837671426218081,
                        "unit": "rad / m2",
                    },
                    "max_faraday_depth_extent": {
                        "value": 4.28191284692133,
                        "unit": "rad / m2",
                    },
                    "max_faraday_depth": {
                        "value": 222.0457075444506,
                        "unit": "rad / m2",
                    },
                },
            },
            "weighting": {"continuum_weighting": None, "spectral_weighting": None},
            "weighted_result": {
                "weighted_continuum_sensitivity": {
                    "value": 8.160770940086345e-06,
                    "unit": "Jy / beam",
                },
                "continuum_confusion_noise": None,
                "total_continuum_sensitivity": None,
                "continuum_synthesized_beam_size": None,
                "continuum_surface_brightness_sensitivity": None,
                "weighted_spectral_sensitivity": {
                    "value": 0.0011383846305170265,
                    "unit": "Jy / beam",
                },
                "spectral_confusion_noise": None,
                "total_spectral_sensitivity": None,
                "spectral_synthesized_beam_size": None,
                "spectral_surface_brightness_sensitivity": None,
                "continuum_integration_time": None,
                "spectral_integration_time": None,
                "confusion_noise_per_subband": None,
                "synthesized_beam_size_per_subband": None,
                "integration_time_per_subband": None,
                "warnings": [],
            },
        }

        response = requests.get(f"{LOW_API_URL}/continuum/calculate", params=params)
        result = loads(response.content)

        assert response.status_code == HTTPStatus.OK, response.content
        assert expected == result


class TestZoom:
    ZOOM_CALCULATE_URL = f"{LOW_API_URL}/zoom/calculate"

    WEIGHTING_INPUT = {
        "weighting_mode": "uniform",
        "robustness": 0.0,
    }
    CALCULATE_INPUT = {
        "total_bandwidths_khz": [390.6, 390.6],
        "spectral_resolutions_hz": [14.1285, 14.1285],
        "integration_time_h": 5,
    }

    def test_low_calculate_line_with_params(self):
        params = {
            "num_stations": 512,
            "freq_centres_mhz": [200],
            "total_bandwidths_khz": [24.4],
            "spectral_resolutions_hz": [14.1285],
            "pointing_centre": "13:25:00 -63:01:09",
            "integration_time_h": 5,
        }
        response = requests.get(
            self.ZOOM_CALCULATE_URL, params=params | self.WEIGHTING_INPUT
        )
        result = loads(response.content)

        assert response.status_code == HTTPStatus.OK
        assert (
            pytest.approx(result["calculate"][0]["spectral_sensitivity"]["value"])
            == 20975.534191812894
        )
        assert result["calculate"][0]["spectral_sensitivity"]["unit"] == "uJy / beam"

    def test_low_calculate_line_validation_error(self):
        params = {
            "num_stations": 512,
            "freq_centres_mhz": [160],
            "total_bandwidths_khz": [30],
            "spectral_resolutions_hz": [14.1285],
            "pointing_centre": "13:25:00 -63:01:09",
            "integration_time_h": 5,
        }
        response = requests.get(
            self.ZOOM_CALCULATE_URL, params=params | self.WEIGHTING_INPUT
        )
        result = loads(response.content)

        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert result["detail"] == err_mess.ZOOM_TOTAL_BANDWIDTH("30.0", "custom")

    def test_low_spectropolarimetry_values(self):
        params = {
            "num_stations": 512,
            "freq_centres_mhz": [200],
            "total_bandwidths_khz": [24.4],
            "spectral_resolutions_hz": [14.1285],
            "pointing_centre": "13:25:00 -63:01:09",
            "integration_time_h": 5,
        }

        response = requests.get(
            self.ZOOM_CALCULATE_URL, params=params | self.WEIGHTING_INPUT
        )
        result = loads(response.content)

        assert response.status_code == HTTPStatus.OK
        assert result["calculate"][0]["spectropolarimetry_results"] == {
            "fwhm_of_the_rmsf": {"unit": "rad / m2", "value": 6322.239215617077},
            "max_faraday_depth": {"unit": "rad / m2", "value": 5455112.665520816},
            "max_faraday_depth_extent": {
                "unit": "rad / m2",
                "value": 1.3983677833404595,
            },
        }

    @pytest.mark.parametrize(
        "params, weighting_result",
        [
            (
                {
                    "freq_centres_mhz": [100, 150],
                    "pointing_centre": "08:00:00 20:00:50",
                    "weighting_mode": "natural",
                    "subarray_configuration": "LOW_AA2_all",
                },
                [
                    {
                        "freq_centre": {"value": 100e6, "unit": "Hz"},
                        "weighting_factor": 1.0,
                        "sbs_conv_factor": 12000.79191741923,
                        "confusion_noise": {
                            "limit_type": "value",
                            "value": 0.0014359365152178314,
                        },
                        "beam_size": {
                            "beam_maj_scaled": 0.043746875980444765,
                            "beam_min_scaled": 0.01796188954936314,
                            "beam_pa": -120.84447207990723,
                        },
                    },
                    {
                        "freq_centre": {"value": 150e6, "unit": "Hz"},
                        "weighting_factor": 1.0,
                        "sbs_conv_factor": 12000.79191741923,
                        "confusion_noise": {
                            "value": 0.0005360319483845855,
                            "limit_type": "value",
                        },
                        "beam_size": {
                            "beam_maj_scaled": 0.029164583986963175,
                            "beam_min_scaled": 0.01197459303290876,
                            "beam_pa": -120.84447207990723,
                        },
                    },
                ],
            )
        ],
    )
    def test_low_calculate_line_weighting_with_params(self, params, weighting_result):
        response = requests.get(
            self.ZOOM_CALCULATE_URL, params=params | self.CALCULATE_INPUT
        )
        result = loads(response.content)

        assert response.status_code == HTTPStatus.OK, response.content
        assert result["weighting"] == weighting_result

    def test_calculate_zoom_results_low_aa4_all(self):
        params = {
            "subarray_configuration": "LOW_AA4_all",
            "integration_time_h": 1.0,
            "pointing_centre": "10:00:00.0 -30:00:00.0",
            "elevation_limit": 20.0,
            "freq_centres_mhz": [200.0],
            "spectral_averaging_factor": 1.0,
            "spectral_resolutions_hz": [14.129],
            "total_bandwidths_khz": [24.414],
            "weighting_mode": "uniform",
            "robustness": 0.0,
        }

        response = requests.get(self.ZOOM_CALCULATE_URL, params=params)

        res = json.loads(response.content)

        assert response.status_code == HTTPStatus.OK

        expected = [
            {
                "weighted_spectral_sensitivity": {
                    "value": 0.16359254548040303,
                    "unit": "Jy / beam",
                },
                "spectral_confusion_noise": {
                    "value": 0.000002560245257439271,
                    "unit": "Jy",
                },
                "total_spectral_sensitivity": {
                    "value": 0.16359254550043711,
                    "unit": "Jy / beam",
                },
                "spectral_synthesized_beam_size": {
                    "beam_maj": {"value": 5.189251903360761, "unit": "arcsec"},
                    "beam_min": {"value": 4.399358693666859, "unit": "arcsec"},
                },
                "spectral_surface_brightness_sensitivity": {
                    "value": 218939.11900535336,
                    "unit": "K",
                },
                "warnings": [],
            }
        ]

        assert expected == res["weighted_result"]

    def test_low_zoom_custom_array_results(self):
        params = {
            "num_stations": "200",
            "integration_time_h": 2,
            "pointing_centre": "00:00:00.0 00:00:00.0",
            "elevation_limit": 15,
            "freq_centres_mhz": 200,
            "spectral_averaging_factor": 1,
            "spectral_resolutions_hz": 14.129,
            "total_bandwidths_khz": 24.414,
            "weighting_mode": "uniform",
            "robustness": 0,
        }

        expected = {
            "calculate": [
                {
                    "freq_centre": {"value": 200.0, "unit": "MHz"},
                    "spectral_sensitivity": {
                        "value": 38696.35314798787,
                        "unit": "uJy / beam",
                    },
                    "warnings": [],
                    "spectropolarimetry_results": {
                        "fwhm_of_the_rmsf": {
                            "value": 6318.611811653445,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth_extent": {
                            "value": 1.3983678812167422,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth": {
                            "value": 5454919.046074606,
                            "unit": "rad / m2",
                        },
                    },
                }
            ],
            "weighting": None,
            "weighted_result": [
                {
                    "weighted_spectral_sensitivity": {
                        "value": 0.03869635314798787,
                        "unit": "Jy / beam",
                    },
                    "spectral_confusion_noise": None,
                    "total_spectral_sensitivity": None,
                    "spectral_synthesized_beam_size": None,
                    "spectral_surface_brightness_sensitivity": None,
                    "warnings": [],
                }
            ],
        }

        response = requests.get(f"{LOW_API_URL}/zoom/calculate", params=params)
        result = loads(response.content)

        assert response.status_code == HTTPStatus.OK, response.content
        assert expected == result

    def test_low_multiple_windows_zoom_custom_array_results(self):
        params = {
            "num_stations": "320",
            "integration_time_h": 1,
            "pointing_centre": "00:00:00.0 00:00:00.0",
            "elevation_limit": 20,
            "freq_centres_mhz": [200, 120],
            "spectral_averaging_factor": 1,
            "spectral_resolutions_hz": [14.129, 16],
            "total_bandwidths_khz": [24.414, 48.8],
            "weighting_mode": "uniform",
            "robustness": 0,
        }

        expected = {
            "calculate": [
                {
                    "freq_centre": {"value": 200.0, "unit": "MHz"},
                    "spectral_sensitivity": {
                        "value": 32367.889418240113,
                        "unit": "uJy / beam",
                    },
                    "warnings": [],
                    "spectropolarimetry_results": {
                        "fwhm_of_the_rmsf": {
                            "value": 6318.611811653445,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth_extent": {
                            "value": 1.3983678812167422,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth": {
                            "value": 5454919.046074606,
                            "unit": "rad / m2",
                        },
                    },
                },
                {
                    "freq_centre": {"value": 120.0, "unit": "MHz"},
                    "spectral_sensitivity": {
                        "value": 29687.760038538552,
                        "unit": "uJy / beam",
                    },
                    "warnings": [
                        "The specified pointing contains at least one source brighter than 10.0 Jy. Your observation may be dynamic range limited."
                    ],
                    "spectropolarimetry_results": {
                        "fwhm_of_the_rmsf": {
                            "value": 682.6302297111537,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth_extent": {
                            "value": 0.5035556766323743,
                            "unit": "rad / m2",
                        },
                        "max_faraday_depth": {
                            "value": 1040035.3996867072,
                            "unit": "rad / m2",
                        },
                    },
                },
            ],
            "weighting": None,
            "weighted_result": [
                {
                    "weighted_spectral_sensitivity": {
                        "value": 0.03236788941824011,
                        "unit": "Jy / beam",
                    },
                    "spectral_confusion_noise": None,
                    "total_spectral_sensitivity": None,
                    "spectral_synthesized_beam_size": None,
                    "spectral_surface_brightness_sensitivity": None,
                    "warnings": [],
                },
                {
                    "weighted_spectral_sensitivity": {
                        "value": 0.02968776003853855,
                        "unit": "Jy / beam",
                    },
                    "spectral_confusion_noise": None,
                    "total_spectral_sensitivity": None,
                    "spectral_synthesized_beam_size": None,
                    "spectral_surface_brightness_sensitivity": None,
                    "warnings": [],
                },
            ],
        }

        response = requests.get(f"{LOW_API_URL}/zoom/calculate", params=params)
        result = loads(response.content)

        assert response.status_code == HTTPStatus.OK, response.content
        assert expected == result


class TestSubarrays:
    SUBARRAY_URL = f"{LOW_API_URL}/subarrays"

    def test_subarrays_list(self):
        response = requests.get(self.SUBARRAY_URL)
        result = loads(response.content)

        assert response.status_code == HTTPStatus.OK
        assert {
            "name": "LOW_AA4_core_only",
            "label": "AA4 (core only)",
            "n_stations": 224,
        } in result
